%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: BarryMouthMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: BARRY_GEO
    m_Weight: 0
  - m_Path: BARRY_GEO/strawberry
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_chin_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_leafMain_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_leafMain_00_uJnt/c_leafBack_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_leafMain_00_uJnt/c_leafFront_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_leafMain_00_uJnt/c_leafFront_00_uJnt/c_leafFront_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_leafMain_00_uJnt/c_leafMid_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_leafMain_00_uJnt/c_leafMid_00_uJnt/c_leafMid_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_leafMain_00_uJnt/l_leafBack_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_leafMain_00_uJnt/l_leafFront_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_leafMain_00_uJnt/l_leafFront_00_uJnt/l_leafFront_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_leafMain_00_uJnt/l_leafSide_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_leafMain_00_uJnt/r_leafBack_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_leafMain_00_uJnt/r_leafFront_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_leafMain_00_uJnt/r_leafFront_00_uJnt/r_leafFront_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_leafMain_00_uJnt/r_leafSide_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_mouthMain_00_uJnt
    m_Weight: 1
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_mouthMain_00_uJnt/l_lowLip_00_uJnt
    m_Weight: 1
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_mouthMain_00_uJnt/l_mouthCorner_00_uJnt
    m_Weight: 1
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_mouthMain_00_uJnt/l_upLip_00_uJnt
    m_Weight: 1
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_mouthMain_00_uJnt/r_lowLip_00_uJnt
    m_Weight: 1
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_mouthMain_00_uJnt/r_mouthCorner_00_uJnt
    m_Weight: 1
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/c_mouthMain_00_uJnt/r_upLip_00_uJnt
    m_Weight: 1
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/l_cheek_00_uJnt
    m_Weight: 1
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/l_cheekBone_00_uJnt
    m_Weight: 1
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/l_eye_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/r_cheek_00_uJnt
    m_Weight: 1
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/r_cheekBone_00_uJnt
    m_Weight: 1
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/c_neck_00_uJnt/c_head_00_uJnt/r_eye_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/l_clavicle_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/l_clavicle_00_uJnt/r_clavicle_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/l_clavicle_00_uJnt/r_clavicle_01_uJnt/r_upArm_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/l_clavicle_00_uJnt/r_clavicle_01_uJnt/r_upArm_00_uJnt/r_humerusRibbon_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/l_clavicle_00_uJnt/r_clavicle_01_uJnt/r_upArm_00_uJnt/r_humerusRibbon_01_uJnt/r_lowArm_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/l_clavicle_00_uJnt/r_clavicle_01_uJnt/r_upArm_00_uJnt/r_humerusRibbon_01_uJnt/r_lowArm_00_uJnt/r_radiusRibbon_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/l_clavicle_00_uJnt/r_clavicle_01_uJnt/r_upArm_00_uJnt/r_humerusRibbon_01_uJnt/r_lowArm_00_uJnt/r_radiusRibbon_01_uJnt/r_wrist_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/r_clavicle_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/r_clavicle_00_uJnt/l_clavicle_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/r_clavicle_00_uJnt/l_clavicle_01_uJnt/l_upArm_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/r_clavicle_00_uJnt/l_clavicle_01_uJnt/l_upArm_00_uJnt/l_humerusRibbon_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/r_clavicle_00_uJnt/l_clavicle_01_uJnt/l_upArm_00_uJnt/l_humerusRibbon_01_uJnt/l_lowArm_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/r_clavicle_00_uJnt/l_clavicle_01_uJnt/l_upArm_00_uJnt/l_humerusRibbon_01_uJnt/l_lowArm_00_uJnt/l_radiusRibbon_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/c_spine_00_uJnt/c_spine_01_uJnt/c_spine_02_uJnt/c_chest_00_uJnt/r_clavicle_00_uJnt/l_clavicle_01_uJnt/l_upArm_00_uJnt/l_humerusRibbon_01_uJnt/l_lowArm_00_uJnt/l_radiusRibbon_01_uJnt/l_wrist_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/l_tigh_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/l_tigh_01_uJnt/l_upLeg_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/l_tigh_01_uJnt/l_upLeg_00_uJnt/l_femurRibbon_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/l_tigh_01_uJnt/l_upLeg_00_uJnt/l_femurRibbon_01_uJnt/l_lowLeg_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/l_tigh_01_uJnt/l_upLeg_00_uJnt/l_femurRibbon_01_uJnt/l_lowLeg_00_uJnt/l_tibiaRibbon_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/l_tigh_01_uJnt/l_upLeg_00_uJnt/l_femurRibbon_01_uJnt/l_lowLeg_00_uJnt/l_tibiaRibbon_01_uJnt/l_foot_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/r_tigh_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/r_tigh_01_uJnt/r_upLeg_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/r_tigh_01_uJnt/r_upLeg_00_uJnt/r_femurRibbon_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/r_tigh_01_uJnt/r_upLeg_00_uJnt/r_femurRibbon_01_uJnt/r_lowLeg_00_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/r_tigh_01_uJnt/r_upLeg_00_uJnt/r_femurRibbon_01_uJnt/r_lowLeg_00_uJnt/r_tibiaRibbon_01_uJnt
    m_Weight: 0
  - m_Path: BARRY_SKIN_RIG/c_unityJointsSet_00_grp/c_root_01_uJnt/r_tigh_01_uJnt/r_upLeg_00_uJnt/r_femurRibbon_01_uJnt/r_lowLeg_00_uJnt/r_tibiaRibbon_01_uJnt/r_foot_00_uJnt
    m_Weight: 0
