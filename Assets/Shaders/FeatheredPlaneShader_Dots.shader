﻿Shader "Unlit/FeatheredPlaneShader_Dots"
{
    Properties
    {
        _PlaneColor("Plane Color", Color) = (1,1,1,1)
        _Zoom("Zoom", float) = 1
        _Size("Circle Size", float) = 1
        _Smooth("Circle Smooth Distance", float) = 0.2
    }
    SubShader
    {
        //Fixes sorting issues. Mask all layers underneath
        Pass                                                                                           
        {                                                                                              
            ZWrite On                                                                                  
            ColorMask 0   
        }

        Tags { "RenderType"="Transparent" "Queue"="AlphaTest" }
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite Off
        ZTest LEqual

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

       

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 uv2 : TEXCOORD1;

                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float3 uv2 : TEXCOORD1;

                UNITY_VERTEX_OUTPUT_STEREO
            };

            fixed4 _PlaneColor;
            float _ShortestUVMapping;
            float _Zoom;
            float _Size;
            float _Smooth;
            
            float2 tile(float2 _st, float _zoom) {
                _st *= _zoom;

                //move odd rows
                // _st.x += step(1., fmod(_st.y, 2.0)) * _Time.x;

                return frac(_st);
            } 
       
            float circle(float2 _st, float _size, float _smoothEdges)
            {
                float2 dist = (_st - 0.5) * 2;

                float smooth = _size * _smoothEdges;

                return smoothstep(_size, _size - smooth,  dot(dist, dist));
            }

            float random(float2 st) {
                return frac(sin(dot(st.xy,
                    float2(12.9898, 78.233))) *
                    43758.5453123);
            }
         
            v2f vert (appdata v)
            {
                v2f o;

                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_OUTPUT(v2f, o);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.uv2 = v.uv2;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);

                float2 st = i.uv;

                float2 uniqueTile = floor(i.uv * _Zoom);
                float randomX = random(uniqueTile.x * 100);
                float randomY = random(uniqueTile.y * 100);

                st = tile(st, _Zoom);

                //Multiplier that ping pongs between 0-1. Varies by position with slightly different rates.
                float pingPong = (sin((_Time.z + 100) * (1 - (randomX + randomY) * 0.1) + randomX + randomY) + 1) * 0.5;

                //increase the minimum without overshooting 1
                pingPong *= 0.8;
                pingPong += 0.2;


                //wave
                float2 waveCenter = _Zoom * 0.5;
                float2 dirToCenter = waveCenter - uniqueTile;
                float distToCenter = length(dirToCenter);

                float frequency = 2;
                float period = .5;
                float amplitude = 1;

                float waveMultiplier = ((sin(-_Time.y * frequency + distToCenter * period) + 1) * 0.5) * amplitude;

                //wave is primary effect. PingPong random animation is reduced.
                pingPong = waveMultiplier + pingPong * 0.5;

                fixed4 col = circle(st, _Size * pingPong, _Smooth);

                //Randomize the colors with animation
                col.r *= randomX * pingPong;
                col.g *= randomY * pingPong;


                col = lerp(_PlaneColor, col, col.a);
                // Fade out from as we pass the edge.
                // uv2.x stores a mapped UV that will be "1" at the beginning of the feathering.
                // We fade until we reach at the edge of the shortest UV mapping.
                // This is the remmaped UV value at the vertex.
                // We choose the shorted one so that ll edges will fade out completely.
                // See ARFeatheredPlaneMeshVisualizer.cs for more details.
                col.a *= 1 - smoothstep(1, _ShortestUVMapping, i.uv2.x);
                return col;
            }
            ENDCG
        }
    }
}
