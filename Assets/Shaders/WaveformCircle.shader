Shader "Custom/WaveformCircle"
{
    Properties
    {
       _Visibility("Visibility", Range(0,1)) = 1
       _Pitch("Pitch", Range(0,1)) = 0.5

       _AmplitudeScale("Amplitude Scale", Range(0,1)) = 1
       _AmplitudeMax("Amplitude Max", float) = .3

       _ColorLow("Color - Low Pitch", Color) = (.5,.5,1,1)
       _ColorHigh("Color - High Pitch", Color) = (1,.5,1,1)
       
       _WidthMin("Width Min", float) = 0.1
       _WidthMax("Width Max", float) = 0.3

       _Radius("Radius", Range(0,1)) = 0.833

       _FrequencyMin("Frequency Min", int) = 10
       _FrequencyMax("Frequency Max", int) = 30
       
       _WobbleFrequencyMin("Wobble Frequency Min", float) = 0.7
       _WobbleFrequencyMax("Wobble Frequency Max", float) = 4
       
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
        LOD 100
        ZTest LEqual
        ZWrite Off
     
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
        
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            float _Pitch;
            float _Visibility;

            float _AmplitudeScale;
            float _AmplitudeMax;

            fixed4 _ColorLow;
            fixed4 _ColorHigh;

            float _WidthMin;
            float _WidthMax;

            float _Radius;

            int _FrequencyMax;
            int _FrequencyMin;

            float _WobbleFrequencyMin;
            float _WobbleFrequencyMax;

            float2 random2(float2 st) 
            {
                st = float2(dot(st, float2(127.1, 311.7)),
                    dot(st, float2(269.5, 183.3)));
                return -1.0 + 2.0 * frac(sin(st) * 43758.5453123);
            }

            // Gradient Noise by Inigo Quilez - iq/2013
            // https://www.shadertoy.com/view/XdXGW8
            float noise(float2 st) 
            {
                float2 i = floor(st);
                float2 f = frac(st);

                float2 u = f * f * (3.0 - 2.0 * f);

                return lerp(lerp(dot(random2(i + float2(0.0, 0.0)), f - float2(0.0, 0.0)),
                    dot(random2(i + float2(1.0, 0.0)), f - float2(1.0, 0.0)), u.x),
                    lerp(dot(random2(i + float2(0.0, 1.0)), f - float2(0.0, 1.0)),
                        dot(random2(i + float2(1.0, 1.0)), f - float2(1.0, 1.0)), u.x), u.y);
            }

            float shape(float2 st, float radius) 
            {
                st = -st + 0.5;
                float r = length(st) * 2.0;
                float a = atan2(st.y, st.x);

                float f = radius;

                float frequencyLow = sin(a * _FrequencyMin);
                float frequencyHigh = sin(a * _FrequencyMax);
                float frequency = lerp(frequencyLow, frequencyHigh, _Pitch);

                float wobbleLow = noise(st + _Time.y * _WobbleFrequencyMin);
                float wobbleHigh = noise(st + _Time.y * _WobbleFrequencyMax);
                float wobble = lerp(wobbleLow, wobbleHigh, _Pitch);

                f += frequency * wobble * _AmplitudeScale * _AmplitudeMax;
     
                return 1. - smoothstep(f, f + 0.007, r);
            }

            float shapeBorder(float2 st, float radius, float width) 
            {
                return shape(st, radius) - shape(st, radius - width);
            }

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float width = lerp( _WidthMax, _WidthMin, _Pitch);
                fixed4 col = lerp(_ColorLow, _ColorHigh, _Pitch);
                col *= shapeBorder(i.uv, _Radius, width);
                col.a *= _Visibility;
                return col;
            }
            ENDCG
        }
    }
}
