using UnityEditor;
using UnityEditor.UI;
using UnityEngine;
using UnityEngine.Events;
using NexusStudios.VoxPlop;

namespace Editor
{
    /// <summary>
    /// Allows inspector editing of HoldButton.
    /// </summary>
    [CustomEditor(typeof(HoldButton))]
    public class MyButtonEditor : ButtonEditor
        {
            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();

                // Find the property corresponding to the UnityEvent we want to edit.
                var pointerDown = serializedObject.FindProperty("OnPointerDownEvent");
                var pointerUp = serializedObject.FindProperty("OnPointerUpEvent");

                // Draw the Inspector widget for this property.
                EditorGUILayout.PropertyField(pointerDown, true);
                EditorGUILayout.PropertyField(pointerUp, true);

                // Commit changes to the property back to the component we're editing.
                serializedObject.ApplyModifiedProperties();
            }
        }
}
