using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;


namespace NexusStudios.VoxPlop
{
    /// <summary>
    /// An extension of the button class to allow for events to fire on both touch and release.
    /// </summary>
    public class HoldButton : Button
    {
        /// <summary>
        /// Event called on button press.
        /// </summary>
        [SerializeField]
        public UnityEvent OnPointerDownEvent;
        
        /// <summary>
        /// Event called on button release.
        /// </summary>
        public UnityEvent OnPointerUpEvent;

        /// <summary>
        /// Calls button down event, on touch or click.
        /// </summary>
        /// <param name="_data"></param>
        public override void OnPointerDown(PointerEventData _data)
        {
            OnPointerDownEvent.Invoke();
            base.OnPointerDown(_data);

        }

        /// <summary>
        /// Calls button up event, on touch or mouse up.
        /// </summary>
        /// <param name="_data"></param>
        public override void OnPointerUp(PointerEventData _data)
        {
            OnPointerUpEvent.Invoke();
            base.OnPointerUp(_data);
        }
    }
}