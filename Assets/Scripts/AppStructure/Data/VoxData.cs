using UnityEngine;
using NxAppStructure;
using NexusStudios.VoxPlop;

namespace AppStructure.Data
{
    /// <summary>
    /// Used to maintain information that defines a Vox.
    /// </summary> 
    [CreateAssetMenu(fileName = "VoxData", menuName = "Nexus/Instance/VoxData", order = 1)]
    public class VoxData : NxAppData
    {
        #region Variables
        /// <summary>
        /// The prefab that is spawned when the vox is placed.
        /// </summary>
        public GameObject prefab;

        /// <summary>
        /// A reference to the audio sampler of the instantiated prefab in the scene
        /// </summary>
        [HideInInspector] public AudioSampler audioSampler;

        /// <summary>
        /// Touch point visuals prefab
        /// </summary>
        public GameObject touchVisualizerPrefab;
        
        /// <summary>
        /// The vox's to be displayed in UI components
        /// </summary>
        public Sprite selectIcon;

        /// <summary>
        /// The name of the vox
        /// </summary>
        public VoxName voxName;

        /// <summary>
        /// The image of the title text that appears on the RecordingAudio View
        /// </summary>
        public Texture titleImage;
        
        /// <summary>
        /// A description of the vox that appears on the RecordingAudio View
        /// </summary>
        public string description;

        /// <summary>
        /// The small icon for the vox, appearing on the add a vox button and on library items that contain the vox.
        /// </summary>
        public Texture smallIcon;
        
        /// <summary>
        /// A larger image that appears in dialogs
        /// </summary>
        /// <remarks>Currently unused!</remarks>
        public Texture heroImage;

        /// <summary>
        /// The music that plays when the vox is plopped into a scene
        /// </summary>
        public AudioClip music;
        
        #endregion

        #region CustomMethods
        #endregion

        #region RequiredMethods
        /// <summary>
        /// Called as soon as the app has been initialized and the state machine is ready. 
        /// </summary>
        public override void Initialize()
        {
        }
        #endregion
    }
}
