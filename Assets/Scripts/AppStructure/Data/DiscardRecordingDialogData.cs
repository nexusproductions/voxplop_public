using UnityEngine;
using NxAppStructure;

using AppStructure.States;

namespace AppStructure.States{

    //TODO: With the back logic, this is a little vestigial
    /// <summary>
    /// Enumerates potential states and views to which to return.
    /// </summary>
    /// <remarks>With new back logic, this could be refactored.</remarks>
    public enum DiscardRecordingDialogDestination{
        MainMenu,
        RecordRaw,
        RecordAudio,
        Back
    }
}

namespace AppStructure.Data
{
    /// <summary>
    /// A data structure passed to the DiscardRecordingDialog that adjusts its behaviour, currently just its destination on confirmation.
    /// </summary>
    [CreateAssetMenu(fileName = "DiscardRecordingDialogData", menuName = "Nexus/Instance/DiscardRecordingDialogData", order = 1)]
    public class DiscardRecordingDialogData : NxAppData
    {
        #region Variables
        /// <summary>
        /// Target state on confirmation in dialog.
        /// </summary>        
        public DiscardRecordingDialogDestination ConfirmationDestination;
        #endregion

        #region CustomMethods
        #endregion

        #region RequiredMethods
        /// <summary>
        /// Called as soon as the app has been initialized and the state machine is ready. 
        /// </summary>
        public override void Initialize()
        {
            // throw new System.NotImplementedException();
        }
        #endregion
    }
}
