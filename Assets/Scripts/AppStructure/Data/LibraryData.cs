using UnityEngine;
using NxAppStructure;
using System.Collections.Generic;

using NexusStudios.VoxPlop;

namespace AppStructure.Data
{
    /// <summary>
    /// The different types of files (with various amounts of attached data) the library cana manage.
    /// </summary>
    public enum LibraryItemType {
        None,
        RawVideo,
        PloppedVideo_Solo,
        PloppedVideo_Multiple
    }

    /// <summary>
    /// The ways in which the library can display its contents.
    /// </summary>
    public enum LibraryDisplayMode{
        Gallery,
        List
    }

    /// <summary>
    /// An item the library is managing, loaded by LibraryManager, managed by <see cref="LibraryData"/>, to be displayed by LibraryItemCtrl.
    /// </summary>
    public struct LibraryItem{
        public LibraryItemType type;
        public Texture2D thumbnail;
        public float duration;
        public string filename;
        public VoxData embeddedVox;
    }

    /// <summary>
    /// LibraryData provides content for LibraryView, as well as setting filtering and display options.
    /// </summary>
    [CreateAssetMenu(fileName = "LibraryData", menuName = "Nexus/Instance/LibraryData", order = 1)]
    public class LibraryData : NxAppData
    {
        #region Variables
        /// <summary>
        /// How the library items should be displayed.
        /// </summary>
        /// TODO: are we going to have multiple displays?
        public LibraryDisplayMode displayMode;

        /// <summary>
        /// A filter for which types of items should be displayed
        /// </summary>
        /// FIXME: implement
        public List<LibraryItemType> displayTypes;

        /// <summary>
        /// A reference to the thumbnail of the most recent shot
        /// </summary>
        public Texture2D lastShotThumbnail { get { return libraryItems[0].thumbnail; } }

        /// <summary>
        /// Items loaded from LibraryManager that match the filtered <see cref="displayTypes"/>.
        /// </summary>
        public LibraryItem[] libraryItems;

        public ARSceneData arSceneData;

        /// <summary>
        /// The prefab to instantiate when populating the library view.
        /// </summary>
        public GameObject libraryGridItemPrefab;
        #endregion

        #region CustomMethods

        /// <summary>
        /// Replaces current library items with reloaded set from LibraryManager.
        /// </summary>
        public void ReloadLibrary(){
            /// load mp4s from persistent data path

            Debug.Log("Reloading library...");
            libraryItems = LibraryManager.GetItems();
        }

        /// <summary>
        /// Delete an item from the library (and storage).
        /// </summary>
        /// <param name="item">Item to be deleted</param>
        public void DeleteItem(LibraryItem item){
            // TODO: Delete item
            Debug.Log("Deleting item...");

        }

        #endregion

        #region RequiredMethods
        /// <summary>
        /// Called as soon as the app has been initialized and the state machine is ready. 
        /// </summary>
        public override void Initialize()
        {
            // Load libary on startup. This should create any thumbnails that don't exist ahead of time.
            ReloadLibrary();
        }
        #endregion
    }



}
