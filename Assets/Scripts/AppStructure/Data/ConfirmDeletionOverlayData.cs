using UnityEngine;
using NxAppStructure;

using AppStructure.Views;

namespace AppStructure.Data
{
    /// <summary>
    /// The NxAppData class should be used to store persistent data and references.
    /// </summary>
    [CreateAssetMenu(fileName = "ConfirmDeletionOverlayDataData", menuName = "Nexus/Instance/ConfirmDeletionOverlayDataData", order = 1)]
    public class ConfirmDeletionOverlayData : NxAppData
    {
        #region Variables

        /// <summary>
        /// Allows for deletion of item by parent.
        /// </summary>
        /// <remarks>It may be better to pass an item reference and do the deletion here?</remarks>
        public LibraryView parentView;
        #endregion

        #region CustomMethods
        #endregion

        #region RequiredMethods
        /// <summary>
        /// Called as soon as the app has been initialized and the state machine is ready. 
        /// </summary>
        public override void Initialize()
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
