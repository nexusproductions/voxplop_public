using System.Collections.Generic;
using AppStructure.States;
using NexusStudios.VoxPlop;
using UnityEngine;
using NxAppStructure;
using UnityEngine.Video;

namespace AppStructure.Data
{
    /// <summary>
    /// Global data structure, with references to library and arscenedata for inter-scene access.
    /// </summary>
    [CreateAssetMenu(fileName = "GlobalSessionData", menuName = "Nexus/Instance/GloballSessionData", order = 1)]
    public class GlobalSessionData : NxAppData
    {
        #region Variables
        /// <summary>
        /// Where the native android back button should take user
        /// </summary>
        private Stack< NxAppState<NxAppManager> > backStack = new Stack<NxAppState<NxAppManager>>();

        /// <summary>
        /// Reference to Library Data
        /// </summary>
        public LibraryData libraryData;

        /// <summary>
        /// Reference to AR Scene Data
        /// </summary>
        public ARSceneData arSceneData;
        
        /// <summary>
        /// The  length of a video recording.
        /// </summary>
        public float videoRecLength;

        /// <summary>
        /// The maximum length permissible for an audio recording. This is set to the length of the video after it is recorded.
        /// </summary>
        public float audioRecLengthMax;

        /// <summary>
        /// Data for population of import fail message.
        /// </summary>
        /// <remarks></remarks>
        public PopupDialogData importFailPopupData;

        /// <summary>
        /// Barry's VoxData.
        /// </summary>
        public VoxData barryData;
        
        /// <summary>
        /// Screamy's VoxData
        /// </summary>
        public VoxData screamyData;
        
        #endregion

        #region CustomMethods

        /// <summary>
        /// Returns data for given vox.
        /// </summary>
        /// <param name="_name">The name of the desired Vox</param>
        /// <returns>The Vox's Data</returns>
        public  VoxData GetVox(VoxName _name)
        {
            switch (_name)
            {
                case VoxName.Barry:
                    return barryData;
                case VoxName.Screamy:
                    return screamyData;
                default:
                    return null;
            }
        }
 
        /// <summary>
        /// Adds a State to the stack for destinations for the back button.
        /// </summary>
        /// <param name="backState">The State to add</param>
        public void PushBackState(NxAppState<NxAppManager> backState)
        {
            Debug.Log("push back stack: "+ backState);

            backStack.Push(backState);
        }

        /// <summary>
        /// Pops a state off the stack, if one exists.
        /// </summary>
        /// <returns>The popped State</returns>
        public NxAppState<NxAppManager> PopBackState()
        {
            if (backStack.Count > 0)
            {
                Debug.Log("pop back stack: "+ backStack.Peek());
                return backStack.Pop();
            }
            else 
                return null;
        }
        
        /// <summary>
        /// Clears the back stack.
        /// </summary>
        public void ClearBackStack()
        {
            Debug.Log("clear back stack");
            backStack.Clear();
        }

        #endregion

        #region RequiredMethods
        /// <summary>
        /// Called as soon as the app has been initialized and the state machine is ready. 
        /// </summary>
        public override void Initialize()
        {
        }
        #endregion
    }
}
