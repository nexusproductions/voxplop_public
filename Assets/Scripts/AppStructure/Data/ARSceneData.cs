using UnityEngine;
using NxAppStructure;

using UnityEngine.Events;

using NexusStudios.VoxPlop;
using UnityEngine.XR.ARFoundation;

namespace AppStructure.Data
{
    /// <summary>
    /// A UnityEvent that carries a float.
    /// </summary> <see cref="ARSceneData.PitchChangedEvent"/>
    public class FloatEvent:UnityEvent<float>{};

    /// <summary>
    /// A UnityEvent that carries a bool.
    /// </summary> <see cref="ARSceneData.PlaneHiddenChangedEvent"/>
    public class BoolEvent:UnityEvent<bool>{};
    
    /// <summary>
    /// A UnityEvent that carries a string.
    /// </summary>
    public class StringEvent:UnityEvent<string>{};

    /// <summary>
    /// A UnityEvent that carries an ARSessionState.
    /// </summary>
    public class ARStateEvent : UnityEvent<ARSessionState>{};

    /// <summary>
    /// ARSceneData contains references to events and variables necessary for the states that use AR, RecordingRaw and PloppingVox. Most importantly, it maintains the current Sidecar, which hosts the data for the current scene.
    /// </summary>
    [CreateAssetMenu(fileName = "ARSceneData", menuName = "Nexus/Instance/ARSceneData", order = 1)]
    public class ARSceneData : NxAppData
    {
        #region Variables
        /// <summary>
        /// Invoked to begin recording in current active state, be it RecordingRaw or RecordingAudio
        /// </summary>
        [HideInInspector]
        public UnityEvent StartRecordingEvent;

        /// <summary>
        /// Invoked to stop recording in current active state, be it RecordingRaw or RecordingAudio
        /// </summary>
        [HideInInspector]
        public UnityEvent StopRecordingEvent;

        /// <summary>
        /// Invoked to begin playback in current active state, be it RecordingRaw or RecordingAudio
        /// </summary>
        /// TODO: Is this used?
        [HideInInspector]
        public UnityEvent StartPlaybackEvent;

        /// <summary>
        /// Invoked to stop playback in current active state, be it RecordingRaw or RecordingAudio
        /// </summary>
        [HideInInspector]
        public UnityEvent StopPlaybackEvent;

        /// <summary>
        /// Invoked when pitch is changed by <see cref="SetPitch(float)"/> to update UI and Vox AudioSource pitch.
        /// </summary>
        [HideInInspector]
        public FloatEvent PitchChangedEvent;

        /// <summary>
        /// Invoked by ARRecorder to update relevant UI components about the current normalized progress of the recording, scaled between its minimum and maximum duration.
        /// </summary>
        [HideInInspector]
        public FloatEvent RecProgressUpdateEvent;


        /// <summary>
        /// Invoked by <see cref="SetPlanesHidden"/> to show and hide ARPlanes.
        /// </summary>
        [HideInInspector]
        public BoolEvent PlaneHiddenChangedEvent;


        /// <summary>
        /// Invoked to begin creation and sharing of a flattened video. 
        /// </summary>
        [HideInInspector]
        public UnityEvent StartFlatOutputEvent;

        /// <summary>
        /// Invoked to being creation and sharing of a spatial video (with sidecar).
        /// </summary>
        [HideInInspector]
        public UnityEvent StartSpatialOutputEvent;

        /// <summary>
        /// Invoked when creation of flat video is complete and ready to be shared.
        /// </summary>
        /// <remarks>could this use an ARSession callback instead?</remarks>
        [HideInInspector]
        public UnityEvent CompletedFlatOutputEvent;

        /// <summary>
        /// Invoked when playback of current spatial video is complete. Listened to when outputting flat or spatial videos.
        /// </summary>
        [HideInInspector]
        public UnityEvent CompletedPlaybackEvent;

        /// <summary>
        /// Invoked when a session becomes localized. Used to spawn objects in world space.
        /// </summary>
        [HideInInspector] 
        public UnityEvent ARSessionLocalizedEvent;

        /// <summary>
        /// Invoked after a video is saved. True if successful.
        /// </summary>
        [HideInInspector] 
        public BoolEvent SaveVideoResultEvent;

        /// <summary>
        /// Invoked when the ARSession stops recording.
        /// </summary>
        [HideInInspector]
        public UnityEvent StoppedRecordingEvent;

        /// <summary>
        /// Invoked when a vox is initially placed into a scene by the user.
        /// </summary>
        [HideInInspector] 
        public UnityEvent VoxPlacedEvent;

        /// <summary>
        /// Invoked when the ARSession enters either an initializing or localized state, allowing for the hiding of the loading screen.
        /// </summary>
        [HideInInspector] 
        public ARStateEvent SessionLoadedEvent;
        
        /// <summary>
        /// Invoked to save a video to the library.
        /// </summary>
        [HideInInspector]
        public StringEvent SaveVideoEvent;
        
        /// <summary>
        /// Invoked when a vox is initially placed into a scene by the user.
        /// </summary>
        [HideInInspector] 
        public UnityEvent RecordingErrorEvent;

        [HideInInspector] 
        public UnityEvent PlaybackErrorEvent;

        /// <summary>
        /// The index of the currently active (being placed / manipulated) vox in the sidecar.
        /// </summary> 
        public int activeVoxIndex;

        /// <summary>
        /// The name of the currently playing spatial video.
        /// </summary>
        /// TODO: don't forget to look into a better way of triggering playback when entering RecordingRaw
        public string currentFilename;

        /// <summary>
        /// Holds the time that remains on the AR video playback. 
        /// </summary>
        public float playbackTimeRemaining;
        
        /// <summary>
        /// Holds the timer for the AR video playback. 
        /// </summary>
        public float playbackTimer;
        
        /// <summary>
        /// Current status of ARPlane visibility, set by <see cref="SetPlanesHidden"/>.
        /// </summary>
        public bool planesHidden { get; private set; }

        /// <summary>
        /// Current VoxSidecar, managing vox data (type, pitch, placement, etc.) for current scene.
        /// </summary>
        public VoxSidecar sidecar;
        
        /// <summary>
        /// Indicates if the current temp file is "dirty" and should warn user of data loss on exit.
        /// </summary>
        public bool fileIsSaved;

        /// <summary>
        /// The smallest size of a Vox
        /// </summary>
        public float VoxMinScale = 0.2f;
        
        /// <summary>
        /// Min distance of plopped vox from camera (used to calculate vox scale in scene) 
        /// </summary>
        public float VoxMinDistance = 0.3f;
        
        /// <summary>
        /// The maximum size of a Vox
        /// </summary>
        public float VoxMaxScale = 0.4f;
        
        /// <summary>
        /// Max distance of plopped vox from camera (used to calculate vox scale in scene) 
        /// </summary>
        public float VoxMaxDistance = 1f;
        
        
        #endregion

        #region CustomMethods
        /// <summary>
        /// Sets voice pitch / speed for current active vox.
        /// </summary>
        /// <param name="_pitch">The desired pitch, where 1.0 is normal speed.</param>
        public void SetPitch(float _pitch){
            sidecar.voxes[activeVoxIndex].pitch = _pitch;
            PitchChangedEvent.Invoke(sidecar.voxes[activeVoxIndex].pitch);
        }

        /// <summary>
        /// Sets visibilty of ARPlanes in scene.
        /// </summary>
        /// <param name="visible">Whether or not planes should be visible.</param>
        public void SetPlanesHidden(bool hidden){
            Debug.Log("Setting planes hidden: " + hidden);
            planesHidden = hidden;
            PlaneHiddenChangedEvent.Invoke(planesHidden);
        }
        
        /// <summary>
        /// Resets data to naive state, ready for content creation / loading.
        /// </summary>
        public void Reset()
        {
            currentFilename = "";
            sidecar.Clear();
            planesHidden = true;
            activeVoxIndex = 0;

        }
        #endregion

        #region RequiredMethods
        /// <summary>
        /// Called as soon as the app has been initialized and the state machine is ready. 
        /// </summary>
        public override void Initialize()
        {
            // Set up custom events.
            PitchChangedEvent = new FloatEvent();
            RecProgressUpdateEvent = new FloatEvent();
            PlaneHiddenChangedEvent = new BoolEvent();
            SaveVideoEvent = new StringEvent();
            SaveVideoResultEvent = new BoolEvent();
            SessionLoadedEvent = new ARStateEvent();
            
            Debug.Log("sceneData initialized");
        }
        #endregion
    }
}
