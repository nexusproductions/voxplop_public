using UnityEngine;
using NxAppStructure;
using UnityEngine.Events;

namespace AppStructure.Data
{
    /// <summary>
    /// Stores data to populate informational popups.
    /// </summary>
    [CreateAssetMenu(fileName = "PopupDialogData", menuName = "Nexus/Instance/PopupDialogData", order = 1)]
    public class PopupDialogData : NxAppData
    {
        #region Variables

        /// <summary>
        /// The text content of the popup
        /// </summary>
        public string body;

        /// <summary>
        /// The transparency of the popup's background 
        /// </summary>
        public float bg_alpha = 1f;
        
        /// <summary>
        /// For how many seconds should the popup appear? "-1" disables automatic closure.
        /// </summary>
        public float closeAfterSeconds = -1f;
        #endregion

        #region CustomMethods
        #endregion

        #region RequiredMethods
        /// <summary>
        /// Called as soon as the app has been initialized and the state machine is ready. 
        /// </summary>
        public override void Initialize()
        {
        }
        #endregion
    }
}
