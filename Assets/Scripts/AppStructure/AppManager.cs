using System;
using AppStructure.Data;
using UnityEngine;

using System.IO;
using AppStructure.States;
using AppStructure.Views;
using NexusStudios.VoxPlop;

namespace AppStructure
{
    /// <summary>
    /// The AppManager is the glue responsible for keeping key references and communicating between the different systems.
    /// </summary>
    public class AppManager : NxAppManager
    {
        #region Variables

        /// <summary>
        /// How many times the user arrived at the main menu
        /// </summary>
        public static string PerfsKey_MainCounter = "PerfsKey_MainCounter";

        private static int mainCounter = 0;
        /// <summary>
        /// The current intent, stored on app start or refocus.
        /// </summary>
        private string intentArgument;

        /// <summary>
        /// Is the user currently pressing the back button?
        /// </summary>
        private bool bEscapeDown;
        #endregion

        #region CustomMethods

        public static bool ShowTutorial()
        {
            int globalMainCounter = PlayerPrefs.GetInt(PerfsKey_MainCounter, 0);

            Debug.Log("ShowTutorial? mainCounter:" + mainCounter + " globalMainCounter:"+globalMainCounter);
            if (mainCounter == 0 && globalMainCounter == 0) return true; // First time running app, pre main screen
            if (mainCounter == 1 && globalMainCounter == 1) return true; // First time running app, on or post main screen
            Debug.Log(" Don't show tutorial");
            return false;
        }

        public static void IncrementMainMenuCounter()
        {
            mainCounter++;
            PlayerPrefs.SetInt(PerfsKey_MainCounter, PlayerPrefs.GetInt(PerfsKey_MainCounter, 0) + 1);
            PlayerPrefs.Save();
        }
        
        /// <summary>
        /// Return to the previous state / location. Called on Android back button press.
        /// </summary>
        public void GoBack()
        {
            var sessionData = (GlobalSessionData) GlobalAppData;
            
            var targetState = sessionData.PopBackState()?.GetType();
            Debug.Log("Going back: " + targetState);

            if (targetState == typeof(MainMenuState))
            {
                ChangeState<MainMenuState>();
            }
            else if (targetState == typeof(MediaLibraryState))
            {
                ChangeState<MediaLibraryState>();
            }
            else if (targetState == typeof(RecordingRawState))
            {
                ChangeState<RecordingRawState>();
            }
            else if (targetState == typeof(RecordingAudioState))
            {
                ChangeState<RecordingAudioState>();
            }
            else if (targetState == typeof(PloppingVoxState))
            {
                ChangeState<PloppingVoxState>();
            }
            else
            {
                ChangeState<MainMenuState>();
            }

            // discard current state so it doesn't add to the stack
            var leftState = sessionData.PopBackState();
            Debug.Log("went back from " + leftState);

        }




        /// <summary>
        /// Checks Android intent variable to see if it is attempting to load an mp4. If so, import it to the library and open it in the correct scene.
        /// </summary>
        private LibraryItem GetItemFromIntent()
        {
            // get intent URL and return an item if possible.

            Debug.Log("checking intent.");
            AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");

            var newArgument = intent.Call<string>("getDataString");
            Debug.Log("intent URI:" + intent.Call<string>("toUri", 0).ToString());
            Debug.Log("argument:" + newArgument);

            // update cached argument to prevent retrigger on resume
            if (newArgument != intentArgument)
            {
                intentArgument = newArgument;

                // TODO: how can we check extension with SAF? mail returns a number in URL not a filename.
                if (true)
                {
                    return LibraryManager.ImportItem(newArgument);

                }
                else
                {
                    Debug.Log("file's not a .plop!");
                    var sessionData = (GlobalSessionData) GlobalAppData;
                    ViewController.OpenOverlay<PopupDialogView>(sessionData.importFailPopupData, true);
                    return new LibraryItem();
                }
            }
            else
            {
                Debug.Log("no new argument.");
                // update cached argument to prevent retrigger on resume
                intentArgument = newArgument;
                // library item is type none by default
                return new LibraryItem();
            }
            
        }

        /// <summary>
        /// If App is returning to foreground, check intent.
        /// </summary>
        /// <param name="focusStatus"></param>
        void OnApplicationFocus(bool focusStatus)
        {
            if (focusStatus)
            {
                CheckIntentAndOpen();
            }
        }

        
        /// <summary>
        /// Opens item if intent is new and can create a valid item.
        /// </summary>
        /// TODO: could be refactored to make cleaner with GetItemFromIntent
        private void CheckIntentAndOpen()
        {
            if(LibraryManager.globalData == null)
                LibraryManager.globalData = (GlobalSessionData) GlobalAppData;
            if(VoxSidecar.globalData == null)
                VoxSidecar.globalData = (GlobalSessionData) GlobalAppData;

#if UNITY_EDITOR

#elif UNITY_ANDROID
            var newItem = GetItemFromIntent();
            if (newItem.type != LibraryItemType.None){
                LibraryManager.OpenItem(newItem);
                if (newItem.type == LibraryItemType.RawVideo)
                {
                    ChangeState<RecordingRawState>();

                }
                else if (newItem.type == LibraryItemType.PloppedVideo_Solo ||
                         newItem.type == LibraryItemType.PloppedVideo_Multiple)
                {
                    ChangeState<PloppingVoxState>();
                }
            }
            else 
            {

            }
#endif
        }

        /// <summary>
        /// Clean up temp files on quit
        /// </summary>
        private void OnApplicationQuit()
        {
            Debug.Log("Quitting...");
            LibraryManager.RemoveTempFiles();
        }

        #endregion

        #region RequiredMethods

        // NxAppManager initializes the state machine in Awake, so we have to call base.Awake() and use override.
        public override void Awake()
        {
            base.Awake();
            
            // if user has done the tutorial, go ahead to the main menu, or wherever the intent leads if there is one.
            if (!ShowTutorial())
            {
#if UNITY_EDITOR
                ChangeState<MainMenuState>();
#elif UNITY_ANDROID
                ChangeState<MainMenuState>();
                // check intent on first start
                CheckIntentAndOpen();
#endif
            }

            // setup static refs to current global data
            LibraryManager.globalData = (GlobalSessionData) GlobalAppData;
            VoxSidecar.globalData = (GlobalSessionData) GlobalAppData;
        }

        // Start is not used by NxAppManager, but you may use it and any other Monobehaviour methods you like.
        public void Start()
        {
        }

        // NxAppManager calls on the state machine to update the current time in Update, so we have to call base.Update and use override
        public override void Update()
        {
            base.Update();

            // watch for user pressing the back button. Go back if so.
            if (Input.GetKeyDown(KeyCode.Escape) && !bEscapeDown)
            {
                GoBack();
                Debug.Log("Back Button.");
                bEscapeDown = true;
            }
            else
            {
                bEscapeDown = false;
            }
        }
        #endregion


    }
}