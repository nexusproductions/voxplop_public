using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Loju.View;

using AppStructure;
using AppStructure.States;

namespace AppStructure.Views
{
    

    /// <summary>
    /// Handles the main menu view. Responds to button clicks.
    /// </summary>
    public class MainMenuView : AbstractView 
    {
        #region Variables

        /// <summary>
        /// The button used to make a new plop
        /// </summary>
        public Button CreatePlopButton;
        
        /// <summary>
        /// The object that holds all the first-time-user hints
        /// </summary>
        public GameObject Hints;

        #endregion

        #region CustomMethods
        /// <summary>
        /// Called on button click. Displays info on first run, then loads create vox scene on subsequent runs.
        /// </summary>
        public void OnCreateVoxButton()
        {
            // if user has seen the spatial video notice, continue to record, otherwise, show it.
            if (PlayerPrefs.HasKey("UserPassedSpatialVideoInfo"))
            {
                var appManager = (AppManager) _controller.NxAppManager;
                appManager.ChangeState<RecordingRawState>();
            }
            else
                ChangeLocation<SpatialVideoView>();
        }

        /// <summary>
        /// Called on button click. Displays library.
        /// </summary>
        public void OnLoadButton()
        {
            var appManager = (AppManager) _controller.NxAppManager;
            appManager.ChangeState<MediaLibraryState>();
        }

        #endregion

        #region RequiredMethods
        protected override void OnCreate()
        {
            AppManager.IncrementMainMenuCounter();

            bool showTut = AppManager.ShowTutorial();
 
            Hints.SetActive(showTut);
            CreatePlopButton.interactable = !showTut;
        }

        protected override void OnShowStart(object data)
        {
            // handle transitioning in to view

            OnShowComplete();
        }

        protected override void OnHideStart()
        {
            // handle transitioning out from view

            OnHideComplete();
        }
        #endregion
    }
}