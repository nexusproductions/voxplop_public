using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Loju.View;

using NexusStudios.VoxPlop;
using AppStructure.Data;

namespace AppStructure.Views
{
    /// <summary>
    /// Handles view dialog for sharing videos.
    /// </summary>
    public class ShareVideoView : AbstractView 
    {
        #region Variables

        /// <summary>
        /// The scene data
        /// </summary>
        private ARSceneData sceneData;

        /// <summary>
        /// The popup data to display while embedding a sidecar in a video
        /// </summary>
        public PopupDialogData embeddingPopupData;
        
        /// <summary>
        /// The plopping vox view that opened the dialog
        /// </summary>
        public PloppingVoxView parentView;
        #endregion

        #region CustomMethods
        

        
        /// <summary>
        /// Called on button click. Renders flattened version of video.
        /// </summary>
        public void OnShareFlatButtonPress(){
            // Removed for Open Source Repo //
            Debug.LogError("Attempt to share flat video");
        }

        /// <summary>
        /// Called on button click. Saves and embeds sidecar, shares video.
        /// </summary>
        public void OnShareSpatialButtonPress() {
            //show overlay
            _controller.OpenOverlay<PopupDialogView>(embeddingPopupData,true);

            // TODO: I don't like having to ref the parent.
            parentView.outputMode = OutputMode.ShareSpatialEmbedded;

            // serialize media (audioClip->wav->ogg->bytes)
            sceneData.sidecar.SerializeMedia();

            // play video, record as new session, flagged to add sidecar
            sceneData.StartSpatialOutputEvent.Invoke();

            // on completion, share
            sceneData.CompletedPlaybackEvent.AddListener(parentView.OnSpatialPlaybackCompleted);
            
            sceneData.fileIsSaved = true;
        }
        
        /// <summary>
        /// Closes overlay, called on cancel button press.
        /// </summary>
        public void OnCancelButtonPress()
        {
            _controller.CloseOverlay<ShareVideoView>();
        }
        #endregion

        #region RequiredMethods
        protected override void OnCreate()
        {
            // view setup
        }

        protected override void OnShowStart(object data)
        {
            // handle transitioning in to view
            sceneData = (ARSceneData) data;
            
            OnShowComplete();
        }

        protected override void OnHideStart()
        {
            // handle transitioning out from view

            OnHideComplete();
        }
        #endregion
    }
        
}