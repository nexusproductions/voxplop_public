using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Loju.View;

using AppStructure;
using AppStructure.States;
using AppStructure.Data;
using TMPro;


namespace AppStructure.Views
{
    /// <summary>
    /// Handles pitching audio view.
    /// </summary>
    /// <remarks>Currently unused.</remarks>
    public class PitchAudioView : AbstractView 
    {
        #region Variables
        ARSceneData sceneData;

        /// <summary>
        /// The slider for controlling pitch of audio clip
        /// </summary>
        public Slider pitchSlider;
        
        /// <summary>
        /// The Vox's name, displayed as the title of the view.
        /// </summary>
        public RawImage title;
        
        /// <summary>
        /// The subtitle, a description of the Vox.
        /// </summary>
        public TMP_Text description;
        
        #endregion

        #region CustomMethods
        /// <summary>
        /// Called on button click. Goes to record audio view for rerecording.
        /// </summary>
        public void OnBackButtonPress()
        {
           ChangeLocation<RecordingAudioView>(sceneData);
        }

        /// <summary>
        /// Called on slider move. Changes pitch in sceneData.
        /// </summary>
        /// <param name="value">The new pitch value</param>
        public void OnSliderMove(float value)
        {
            sceneData.SetPitch(value);
        }

        /// <summary>
        /// Called on button click. Confirms and continues to next state.
        /// </summary>
        public void OnFinishButtonPress()
        {
            var appManager = (AppManager) _controller.NxAppManager;
            appManager.ChangeState<PloppingVoxState>();
        }
        #endregion

        #region RequiredMethods
        protected override void OnCreate()
        {
            // view setup
        }

        protected override void OnShowStart(object data)
        {
            // handle transitioning in to view
            sceneData = (ARSceneData) data;
            Debug.Log("showing pitch audio view with "+ sceneData.currentFilename);

            // setup UI components based on sidecar data.
            pitchSlider.value = sceneData.sidecar.voxes[sceneData.activeVoxIndex].pitch;
            title.texture = sceneData.sidecar.voxes[sceneData.activeVoxIndex].voxData.titleImage;
            description.text = sceneData.sidecar.voxes[sceneData.activeVoxIndex].voxData.description;
            
            OnShowComplete();
        }

        protected override void OnHideStart()
        {
            // handle transitioning out from view

            OnHideComplete();
        }
        #endregion
    }
}