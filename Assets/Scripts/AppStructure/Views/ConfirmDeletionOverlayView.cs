using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Loju.View;

using AppStructure.Data;
using NexusStudios.VoxPlop;

namespace AppStructure.Views
{

    /// <summary>
    /// Manages view form confirming deletion of videos in library.
    /// </summary>
    public class ConfirmDeletionOverlayView : AbstractView
    {
        #region Variables

        /// <summary>
        /// The data for populating the view.
        /// </summary>
        private ConfirmDeletionOverlayData viewData;

        #endregion

        #region CustomMethods

        /// <summary>
        /// Called on press of confirmation button. Triggers Delete action on parent view.
        /// </summary>
        public void OnConfirmButton()
        {
            viewData.parentView.ConfirmDeletion();

            _controller.CloseOverlay<ConfirmDeletionOverlayView>();
        }

        /// <summary>
        /// Called on press of cancel button. Closes view.
        /// </summary>
        public void OnCancelButton()
        {
            viewData.parentView.CancelDeletion();

            _controller.CloseOverlay<ConfirmDeletionOverlayView>();
        }

        #endregion

        #region RequiredMethods

        protected override void OnCreate()
        {
        }

        protected override void OnShowStart(object data)
        {
            // handle transitioning in to view
            viewData = (ConfirmDeletionOverlayData) data;
            OnShowComplete();
        }

        protected override void OnHideStart()
        {
            // handle transitioning out from view
            OnHideComplete();
        }

        #endregion
    }

}
