using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Loju.View;

using AppStructure;
using AppStructure.States;
using AppStructure.Data;
using TMPro;
using UnityEngine.Serialization;
using UnityEngine.XR.ARFoundation;

namespace AppStructure.Views
{
    /// <summary>
    /// Handles view for UI during recording raw video state.
    /// </summary>
    public class RecordRawView : AbstractView 
    {
        #region Variables
        /// <summary>
        /// Scene data.
        /// </summary>
        ARSceneData sceneData;

        /// <summary>
        /// The UI element for displaying recording progress. Set in inspector.
        /// </summary>
        public Image progressBar;

        /// <summary>
        /// If the user has started recording an AR session.
        /// </summary>
        private bool bStartedRecording = false;
        
        /// <summary>
        /// The numerical countdown UI element.
        /// </summary>
        public TMP_Text countdown;
        
        /// <summary>
        /// The record button UI element.
        /// </summary>
        public GameObject recButton;

        public GameObject recButtonContainer;
        
        /// <summary>
        /// The instructions for the user to record.
        /// </summary>
        public TMP_Text instructions;
        
        /// <summary>
        /// The UI element for AR Plane visibility toggle. Set in inspector.
        /// </summary>
        [FormerlySerializedAs("planesVisibleToggle")] 
        public Toggle planesHiddenToggle;

        /// <summary>
        /// A copy of the waiting UI, to cover up some camera wonkiness 
        /// </summary>
        public GameObject JustASec;
        #endregion

        #region CustomMethods
        /// <summary>
        /// Called on button click. Returns to previous state.
        /// </summary>
        public void OnBackButtonPress()
        {
            // also may need to go back to library.
            var appManager = (AppManager) _controller.NxAppManager;
            appManager.ChangeState<MainMenuState>();
        }

        /// <summary>
        /// Called on button down. Invokes recording event.
        /// </summary>
        public void OnRecButtonPress()
        {
            Debug.Log("rec button down");
            
            if (!bStartedRecording)
            {
                sceneData.StartRecordingEvent.Invoke();
                sceneData.StoppedRecordingEvent.AddListener(OnStoppedRecording);
                
                recButton.SetActive(false);
                countdown.gameObject.SetActive(true);
                bStartedRecording = true;
            }


        }

        /// <summary>
        /// Called on button release. Invokes recording stop event and procedes to review view.
        /// </summary>
        public void OnRecButtonRelease()
        {
            Debug.Log("rec button up");

        }

        /// <summary>
        /// Continues to the Review view when user completes recording.
        /// </summary>
        public void OnStoppedRecording()
        {
            Debug.Log("Done recording. moving on.");
            sceneData.StartPlaybackEvent.Invoke();
            _controller.ChangeLocation<ReviewRawView>(sceneData,false);
            sceneData.StoppedRecordingEvent.RemoveListener(OnStoppedRecording);

        }

        /// <summary>
        /// Called by progress update event. Sets image fill based on progress.
        /// </summary>
        /// <param name="progress">Normalized recording progress</param>
        void OnRecProgressUpdate(float progress)
        {
            progressBar.fillAmount = progress;
            var appManager = (AppManager) _controller.NxAppManager;
            var globalData = (GlobalSessionData) appManager.GlobalAppData;
            var length = globalData.videoRecLength;
            countdown.text = $"{Mathf.CeilToInt(length - (progress * length))}";
        }

        /// <summary>
        /// UI element for AR Plane visibility toggle.
        /// </summary>
        /// <param name="visible">Should the planes be visible? </param>
        public void OnSetPlanesHiddenToggle(bool hidden)
        {
            sceneData.SetPlanesHidden(hidden);
        }

        /// <summary>
        /// Wait for localization to show rec button- ensures session is active to prevent bad records.
        /// </summary>
        public void OnSessionLoaded()
        {
            recButtonContainer.SetActive(true);
        }

        /// <summary>
        /// The "JustASec" object covers up some camera wonkiness (black frames or flicking)
        /// </summary>
        IEnumerator hideJustASec()
        {
            yield return new WaitForSeconds(2f);
            JustASec.SetActive(false);
        }
        #endregion

        #region RequiredMethods
        protected override void OnCreate()
        {
            
        }

        protected override void OnShowStart(object data)
        {
            // handle transitioning in to view
            //cast
            sceneData = (ARSceneData) data;

            // stop playback when entering this view. 
            sceneData.StopPlaybackEvent.Invoke();

            // listen for recorder progress
            sceneData.RecProgressUpdateEvent.AddListener(OnRecProgressUpdate);
            
            // listen for localization
            sceneData.ARSessionLocalizedEvent.AddListener(OnSessionLoaded);
            
            // set element to current state
            var appManager = (AppManager) _controller.NxAppManager;
            var globalData = (GlobalSessionData) appManager.GlobalAppData;
            instructions.text = $"Tap to record a {globalData.videoRecLength}\" video";
            
            // hide planes before record
            sceneData.SetPlanesHidden(true);

            OnShowComplete();
            StartCoroutine(hideJustASec());
        }

        protected override void OnHideStart()
        {
            // handle transitioning out from view

            // stop listening.
            sceneData.RecProgressUpdateEvent.RemoveListener(OnRecProgressUpdate);
            sceneData.ARSessionLocalizedEvent.RemoveListener(OnSessionLoaded);

            OnHideComplete();

        }
        #endregion
    }
    
}