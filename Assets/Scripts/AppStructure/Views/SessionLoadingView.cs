using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Loju.View;

namespace AppStructure.Views
{
    /// <summary>
    /// Handles the loading screen view.
    /// </summary>
    public class SessionLoadingView : AbstractView 
    {
        #region Variables
        #endregion

        #region CustomMethods
        #endregion

        #region RequiredMethods
        protected override void OnCreate()
        {
            // view setup
        }

        protected override void OnShowStart(object data)
        {
            // handle transitioning in to view

            OnShowComplete();
        }

        protected override void OnHideStart()
        {
            // handle transitioning out from view

            OnHideComplete();
        }
        #endregion
    }
}