using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using AppStructure.Data;
using AppStructure.States;
using Loju.View;
using NexusStudios.VoxPlop;
using TMPro;

namespace AppStructure.Views
{
    

    /// <summary>
    /// Handles view to direct user to correct destination when opening a populated video.
    /// </summary>
    /// <remarks>Currently unused.</remarks>
    public class OpenPloppedVideoDialogView : AbstractView 
    {
        #region Variables

        /// <summary>
        /// The text of the dialog.
        /// </summary>
        public TMP_Text bodyText;
        
        /// <summary>
        /// The UI element for the vox currently in the video.
        /// </summary>
        public RawImage voxImage;
        
        /// <summary>
        /// The text for the button to add a vox to the video.
        /// </summary>
        public TMP_Text addButtonText;

        /// <summary>
        /// The currently selected library item being opened.
        /// </summary>
        private LibraryItem itemSelected;

        /// <summary>
        /// Barry's VoxData.
        /// </summary>
        private VoxData barryData;
        
        /// <summary>
        /// Screamy's VoxData.
        /// </summary>
        private VoxData screamyData;
        #endregion

        #region CustomMethods

        /// <summary>
        /// Called on Add Vox Button press. Begins creation of new vox, transitions into recording audio state.
        /// </summary>
        public void OnAddButtonPress()
        {
            var appManager = (AppManager) _controller.NxAppManager;
            var globalData = (GlobalSessionData) appManager.GlobalAppData;
            var newVox = new VoxSidecarData();
            
            LibraryManager.OpenItem(itemSelected);

            if (itemSelected.embeddedVox.voxName == VoxName.Barry)
                newVox.voxData = screamyData;
            else
                newVox.voxData = barryData;
            globalData.arSceneData.sidecar.voxes.Add(newVox);
            globalData.arSceneData.activeVoxIndex = 1;
            
            appManager.ChangeState<RecordingAudioState>();
            _controller.CloseOverlay<OpenPloppedVideoDialogView>();

        }

        /// <summary>
        /// Opens Selected item in Plopping Vox State.
        /// </summary>
        public void OnViewButtonPress()
        {
            LibraryManager.OpenItem(itemSelected);
            var appManager = (AppManager) _controller.NxAppManager;
            appManager.ChangeState<PloppingVoxState>();
            _controller.CloseOverlay<OpenPloppedVideoDialogView>();

        }

        /// <summary>
        /// Closes overlay.
        /// </summary>
        public void OnCancelButtonPress()
        {
            _controller.CloseOverlay<OpenPloppedVideoDialogView>();
        }

        /// <summary>
        /// Sets text of popup to account for vox in the video.
        /// </summary>
        private void SetBodyText()
        {
            var hasVox = "";
            var addVox = "";

            if (itemSelected.embeddedVox.voxName == VoxName.Barry)
            {
                hasVox = "Barry";
                addVox = "Screamy";
            }
            else
            {
                hasVox = "Screamy";
                addVox = "Barry";
            }
            
            bodyText.text = $"This video has {hasVox} in it! Would you like to add {addVox} or just check it out?";
        }

        /// <summary>
        /// Sets button text to indicate appropriate vox to add.
        /// </summary>
        private void SetAddButtonText()
        {
            if (itemSelected.embeddedVox.voxName == VoxName.Barry)
            {
                addButtonText.text = "Add Screamy";
            }
            else
            {
                addButtonText.text = "Add Barry";

            }
        }
        
        #endregion

        #region RequiredMethods
        protected override void OnCreate()
        {
            // view setup
        }

        protected override void OnShowStart(object data)
        {
            // handle transitioning in to view
            var appManager = (AppManager) _controller.NxAppManager;
            var globalData = (GlobalSessionData) appManager.GlobalAppData;
            
            barryData = globalData.barryData;
            screamyData = globalData.screamyData;
            
            // setup UI components
            itemSelected = (LibraryItem) data;
            SetBodyText();
            voxImage.texture = itemSelected.embeddedVox.heroImage;
            SetAddButtonText();
            
            OnShowComplete();
        }

        protected override void OnHideStart()
        {
            // handle transitioning out from view

            OnHideComplete();
        }
        #endregion
    }
}