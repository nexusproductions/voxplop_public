using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Loju.View;
using AppStructure;
using AppStructure.States;
using UnityEngine.Android;

namespace AppStructure.Views
{
    /// <summary>
    /// Handles the informational view displayed on first app startup.
    /// </summary>
    public class IntroPermissionsView : AbstractView 
    {
        #region Variables
        /// <summary>
        /// Permissions to request.
        /// </summary>
        string[] permissions = {Permission.Camera, Permission.Microphone};

        #endregion

        #region CustomMethods

        /// <summary>
        /// Called on button click. Gets permissions and continues to main menu.
        /// </summary>
        public void OnContinueButton()
        {
            GetPermissions();
        }

        /// <summary>
        /// Called on button click. Opens browser to more information about VoxPlop.
        /// </summary>
        public void OnLearnMoreButton()
        {
            Application.OpenURL("http://www.voxplop.com");
        }

        /// <summary>
        /// Requests all applicable permissions and setups callbacks. Just continues if in editor.
        /// </summary>
        private void GetPermissions()
        {
    #if UNITY_ANDROID && !UNITY_EDITOR
            var callbacks = new PermissionCallbacks();
            callbacks.PermissionGranted += OnPermissionGranted;
            callbacks.PermissionDenied += OnPermissionsDenied;
            callbacks.PermissionDeniedAndDontAskAgain += OnPermissionDeniedAndDontAsk;

            if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
                Permission.RequestUserPermissions(permissions, callbacks);

            if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
                Permission.RequestUserPermissions(permissions, callbacks);
                 
            if (Permission.HasUserAuthorizedPermission(Permission.Camera) && Permission.HasUserAuthorizedPermission(Permission.Microphone))  
                Continue();
            
    #else
            Continue();
    #endif

        }

        /// <summary>
        /// Continues to the onboarding view
        /// </summary>
        private void Continue()
        {
            var appManager = (AppManager) _controller.NxAppManager;
            appManager.ChangeState<MainMenuState>();

        }

        /// <summary>
        /// Displays Permissions fail view.
        /// </summary>
        private void DisplayPermissionsError()
        {
            Debug.LogWarning("Permissions fail!");
            _controller.ChangeLocation<PermissionsFailView>(null, true);
        }

        /// <summary>
        /// Confirms user has all applicable permissions
        /// </summary>
        /// <returns>True if user has all necessary permissions.</returns>
        private bool HasAllPermissions()
        {
            // loop through and check all permissions. If we fail on any return false;
            foreach (var permission in permissions)
            {
                if (!Permission.HasUserAuthorizedPermission(permission))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Called when a permission is granted. Confirms user has all permissions and continues if so.
        /// </summary>
        /// <param name="permission"></param>
        private void OnPermissionGranted(string permission)
        {
            // continue if we've got everything we need:
            if (HasAllPermissions())
            {
                Continue();
            }
            else
            {
                DisplayPermissionsError();

            }

        }

        /// <summary>
        /// Called on permission denial. Displays fail view.
        /// </summary>
        /// <param name="permission"></param>
        private void OnPermissionsDenied(string permission)
        {
            DisplayPermissionsError();
        }

        /// <summary>
        /// Called on permissions permanently denied. Displays fail view.
        /// </summary>
        /// <param name="permission"></param>
        private void OnPermissionDeniedAndDontAsk(string permission)
        {
            DisplayPermissionsError();
        }
        #endregion

        #region RequiredMethods
        protected override void OnCreate()
        {
            // view setup
        }

        protected override void OnShowStart(object data)
        {
            // handle transitioning in to view

            OnShowComplete();
        }

        protected override void OnHideStart()
        {
            // handle transitioning out from view

            OnHideComplete();
        }
        #endregion
    }
}