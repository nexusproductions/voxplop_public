using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Loju.View;

using AppStructure;
using AppStructure.States;
using AppStructure.Data;

using UnityEngine.UI;

using NexusStudios.VoxPlop;
using System.Linq;
using DG.Tweening;
using UnityEngine.XR;

namespace AppStructure.Views
{
    public enum OutputMode
    {
        
        ShareFlat,
        ShareSpatialEmbedded,
        SaveSpatialToLibrary
    }
    /// <summary>
    /// Handles UI in PloppingVox state.
    /// </summary>
    public class PloppingVoxView : AbstractView 
    {
        #region Variables
        /// <summary>
        /// Scene data reference.
        /// </summary>
        ARSceneData sceneData;

        /// <summary>
        /// UI element for active vox pitch control.
        /// </summary>
        public Slider pitchSlider;

        /// <summary>
        /// UI element for AR Plane visibility toggle.
        /// </summary>
        public Toggle planesVisibleToggle;

        /// <summary>
        /// The popup content to display when embedding a sidecar.
        /// </summary>
        public PopupDialogData embeddingPopupData;
        
        /// <summary>
        /// The popup data to display after a successful write to the library.
        /// </summary>
        public PopupDialogData saveSuccessPopupData;
        
        /// <summary>
        /// The popup data to display on a failed write to the library.
        /// </summary>
        public PopupDialogData saveFailPopupData;

        /// <summary>
        /// What sort of share action has been called?
        /// </summary>
        public OutputMode outputMode;

        /// <summary>
        /// The UI element to add a vox to the scene.
        /// </summary>
        public Button addVoxButton;
        
        /// <summary>
        /// Barry's data.
        /// </summary>
        private VoxData barryData;
        
        /// <summary>
        /// Screamy's data.
        /// </summary>
        private VoxData screamyData;
        
        /// <summary>
        /// The last value at which the pitch slider provided a haptic response.
        /// </summary>
        private float lastTickSliderVal;
        
        /// <summary>
        /// The UI element that directs user to tap to place a vox.
        /// </summary>
        public GameObject instructionText;

        /// <summary>
        /// Container for the tutorial hints
        /// </summary>
        public CanvasGroup HintsRecording;

        /// <summary>
        /// Container for the tutorial hints
        /// </summary>
        public CanvasGroup HintsPlayback;

        
        private Tween hintsFade = null;
        private bool finishedLoading = false;
        private static int pageCounter = 0;
        #endregion

        #region CustomMethods
        /// <summary>
        /// Called on button click. Returns to previous state, with confirmation.
        /// </summary>
        public void OnBackButtonPress()
        {
            
            // if not saved, show dialogue
            if (!sceneData.fileIsSaved)
            {
                fadeOutHints();
                
                // create new dialogue payload
                var data = ScriptableObject.CreateInstance<DiscardRecordingDialogData>();
                
                // set it to appropriate destination
                data.ConfirmationDestination = DiscardRecordingDialogDestination.Back;

                // display the dialogue
                _controller.OpenOverlay<DiscardRecordingOverlayView>(data,true);
            }
            else
            {
                var appManager = (AppManager) _controller.NxAppManager;
                appManager.GoBack();

            }

        }

        /// <summary>
        /// Called on button click. Returns to main menu, with confirmation.
        /// </summary>
        public void OnCloseButtonPress()
        {

            // if not saved, show dialogue
            if (!sceneData.fileIsSaved)
            {
                // create new dialogue payload
                var data = new DiscardRecordingDialogData();

                // set destination
                data.ConfirmationDestination = DiscardRecordingDialogDestination.MainMenu;

                // show dialogue
                _controller.OpenOverlay<DiscardRecordingOverlayView>(data,true);
            }
            // otherwise, just go.
            else
            {
                var appManager = (AppManager) _controller.NxAppManager;
                appManager.ChangeState<MainMenuState>();
            }

        }

        /// <summary>
        /// Opens share dialog box. Called on button press.
        /// </summary>
        public void OnShareButtonPress()
        {
            fadeOutHints();
            _controller.OpenOverlay<ShareVideoView>(sceneData,true);
            var shareOverlay = (ShareVideoView) _controller.GetOverlay<ShareVideoView>();
            shareOverlay.parentView = this;
        }

        /// <summary>
        /// Returns to the recording audio screen and populates it with the unused vox. Called on button press.
        /// </summary>
        public void OnAddVoxButtonPress()
        {
            fadeOutHints();
            var voxList = sceneData.sidecar.voxes;
            if (voxList.Count == 1)
            {
                var newVox = new VoxSidecarData();

                if (voxList[0].voxData.voxName== VoxName.Barry)
                    newVox.voxData = screamyData;
                else
                    newVox.voxData = barryData;
                sceneData.sidecar.voxes.Add(newVox);
                sceneData.activeVoxIndex = 1;
            
                var appManager = (AppManager) _controller.NxAppManager;
                appManager.ChangeState<RecordingAudioState>();

            }
        }

        /// <summary>
        /// Called on button click. Saves and embeds sidecar. Saves to library.
        /// </summary>
        public void OnSaveButtonPress()
        {
            Debug.Log("save button pressed.");
            fadeOutHints();

            //show overlay
            _controller.OpenOverlay<PopupDialogView>(embeddingPopupData,true);

            outputMode = OutputMode.SaveSpatialToLibrary;
            // serialize media (audioClip->wav->ogg->bytes)
            sceneData.sidecar.SerializeMedia();

            // play video, record as new session, flagged to add sidecar
            sceneData.StartSpatialOutputEvent.Invoke();
            
            sceneData.CompletedPlaybackEvent.AddListener(OnSpatialPlaybackCompleted);

            sceneData.fileIsSaved = true;
        }

        /// <summary>
        /// Called on slider change. Sets active vox audio pitch.
        /// </summary>
        /// <param name="val">The new pitch</param>
        public void OnPitchSliderChange(float val)
        {
            fadeOutHints();
            if (Mathf.Abs(val - lastTickSliderVal) > 0.2)
            {
                lastTickSliderVal = val;
                Vibration.Vibrate(10);
            }
            sceneData.SetPitch(val);
        }

        /// <summary>
        /// Called on toggle click. Sets AR Plane Visibility.
        /// </summary>
        /// <param name="hidden">Should AR Planes be visible?</param>
        public void OnSetPlanesHiddenToggle(bool hidden)
        {
            fadeOutHints();
            sceneData.SetPlanesHidden(hidden);
        }

        /// <summary>
        /// Called by event, when video completes playback during rerecording for sidecar embed. Shares video, stops listening to event.
        /// </summary>
        public void OnSpatialPlaybackCompleted()
        {
            Debug.Log("Finished spatial playback...");

            // close overlays
            _controller.CloseAllOverlays();
            
            switch (outputMode)
            {
                case OutputMode.ShareFlat:
                    // shared by flatmovieRecorder.
                    break;
                case OutputMode.ShareSpatialEmbedded:
                    StartCoroutine(ShareVideoWithDelay(1f));
                    break;
                case OutputMode.SaveSpatialToLibrary:
                    StartCoroutine(SaveVideoWithDelay(1f));
                    break;
            }
            
            // mark file as saved
            sceneData.fileIsSaved = true;

            // stop listening to event
            sceneData.CompletedPlaybackEvent.RemoveListener(OnSpatialPlaybackCompleted);

        }

        /// <summary>
        /// Saves the video to the library after a given delay. Prevents read / write collision.
        /// </summary>
        /// <param name="secs">The duration of the delay, in seconds.</param>
        /// <returns></returns>
        private IEnumerator SaveVideoWithDelay(float secs)
        {
            yield return new WaitForSeconds(secs);
            sceneData.SaveVideoEvent.Invoke("embedded");
        }
        
        /// <summary>
        /// Shares the video to the library after a given delay. Prevents read / write collision.
        /// </summary>
        /// <param name="secs">The duration of the delay, in seconds.</param>
        /// <returns></returns>
        private IEnumerator ShareVideoWithDelay(float secs)
        {
            yield return new WaitForSeconds(secs);
            ShareManager.ShareSpatialVideo(sceneData.currentFilename);
        }

        /// <summary>
        /// Sets appearance of UI object based on change in visibility of AR planes 
        /// </summary>
        /// <param name="hidden">True if the planes are now hidden.</param>
        public void OnPlanesHiddenChange(bool hidden)
        {
            // for listening to change when placement is complete.
            planesVisibleToggle.isOn = hidden;
        }

        /// <summary>
        /// Called upon attempt to save video to library
        /// </summary>
        /// <param name="didSave">True if the save was successful</param>
        private void OnSaveResultEvent(bool didSave)
        {
            if(didSave)
                _controller.OpenOverlay<PopupDialogView>(saveSuccessPopupData, false);
            else
                _controller.OpenOverlay<PopupDialogView>(saveFailPopupData, false);
        }

        /// <summary>
        /// Called upon placement of vox. Makes add vox button visible.
        /// </summary>
        private void OnVoxPlacedEvent()
        {
            if (sceneData.sidecar.voxes.Count<2)
            {
                addVoxButton.gameObject.SetActive(true);
            }
        }
        #endregion

        /// <summary>
        /// Fade out the hints
        /// </summary>
        void fadeOutHints()
        {
            if (finishedLoading && hintsFade==null && HintsRecording.gameObject.activeSelf)
                hintsFade = HintsRecording.DOFade(0, 0.5f).OnComplete(() => HintsRecording.gameObject.SetActive(false));
            
            if (finishedLoading && hintsFade==null && HintsPlayback.gameObject.activeSelf)
                hintsFade = HintsPlayback.DOFade(0, 0.5f).OnComplete(() => HintsPlayback.gameObject.SetActive(false));
            
        }
        
        #region RequiredMethods
        protected override void OnCreate()
        {
            HintsRecording.gameObject.SetActive(false);
            HintsPlayback.gameObject.SetActive(false);
        }

        protected override void OnShowStart(object data)
        {
            // handle transitioning in to view
            var appManager = (AppManager) _controller.NxAppManager;
            var globalData = (GlobalSessionData) appManager.GlobalAppData;
            
            barryData = globalData.barryData;
            screamyData = globalData.screamyData;
            
            // cast
            sceneData = (ARSceneData)data;

            // set UI elements to match current values
            pitchSlider.value = sceneData.sidecar.voxes[sceneData.activeVoxIndex].pitch;
            planesVisibleToggle.isOn = sceneData.planesHidden;
            
            // set addVoxButton to reflect current sidecare state
            var voxList = sceneData.sidecar.voxes;
            if (voxList.Count >= 2)
            {
                addVoxButton.gameObject.SetActive(false);

                // get all magnitudes,
                var posMags =
                    from vox in voxList
                    select vox.position.magnitude;
                // if none are zero, both have been placed, hide tap-to-place instructions
                if (!posMags.Contains(0))
                    instructionText.SetActive(false);

            }
            // if only one vox, set UI button to match alternate vox.
            else if (voxList.Count == 1)
            {
                if (voxList[0].voxData.voxName == VoxName.Barry)
                    addVoxButton.image.sprite = screamyData.selectIcon;
                else
                {
                    addVoxButton.image.sprite = barryData.selectIcon;
                }
                if (voxList[0].position.magnitude == 0)
                {
                    // if vox isn't placed, hide button.
                    addVoxButton.gameObject.SetActive(false);
                }

            }
            
            // setup listeners.
            sceneData.PlaneHiddenChangedEvent.AddListener(OnPlanesHiddenChange);
            sceneData.SaveVideoResultEvent.AddListener(OnSaveResultEvent);
            sceneData.VoxPlacedEvent.AddListener(OnVoxPlacedEvent);
            
            OnShowComplete();
            
            // Maybe show one the hints overlays?
            if (AppManager.ShowTutorial())
            {
                pageCounter++;
                HintsRecording.gameObject.SetActive(pageCounter==1);
                HintsPlayback.gameObject.SetActive(pageCounter==2);
            }
            
            finishedLoading = true;
        }

        protected override void OnHideStart()
        {
            // handle transitioning out from view
            
            //cleanup
            sceneData.PlaneHiddenChangedEvent.RemoveListener(OnPlanesHiddenChange);
            sceneData.SaveVideoResultEvent.RemoveListener(OnSaveResultEvent);
            sceneData.VoxPlacedEvent.RemoveListener(OnVoxPlacedEvent);

            OnHideComplete();
        }
        
        
        
        #endregion
    }
    
}
