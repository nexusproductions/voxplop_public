using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Loju.View;

using AppStructure;
using AppStructure.States;
using AppStructure.Data;
using NexusStudios.VoxPlop;


namespace AppStructure.Views
{
    /// <summary>
    /// The overlay displayed when a user attempts to leave a state in which data is being created (i.e. RecordingRawState, PloppingVoxState).
    /// </summary>
    public class DiscardRecordingOverlayView : AbstractView 
    {
        #region Variables
        /// <summary>
        /// Where the user should go after confirmation
        /// </summary>
        private DiscardRecordingDialogDestination destination;
        #endregion

        #region CustomMethods
        /// <summary>
        /// Called on click of button. Simply closes view.
        /// </summary>
        public void OnNoButton(){
            _controller.CloseOverlay<DiscardRecordingOverlayView>();
        }

        /// <summary>
        /// Called on click of button. Sends user to <see cref="destination"/> state.
        /// </summary>
        public void OnYesButton(){
            // FIXME: this feels anti-AppStructure- is there a better way to access arSceneData?
            var globalData = (GlobalSessionData) _controller.NxAppManager.GlobalAppData;
            var appManager = (AppManager) _controller.NxAppManager;

            // behavior based on variable set on start.
            switch(destination){
                case DiscardRecordingDialogDestination.MainMenu:
                    // go to main menu, usually on "Close" button.
                    appManager.ChangeState<MainMenuState>();
                    break;
                case DiscardRecordingDialogDestination.RecordRaw:
                    // go to recording raw state
                    _controller.ChangeLocation<RecordRawView>(globalData.arSceneData,false);
                    break;
                case DiscardRecordingDialogDestination.RecordAudio:
                    // go to recording audio scene
                    appManager.ChangeState<RecordingAudioState>();
                    break;
                case DiscardRecordingDialogDestination.Back:
                    appManager.GoBack();
                    break;
                default:
                    break;
            }

            // then close overlay.
            _controller.CloseOverlay<DiscardRecordingOverlayView>();

        }
        #endregion

        #region RequiredMethods
        protected override void OnCreate()
        {
            // view setup
        }

        protected override void OnShowStart(object data)
        {
            // set destination based on data passed in
            var castData = (DiscardRecordingDialogData) data;
            destination = castData.ConfirmationDestination;
            OnShowComplete();
        }

        protected override void OnHideStart()
        {
            // handle transitioning out from view

            OnHideComplete();
        }
        #endregion
    }


}