using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using AppStructure.Data;
using Loju.View;

namespace AppStructure.Views
{
    /// <summary>
    /// Handles view that appears while user is outputting a flattened video.
    /// </summary>
    public class RenderingOverlayView : AbstractView 
    {
        #region Variables

        /// <summary>
        /// The sceen data.
        /// </summary>
        private ARSceneData sceneData;
        
        /// <summary>
        /// The UI element for controlling pitch.
        /// </summary>
        public Slider pitchSlider;
        #endregion

        #region CustomMethods

        /// <summary>
        /// Set pitch based on user slider interaction.
        /// </summary>
        /// <param name="val">The new pitch.</param>
        public void OnPitchSlider(float val)
        {
            sceneData.SetPitch(val);
        }

        #endregion

        #region RequiredMethods
        protected override void OnCreate()
        {
            // view setup
        }

        protected override void OnShowStart(object data)
        {
            // handle transitioning in to view
            sceneData = (ARSceneData) data;

            // set slider to current pitch.
            pitchSlider.value = sceneData.sidecar.voxes[sceneData.activeVoxIndex].pitch;
            OnShowComplete();
        }

        protected override void OnHideStart()
        {
            // handle transitioning out from view

            OnHideComplete();
        }
        #endregion
    }
        
}