using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Loju.View;
using TMPro;
using AppStructure.Data;

namespace AppStructure.Views
{
    /// <summary>
    /// Handles display of momentary informational popups
    /// </summary>
    public class PopupDialogView : AbstractView 
    {
        #region Variables

        /// <summary>
        /// The text UI element of the popup
        /// </summary>
        public TMP_Text body;

        /// <summary>
        /// The content of the popup.
        /// </summary>
        private PopupDialogData dialogData;

        /// <summary>
        /// The background image, for transparency's sake.
        /// </summary>
        public Image background;
        
        #endregion

        #region CustomMethods
        #endregion

        #region RequiredMethods
        protected override void OnCreate()
        {
            // view setup
        }

        protected override void OnShowStart(object data)
        {
            // handle transitioning in to view
            dialogData = (PopupDialogData) data;
            
            // setup UI elements
            body.text = dialogData.body;
            background.color = new Color(background.color.r,background.color.g,background.color.b,dialogData.bg_alpha);

            // automatically close after a specified duration.
            if (dialogData?.closeAfterSeconds > 0)
            {
                StartCoroutine(CloseAfterSeconds(dialogData.closeAfterSeconds));
            }
            OnShowComplete();
        }

        protected override void OnHideStart()
        {
            // handle transitioning out from view

            OnHideComplete();
        }

        /// <summary>
        /// Closes view after a delay.
        /// </summary>
        /// <param name="delay">The duration of the delay.</param>
        /// <returns></returns>
        private IEnumerator CloseAfterSeconds(float delay)
        {
            yield return new WaitForSeconds(delay);
            _controller.CloseOverlay(this);
        }
        #endregion
    }
    
}