using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Loju.View;

using AppStructure;
using AppStructure.Data;
using AppStructure.States;
using DG.Tweening;
using TMPro;

namespace AppStructure.Views
{
    /// <summary>
    /// Handles view for users to record audio.
    /// </summary>
    public class RecordingAudioView : AbstractView 
    {
        #region Variables
        /// <summary>
        /// Scene data reference
        /// </summary>
        ARSceneData sceneData;

        /// <summary>
        /// The UI element of the name of the vox being recorded.
        /// </summary>
        public RawImage title;
        
        /// <summary>
        /// The UI element for the description of the vox.
        /// </summary>
        public TMP_Text description;
        
        /// <summary>
        /// The UI element for displaying recording progress. Set in inspector.
        /// </summary>
        public Image progressBar;

        public PopupDialogData errorPopupData;
        
        /// <summary>
        /// Container for the tutorial hints
        /// </summary>
        public CanvasGroup HintsRecording;
        
        public bool recActive = false;
        #endregion

        #region CustomMethods
        /// <summary>
        /// Fade out the hints
        /// </summary>
        void fadeOutHints()
        {
            if (HintsRecording.gameObject.activeSelf)
                HintsRecording.DOFade(0, 0.5f).OnComplete(() => HintsRecording.gameObject.SetActive(false));
        }
        
        /// <summary>
        /// Called on button click. Returns to previous state.
        /// </summary>
        public void OnBackButtonPress()
        {
            // remove the vox added to the sidecar on entry
            sceneData.sidecar.voxes.Remove(sceneData.sidecar.voxes.Last());
            var appManager = (AppManager) _controller.NxAppManager;

            appManager.GoBack();
        }

        /// <summary>
        /// Called on button down. Invokes recording event.
        /// </summary>
        public void OnRecButtonPress()
        {
            fadeOutHints();
            sceneData.StartRecordingEvent.Invoke();
            recActive = true;
        }

        /// <summary>
        /// Called on button release. Invokes stop recording event.
        /// </summary>
        public void OnRecButtonRelease()
        {
            sceneData.StopRecordingEvent.Invoke();
            recActive = false;
        }

        public void OnStoppedRecordingEvent()
        {
            var appManager = (AppManager) _controller.NxAppManager;
            appManager.ChangeState<PloppingVoxState>();
        }

        private void OnRecordingError()
        {
            _controller.OpenOverlay<PopupDialogView>(errorPopupData, true);
        }

        /// <summary>
        /// Called by progress update event. Sets image fill based on progress.
        /// </summary>
        /// <param name="progress">Normalized recording progress</param>
        void OnRecProgressUpdate(float progress)
        {
            progressBar.fillAmount = progress;
        }   

        #endregion

        #region RequiredMethods
        protected override void OnCreate()
        {
            // view setup
        }

        protected override void OnShowStart(object data)
        {
            // handle transitioning in to view
            sceneData = (ARSceneData) data;
            
            // start listening to rec progress
            sceneData.RecProgressUpdateEvent.AddListener(OnRecProgressUpdate);
            sceneData.StoppedRecordingEvent.AddListener(OnStoppedRecordingEvent);
            sceneData.RecordingErrorEvent.AddListener(OnRecordingError);
            
            // setup UI elements.
            title.texture = sceneData.sidecar.voxes[sceneData.activeVoxIndex].voxData.titleImage;
            description.text = sceneData.sidecar.voxes[sceneData.activeVoxIndex].voxData.description;
            
            OnShowComplete();
            HintsRecording.gameObject.SetActive(AppManager.ShowTutorial());
        }

        protected override void OnHideStart()
        {
            // handle transitioning out from view

            // stop listening.
            sceneData.RecProgressUpdateEvent.RemoveListener(OnRecProgressUpdate);
            sceneData.StoppedRecordingEvent.RemoveListener(OnStoppedRecordingEvent);
            sceneData.RecordingErrorEvent.RemoveListener(OnRecordingError);

            OnHideComplete();
        }
        #endregion
    }
        
}
