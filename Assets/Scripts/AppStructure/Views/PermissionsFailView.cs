using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using AppStructure;
using AppStructure.States;
using Loju.View;

namespace AppStructure.Views
{
    
/// <summary>
/// Handles view to notify user of permissions requirements upon denial of permissions.
/// </summary>
public class PermissionsFailView : AbstractView 
{
    #region Variables
    #endregion

    #region CustomMethods

    /// <summary>
    /// Opens user's Settings app for user to fix permissions.
    /// </summary>
    public void OnSettingsButtonPress()
    {
        // from: https://gamedev.stackexchange.com/questions/175787/navigating-to-android-app-settings-permission-from-unity
        try
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            using (var unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            using (AndroidJavaObject currentActivityObject = unityClass.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                string packageName = currentActivityObject.Call<string>("getPackageName");

                using (var uriClass = new AndroidJavaClass("android.net.Uri"))
                using (AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("fromParts", "package", packageName, null))
                using (var intentObject = new AndroidJavaObject("android.content.Intent", "android.settings.APPLICATION_DETAILS_SETTINGS", uriObject))
                {
                    intentObject.Call<AndroidJavaObject>("addCategory", "android.intent.category.DEFAULT");
                    intentObject.Call<AndroidJavaObject>("setFlags", 0x10000000);
                    currentActivityObject.Call("startActivity", intentObject);
                }
            }
#endif
        }
        catch (System.Exception ex)
        {
            Debug.LogException(ex);
        }
    }

    /// <summary>
    /// On cancal button press. Returns to intro page.
    /// </summary>
    public void OnCancelButtonPress()
    {
        _controller.ChangeLocation<IntroPermissionsView>(null, true);
    }
    #endregion

    #region RequiredMethods
    protected override void OnCreate()
    {
        // view setup
    }

    protected override void OnShowStart(object data)
    {
        // handle transitioning in to view

        OnShowComplete();
    }

    protected override void OnHideStart()
    {
        // handle transitioning out from view

        OnHideComplete();
    }
    #endregion
}
}