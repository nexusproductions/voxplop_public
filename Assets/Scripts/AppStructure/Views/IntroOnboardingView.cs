using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using AppStructure;
using AppStructure.States;
using DG.Tweening;
using Loju.View;
using TMPro;

namespace AppStructure.Views
{
    /// <summary>
    /// Handles the onboarding UI 
    /// </summary>
    public class IntroOnboardingView : AbstractView
    {
        #region Variables

        /// <summary>
        /// List of UI pages in this view
        /// </summary>
        public List<RectTransform> Pages;

        /// <summary>
        /// Text of the button
        /// </summary>
        public TextMeshProUGUI ButtonText;
        

        private int pageIndex = 0;
        #endregion

        #region CustomMethods

        /// <summary>
        /// handles the next button
        /// </summary>
        public void OnNextButtonClicked()
        {
            if (pageIndex < 3)
            {
                pageIndex++;
                Pages[pageIndex].DOMove(Pages[0].position, 0.5f);
                if (pageIndex == 3)
                    ButtonText.text = "DONE";
            }
            else
            {
                _controller.ChangeLocation<IntroPermissionsView>(null,true);
            }
        }

        #endregion

        #region RequiredMethods

        protected override void OnCreate()
        {
            //Page2.position = Page2.position + new Vector3(Page2.rect.width,0 , 0);
        }

        protected override void OnShowStart(object data)
        {
            OnShowComplete();
        }

        protected override void OnHideStart()
        {
            OnHideComplete();
        }

        #endregion
    }
}