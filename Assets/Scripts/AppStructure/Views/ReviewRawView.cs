using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Loju.View;

using AppStructure;
using AppStructure.States;
using AppStructure.Data;

using NexusStudios.VoxPlop;

namespace AppStructure.Views
{
    /// <summary>
    /// Handles UI for users to review raw video and begin adding vox process
    /// </summary>
    public class ReviewRawView : AbstractView 
    {
        #region Variables
        /// <summary>
        /// Scene data.
        /// </summary>
        ARSceneData sceneData;

        /// <summary>
        /// The UI element for AR Plane visibility toggle. Set in inspector.
        /// </summary>
        public Toggle planesVisibleToggle;

        /// <summary>
        /// The popup data to display on successful save to library.
        /// </summary>
        public PopupDialogData savedVideoDialogData;
        #endregion

        #region CustomMethods

        /// <summary>
        /// Returns to previous state with confirmation. Called on button click. 
        /// </summary>
        public void OnBackButtonPress(){

            // if file is not flagged as saved, display deletion confirmation
            if (!sceneData.fileIsSaved)
            {
                var data = new DiscardRecordingDialogData();

                // if it's coming from the library, return to the library.
                data.ConfirmationDestination = DiscardRecordingDialogDestination.Back;

                _controller.OpenOverlay<DiscardRecordingOverlayView>(data,true);
            }
            // if it has saved, just go.
            else
            {
                _controller.ChangeLocation<RecordRawView>(sceneData, true);
            }

        }

        /// <summary>
        /// Returns to main mainu with confirmation. Called on button click. 
        /// </summary>
        public void OnCloseButtonPress(){

            // if it isn't saved, display confirmation window
            if (!sceneData.fileIsSaved)
            {
                var data = new DiscardRecordingDialogData();
                data.ConfirmationDestination = DiscardRecordingDialogDestination.MainMenu;
                _controller.OpenOverlay<DiscardRecordingOverlayView>(data,true);
            }
            else
            {
                var appManager = (AppManager) _controller.NxAppManager;
                appManager.GoBack();
            }
        }

        /// <summary>
        /// Selects a vox and proceeds to next state. Called on button click. 
        /// </summary>
        /// <param name="voxPick">The selected vox</param>
        public void OnVoxButtonPress(VoxData voxPick){

            // if no voxes in sidecar, make one.
            if (sceneData.sidecar.voxes.Count == 0)
            {
                sceneData.sidecar.voxes.Add(new VoxSidecarData());
            }
            // set vox type in sidecar
            sceneData.sidecar.voxes[sceneData.activeVoxIndex].voxData = voxPick;

            //change state
            var appManager = (AppManager) _controller.NxAppManager;
            appManager.ChangeState<RecordingAudioState>();
        }

        /// <summary>
        /// Shares spatial video. Called on button click. 
        /// </summary>
        public void OnShareButtonPress(){
            ShareManager.ShareSpatialVideo(sceneData.currentFilename);
        }

        /// <summary>
        /// Sets AR Plane visibility. Called on toggle click.
        /// </summary>
        /// <param name="hidden">Should the AR planes be visible?</param>
        public void OnSetPlaneHiddenToggle(bool hidden){
            sceneData.SetPlanesHidden(hidden);
        }

        /// <summary>
        /// Saves current video to libary.
        /// </summary>
        public void OnSaveButtonPress()
        {
            Debug.Log("save button pressed.");
            sceneData.SaveVideoEvent.Invoke("raw");
            
            sceneData.fileIsSaved = true;
            _controller.OpenOverlay<PopupDialogView>(savedVideoDialogData, true);
        }
        

        #endregion

        #region RequiredMethods
        protected override void OnCreate()
        {
            // view setup
        }

        protected override void OnShowStart(object data)
        {
            // handle transitioning in to view
            // cast
            sceneData = (ARSceneData) data;

            // set UI element to current state
            planesVisibleToggle.isOn = sceneData.planesHidden;

            OnShowComplete();
        }

        protected override void OnHideStart()
        {
            // handle transitioning out from view

            OnHideComplete();
        }
        #endregion
    }
}