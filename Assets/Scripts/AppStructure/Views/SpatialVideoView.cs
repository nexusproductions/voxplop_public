using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Loju.View;

using AppStructure;
using AppStructure.States;

namespace AppStructure.Views
{
    /// <summary>
    /// Handles view for displaying informational text about spatial video.
    /// </summary>
    public class SpatialVideoView : AbstractView 
    {
        #region Variables
        #endregion

        #region CustomMethods
        /// <summary>
        /// Continues to next state. Called on button click.
        /// </summary>
        public void OnContinueButtonPress(){
            PlayerPrefs.SetInt("UserPassedSpatialVideoInfo", 1);
            PlayerPrefs.Save();
            var appManager = (AppManager) _controller.NxAppManager;
            appManager.ChangeState<RecordingRawState>();
        }

        /// <summary>
        /// Returns to previous view. Called on button click.
        /// </summary>
        public void OnCancelButtonPress(){
            ChangeLocation<MainMenuView>();
        }
        #endregion

        #region RequiredMethods
        protected override void OnCreate()
        {
            // view setup
        }

        protected override void OnShowStart(object data)
        {
            // handle transitioning in to view

            OnShowComplete();
        }

        protected override void OnHideStart()
        {
            // handle transitioning out from view

            OnHideComplete();
        }
        #endregion
    }
        
}