using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Loju.View;

using AppStructure;
using AppStructure.States;
using AppStructure.Data;



using NexusStudios.VoxPlop;

namespace AppStructure.Views
{
    /// <summary>
    /// Handles the view that allows users to interact with recorded or received content on their device.
    /// </summary>
    public class LibraryView : AbstractView
    {
        #region Variables
        /// <summary>
        /// The scroll view that contains the list of library items. Set in inspector.
        /// </summary>
        public GameObject scrollView;

        /// <summary>
        /// The data the view displays.
        /// </summary>
        LibraryData libraryData;

        /// <summary>
        /// The UI object that notifies users there are no items in the library and offers a button to create one
        /// </summary>
        public GameObject noItemNotification;

        /// <summary>
        /// Signals if the delete toggle has been enabled.
        /// </summary>
        bool bIsDeleteEnabled;
        
        /// <summary>
        /// The UI element of the delete toggle. Set in inspector.
        /// </summary>
        public Toggle deleteToggle;
        
        /// <summary>
        /// The items currently selected for deletion
        /// </summary>
        /// <remarks>I'd love to build out multi-delete at some point</remarks>
        private List<LibraryItem> selectedItems = new List<LibraryItem>();

        /// <summary>
        /// The data to populate the notification for the user being unable to delete prerecorded videos.
        /// </summary>
        public PopupDialogData prerecDeleteAlertData;
        
        /// <summary>
        /// The object that holds all the first-time-user hints
        /// </summary>
        public GameObject Hints;
        
        #endregion

        #region CustomMethods

        /// <summary>
        /// Reloads library and populates the view with items. Called on view show start.
        /// </summary>
        /// <param name="data">The LibraryData to show</param>
        private void SetupLibaryDisplay(object data)
        {
            // cast data to something useful.
            libraryData = (LibraryData)data;

            // reload library from disk
            libraryData.ReloadLibrary();

            // if the library has items, disp[ay them
            if (libraryData.libraryItems.Length > 0)
            {
                // turn on the scroll view (as apposed to the no item notification)
                scrollView.SetActive(true);

                // get the layout children of the scrollView
                var gridLayout = scrollView.GetComponentInChildren<GridLayoutGroup>(true);
                var listLayout = scrollView.GetComponentInChildren<VerticalLayoutGroup>(true);

                //adjust the cell size to fit columns evenly across the scroll view
                int columnCount = gridLayout.constraintCount;
                float layoutWidth = gridLayout.GetComponent<RectTransform>().rect.width;
                float spacingWidth = gridLayout.spacing.x * (float)columnCount * 0.5f; 
                float cellSize = (layoutWidth - spacingWidth) / columnCount;
                gridLayout.cellSize = new Vector2(cellSize, cellSize);

                // set the appropriate display mode
                switch (libraryData.displayMode)
                {
                    case LibraryDisplayMode.Gallery:

                        // activate
                        gridLayout.gameObject.SetActive(true);

                        // then populate
                        foreach (LibraryItem item in libraryData.libraryItems)
                        {
                            // instantiate an item
                            var itemButton = GameObject.Instantiate(libraryData.libraryGridItemPrefab, gridLayout.transform);

                            // set its contents (the item will do the rest in its Start() next frame)
                            itemButton.GetComponentInChildren<LibraryItemCtrl>().item = item;

                            // set a reference back to this view for it to call its load function
                            itemButton.GetComponentInChildren<LibraryItemCtrl>().parentView = this;
                        }
                        break;
                    case LibraryDisplayMode.List:

                        // activate
                        listLayout.gameObject.SetActive(true);

                        // then populate
                        break;
                    default:
                        break;
                }
            }
            // if there aren't, display the notification.
            else
            {
                noItemNotification.SetActive(true);
            }

        }

        /// <summary>
        /// Called on button click. Returns to previous screen.
        /// </summary>
        public void OnBackButton()
        {
            var appManager = (AppManager) _controller.NxAppManager;
            appManager.GoBack();
        }

        /// <summary>
        /// On item clicked. Opens item via library manager.
        /// </summary>
        /// <param name="item">The item selected</param>
        public void OnItemSelected(LibraryItem item)
        {
            if (bIsDeleteEnabled)
            {
                // if it's prerec, alert and don't delete
                if (item.filename.Contains("nexus"))
                {
                    _controller.OpenOverlay<PopupDialogView>(prerecDeleteAlertData, true);
                }
                else
                {
                    var dialogData = ScriptableObject.CreateInstance<ConfirmDeletionOverlayData>();
                    dialogData.parentView = this;

                    selectedItems.Add(item);
                    _controller.OpenOverlay<ConfirmDeletionOverlayView>(dialogData, true);
                }

            }
            else
            {
                if (item.type == LibraryItemType.PloppedVideo_Solo)
                {
                    LibraryManager.OpenItem(item);
                    var appManager = (AppManager) _controller.NxAppManager;
                    appManager.ChangeState<PloppingVoxState>();
                }
                else if (item.type == LibraryItemType.RawVideo)
                {
                    LibraryManager.OpenItem(item);
                    var appManager = (AppManager) _controller.NxAppManager;
                    appManager.ChangeState<RecordingRawState>();
                }
                else if (item.type == LibraryItemType.PloppedVideo_Multiple)
                {
                    LibraryManager.OpenItem(item);
                    var appManager = (AppManager) _controller.NxAppManager;
                    appManager.ChangeState<PloppingVoxState>();
                }
            }
        }
        

        /// <summary>
        /// Called on button click. Changes scene to record a new video.
        /// </summary>
        public void OnNewVideoButton()
        {
            var appManager = (AppManager) _controller.NxAppManager;
            appManager.ChangeState<RecordingRawState>();

        }

        /// <summary>
        /// Sets the deletion state for the view
        /// </summary>
        /// <param name="deleteEnabled"></param>
        public void OnDeleteToggle(bool deleteEnabled)
        {
            bIsDeleteEnabled = deleteEnabled;
        }

        /// <summary>
        /// Called by confirm deletion dialog. Deletes selected item(s) and reloads library.
        /// </summary>
        public void ConfirmDeletion()
        {
            Debug.Log("Confirmed delete...");
            foreach (var item in selectedItems)
            {
                LibraryManager.DeleteItem(item);
            }
            selectedItems.Clear();
            deleteToggle.isOn = false;
            ReloadLibrary();
            
        }

        /// <summary>
        /// Called by conform deletion dialog. Disables deletion mode.
        /// </summary>
        public void CancelDeletion()
        {
            deleteToggle.isOn = false;
        }
        
        /// <summary>
        /// Deletes all UI representations of LibraryItems and repopulates view.
        /// </summary>
        private void ReloadLibrary()
        {
            foreach (LibraryItemCtrl item in scrollView.GetComponentsInChildren<LibraryItemCtrl>())
            {
                Destroy(item.gameObject);
            }
            SetupLibaryDisplay(libraryData);

        }

        #endregion

        #region RequiredMethods
        protected override void OnCreate()
        {
            Hints.SetActive(AppManager.ShowTutorial());
        }

        protected override void OnShowStart(object data)
        {
            // handle transitioning in to view
            SetupLibaryDisplay(data);
            OnShowComplete();
        }

        protected override void OnHideStart()
        {
            // handle transitioning out from view

            OnHideComplete();
        }
        #endregion
    }


}