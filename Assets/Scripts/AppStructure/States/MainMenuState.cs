using System.Collections;
using System.Collections.Generic;
using NxAppStructure;
using UnityEngine;
using AppStructure.Data;
using NexusStudios.VoxPlop;

namespace AppStructure.States
{
    /// <summary>
    /// The main menu. The user picks to create or load a video.
    /// </summary>
    public class MainMenuState : NxAppState<NxAppManager>
    {
        #region Variables
        #endregion

        #region CustomMethods
        #endregion

        #region RequiredMethods
        /// <summary>
        /// Begin is called once the state has been entered and the prefab has been instantiated.
        /// This is where you should initialize any runtime part of your state.
        /// </summary>
        public override void Begin()
        {
            // if we get back to main menu, reset scene data
            var appManager = (AppManager) _context;
            var globalData = (GlobalSessionData) appManager.GlobalAppData;
            globalData.arSceneData.Reset();
            globalData.ClearBackStack();
            
            LibraryManager.RemoveTempFiles();
        }

        /// <summary>
        /// Reason is called every frame without respect to delta time
        /// </summary>
        public override void Reason()
        {
        }

        /// <summary>
        /// Update is called every frame and gives you the delta time since last update
        /// </summary>
        /// <param name="deltaTime"></param>
        public override void Update(float deltaTime)
        {
        }

        /// <summary>
        /// End is called after the prefab has been destroyed, you should clean up anything else you have created in the
        /// scene / memory. The state instance itself lives on.
        /// </summary>
        public override void End()
        {
        }

        /// <summary>
        /// IsValidNextState should implement if the nextState is valid. If all states are valid next states, just implement
        /// return true;
        /// </summary>
        /// <param name="nextState"></param>
        /// <returns></returns>
        public override bool IsValidNextState(NxAppState<NxAppManager> nextState)
        {
            return nextState != null;
        }
        #endregion
        
    
        #region OptionalMethods
        /// <summary>
        /// called directly after the machine and context are initialized allowing the state to do any required one-time setup
        /// </summary>
        public override void OnInitialized()
        {
        }
        
        /// <summary>
        /// LateUpdate is called every frame after all Update functions (including animation) have been called and
        /// gives you the delta time since last LateUpdate.
        /// </summary>
        /// <param name="deltaTime"></param>
        public override void LateUpdate(float deltaTime)
        {
        }

        /// <summary>
        /// Did enter gets called before instantiating the prefab (if there is one) and lets you know which state it came from.
        /// </summary>
        /// <param name="fromPrevState"></param>
        public override void WillEnterFrom(NxAppState<NxAppManager> fromPrevState)
        {
        }
        
        /// <summary>
        /// WillExitTo gets called before destroying the prefab, and tells you which state is about to be loaded.
        /// </summary>
        /// <param name="nextState"></param>
        public override void WillExitTo(NxAppState<NxAppManager> nextState)
        {
            var appManager = (AppManager) _context;
            var globalData = (GlobalSessionData) appManager.GlobalAppData;            
            globalData.PushBackState(this);
        }
        #endregion
    }
}
