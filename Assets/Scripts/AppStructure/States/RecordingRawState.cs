using System.Collections;
using System.Collections.Generic;
using NxAppStructure;
using UnityEngine;

using AppStructure.Data;
using AppStructure.Views;
using Loju.View;
using UnityEngine.XR.ARFoundation;

namespace AppStructure.States
{
    /// <summary>
    /// The state in which the user is recording (including reviewing) a raw video, either recording fresh or looking at a raw video loaded from the library.
    /// </summary>
    public class RecordingRawState : NxAppState<NxAppManager>
    {
        #region Variables

        private float loadRecordViewTimer = 0f;
        private bool shouldLoadRecordView;
        #endregion

        #region CustomMethods

        /// <summary>
        /// Called when the AR Session enters an active state. Hides loading screen, plays current file if there is one.
        /// </summary>
        /// <param name="state"></param>
        public void OnSessionLoadedEvent(ARSessionState state)
        {
            // cast scenedata
            var sceneData = (ARSceneData) PersistentData;
            
            // if it has a currentFilename, play it and review.
            if (sceneData.currentFilename != "")
            {
                Debug.Log("Current file exists. Switching to Review View");
                
                // switch view to review
                _context.ViewController.ChangeLocation<ReviewRawView>(sceneData, true);
            }
            else
            {
                Debug.Log("no current file exists. Switching to Record View");

                // switch view to record new
                // we have to do this because the live camera provides black frames and there is no clear way to differentiate them from legit ones.
                shouldLoadRecordView = true;
            }
            
            sceneData.SessionLoadedEvent.RemoveListener(OnSessionLoadedEvent);
        }
        
        private void OnPlaybackError()
        {
            _context.ChangeState<MediaLibraryState>();
            
        }
        #endregion

        #region RequiredMethods
        /// <summary>
        /// Begin is called once the state has been entered and the prefab has been instantiated.
        /// This is where you should initialize any runtime part of your state.
        /// </summary>
        public override void Begin()
        {
            // cast scenedata
            var sceneData = (ARSceneData) PersistentData;

            sceneData.SessionLoadedEvent.AddListener(OnSessionLoadedEvent);
            sceneData.PlaybackErrorEvent.AddListener(OnPlaybackError);
        }

        /// <summary>
        /// Reason is called every frame without respect to delta time
        /// </summary>
        public override void Reason()
        {
        }

        /// <summary>
        /// Update is called every frame and gives you the delta time since last update
        /// </summary>
        /// <param name="deltaTime"></param>
        public override void Update(float deltaTime)
        {
            var sceneData = (ARSceneData) PersistentData;

            var loadRecordViewTimeout = 0.5f;
            
            if (shouldLoadRecordView)
            {
                Debug.Log("wait for camera... " + loadRecordViewTimer);
                if (loadRecordViewTimer > loadRecordViewTimeout)
                {
                    _context.ViewController.ChangeLocation<RecordRawView>(sceneData, true);
                    loadRecordViewTimer = 0;
                    shouldLoadRecordView = false;
                }
                else
                {
                    loadRecordViewTimer += deltaTime;
                }
                    
            }
        }

        /// <summary>
        /// End is called after the prefab has been destroyed, you should clean up anything else you have created in the
        /// scene / memory. The state instance itself lives on.
        /// </summary>
        public override void End()
        {
            var sceneData = (ARSceneData) PersistentData;
            sceneData.SessionLoadedEvent.RemoveListener(OnSessionLoadedEvent);
            sceneData.PlaybackErrorEvent.RemoveListener(OnPlaybackError);

            // actually reset ARSession
            LoaderUtility.Deinitialize();
            LoaderUtility.Initialize();
        }

        /// <summary>
        /// IsValidNextState should implement if the nextState is valid. If all states are valid next states, just implement
        /// return true;
        /// </summary>
        /// <param name="nextState"></param>
        /// <returns></returns>
        public override bool IsValidNextState(NxAppState<NxAppManager> nextState)
        {
            return nextState != null;
        }
        #endregion
        
    
        #region OptionalMethods
        /// <summary>
        /// called directly after the machine and context are initialized allowing the state to do any required one-time setup
        /// </summary>
        public override void OnInitialized()
        {
        }
        
        /// <summary>
        /// LateUpdate is called every frame after all Update functions (including animation) have been called and
        /// gives you the delta time since last LateUpdate.
        /// </summary>
        /// <param name="deltaTime"></param>
        public override void LateUpdate(float deltaTime)
        {
        }

        /// <summary>
        /// Did enter gets called before instantiating the prefab (if there is one) and lets you know which state it came from.
        /// </summary>
        /// <param name="fromPrevState"></param>
        public override void WillEnterFrom(NxAppState<NxAppManager> fromPrevState)
        {
        }
        
        /// <summary>
        /// WillExitTo gets called before destroying the prefab, and tells you which state is about to be loaded.
        /// </summary>
        /// <param name="nextState"></param>
        public override void WillExitTo(NxAppState<NxAppManager> nextState)
        {
            // discard current video reference when returning to main menu   
            if (nextState.GetType() == typeof(MainMenuState)){
                var sceneData = (ARSceneData) PersistentData; 
                sceneData.currentFilename = "";
            }
            
            // add to back queue
            var appManager = (AppManager) _context;
            var globalData = (GlobalSessionData) appManager.GlobalAppData;
            globalData.PushBackState(this);
        }
        #endregion
    }
}
