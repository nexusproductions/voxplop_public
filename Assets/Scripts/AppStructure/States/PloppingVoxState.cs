using System.Collections.Generic;
using NxAppStructure;
using UnityEngine;
using AppStructure.Data;
using AppStructure.Views;
using DG.Tweening;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.EventSystems;
using NexusStudios.VoxPlop;

namespace AppStructure.States
{
    /// <summary>
    /// The state where the user is viewing a spatial video and adding a new Vox.
    /// </summary>
    public class PloppingVoxState : NxAppState<NxAppManager>
    {
        #region Variables
        /// <summary>
        /// The scene data.
        /// </summary>
        ARSceneData sceneData;

        /// <summary>
        /// Is the user currently touching the screen? Prevents multiple touches across frames.
        /// </summary>
        bool bIsTouched = false;
        
        /// <summary>
        /// A handy helper that stores refenceses to objects we might need in the scene
        /// </summary>
        private PloppingVoxPrefabHelper plopHelper;

        /// <summary>
        /// The touch point indicator around the voxes in teh scene.
        /// </summary>
        private TouchCircle touchCircle;
        
        /// <summary>
        /// The voxes currently spawned in the scene.
        /// </summary>
        private List<VoxCtrl> spawnedVoxes;

        /// <summary>
        /// The background music player in the scene.
        /// </summary>
        private MusicPlayer musicPlayer;

        /// <summary>
        /// Have we entered the scene?
        /// </summary>
        // private bool didEnterView;

        #endregion

        #region CustomMethods
        /// <summary>
        /// Called when an AR State enters an active state. Hides loading screen.
        /// </summary>
        /// <param name="state">The entered state</param>
        private void OnSessionLoadedEvent(ARSessionState state)
        {
            Debug.Log("session loaded, changing view.");
            if (_context.ViewController.CurrentLocation.GetType() != typeof(PloppingVoxView))
                EnterView();
        }

        private void EnterView()
        {
            _context.ViewController.ChangeLocation<PloppingVoxView>(sceneData,true);
            sceneData.SessionLoadedEvent.RemoveListener(OnSessionLoadedEvent);
            
            // if entering the scene with a placed vox, start the music
            if(sceneData.sidecar.voxes[0].position.magnitude >0)
                musicPlayer.SetClip(sceneData.sidecar.voxes[0].voxData.music);

            // didEnterView = true;
        }

        /// <summary>
        /// Called on beginning output. Restarts playback, resets scene objects and music, hides plane visualizer.
        /// </summary>
        private void OnStartOutputEvent()
        {
            Debug.Log("starting output. destroying voxes in scene");
            sceneData.SetPlanesHidden(true);
            foreach (var vox in spawnedVoxes)
            {
                GameObject.Destroy(vox.gameObject);
            }
            spawnedVoxes.Clear();
            
            musicPlayer.Restart();
        }

        /// <summary>
        /// Called on completing playback. Clears voxes in scene.
        /// </summary>
        private void OnCompletedPlaybackEvent()
        {
            Debug.Log("completed playback. destroying voxes in scene");
            foreach (var vox in spawnedVoxes)
            {
                GameObject.Destroy(vox.gameObject);
            }
            spawnedVoxes.Clear();
        }
        
        /// <summary>
        /// Called when AR Session localizes. Spawns voxes into the scene.
        /// </summary>
        void OnARSessionLocalizedEvent()
        {
            // FIXME: this is a wonky catch for if session loaded fails to fire when entering from an external file
            // if it hasn't entered by now, enter.
            if (_context.ViewController.CurrentLocation.GetType() != typeof(PloppingVoxView) &&
                _context.ViewController.CurrentLocation.GetType() == typeof(SessionLoadingView))
                EnterView();

            Debug.Log("session localized, spawning.");
            
            // if the sidecar has a vox in it that has positional data, place it.
            for (int i = 0 ; i< sceneData.sidecar.voxes.Count; i++)
            {
                var vox = sceneData.sidecar.voxes[i];
                
                // if the position is non-zero, it has been placed and should be spawned
                if (vox.position.magnitude>0)
                {
                    var newVox = SpawnVox(vox);
                    spawnedVoxes.Add(newVox);
                    
                    // If we have two voxes in the scene, let me know about each other.
                    if (spawnedVoxes.Count == 2)
                    {
                        spawnedVoxes[0].DualVoxMode = VoxCtrl.DualVoxModes.Primary;
                        spawnedVoxes[1].DualVoxMode = VoxCtrl.DualVoxModes.Secondary;
                        spawnedVoxes[0].OtherVox = spawnedVoxes[1];
                        spawnedVoxes[1].OtherVox = spawnedVoxes[0];
                    }
                    
                    if(sceneData.activeVoxIndex == i)
                        newVox.GetComponent<VoxCtrl>().SetBeingPlaced(true);
                    else
                        newVox.GetComponent<VoxCtrl>().SetBeingPlaced(false);
                    
                }
            }

            for (int i = 0; i < spawnedVoxes.Count; i++)
            {
                spawnedVoxes[i].Spawn(true);
            }

        }

        /// <summary>
        /// Instantiates a touch point indicator in the scene.
        /// </summary>
        /// <param name="pos">Target position in world space.</param>
        void SpawnTouchCircle(Vector3 pos)
        {
            if (touchCircle == null)
                touchCircle = GameObject.Instantiate(
                    sceneData.sidecar.voxes[sceneData.activeVoxIndex].voxData.touchVisualizerPrefab, pos,
                    Quaternion.identity, plopHelper.ARSessionOrigin.transform).GetComponent<TouchCircle>();
            touchCircle.SetPosition(pos);
        }

        /// <summary>
        /// Instantiates a Vox into the scene and performs sets it up.
        /// </summary>
        /// <param name="vox">The vox to spawn</param>
        /// <returns>The control script of the new vox.</returns>
        private VoxCtrl SpawnVox(VoxSidecarData vox)
        {
            Debug.Log("spawning vox");
            
            // Get the session origin for parenting. 
            var origin = plopHelper.ARSessionOrigin.transform;

            // Get the right prefab to create.
            var pickedVox = vox.voxData;

            // Instantiate it at raycast intersection, parented to origin.
            VoxCtrl thisVox = GameObject.Instantiate(pickedVox.prefab, vox.position, Quaternion.identity, origin).GetComponent<VoxCtrl>();;

            pickedVox.audioSampler = thisVox.gameObject.GetComponent<AudioSampler>();

            // Scale the box based on distance from camera 
            float distance = Vector3.Distance(plopHelper.ARCamera.transform.position, thisVox.transform.position);
            float lerp = (distance - sceneData.VoxMinDistance) / (sceneData.VoxMaxDistance - sceneData.VoxMinDistance);
            float scale = Mathf.Lerp(sceneData.VoxMinScale, sceneData.VoxMaxScale, lerp);
            thisVox.transform.localScale = new Vector3(scale,scale,scale);
            
            // Set being placed state of the vox
            thisVox.SetCamera(plopHelper.ARCamera.transform);
            
            // Get the audio clip
            var currentClip = vox.audioClip;

            // if it exists, set the current vox's audio clip to it.
            if(currentClip) 
            {
                thisVox.AudioClip = currentClip;
                thisVox.SetPitch(vox.pitch);
            }

            // face the camera and trigger the entrance animation.
            thisVox.TurnToFaceCamera();

            return thisVox;
        }
        
        /// <summary>
        /// Called when finished outputting a flat video. Returns UI to plopping vox interface.
        /// </summary>
        private void OnCompletedFlatOutputEvent()
        {
            _context.ViewController.ChangeLocation<PloppingVoxView>(sceneData,true);
        }

        /// <summary>
        /// Checks to see if a point is over a UI component.
        /// From: https://github.com/Unity-Technologies/arfoundation-samples/issues/25
        /// </summary>
        /// <param name="pos">Screen position of cursor (or touch)</param>
        /// <returns>true if the point is over a UI component</returns>
        bool IsPointOverUIObject(Vector2 pos)
        {
            
            if (EventSystem.current.IsPointerOverGameObject())
                return false;

            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(pos.x, pos.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;

        }

        private void OnPlaybackError()
        {
            _context.ChangeState<MediaLibraryState>();
            
        }
        #endregion
        
        #region RequiredMethods
        /// <summary>
        /// Begin is called once the state has been entered and the prefab has been instantiated.
        /// This is where you should initialize any runtime part of your state.
        /// </summary>
        public override void Begin()
        {
            // Start playback of current video on entrance.
            sceneData.StartPlaybackEvent.Invoke();

            // Setup in-scene references
            plopHelper = PrefabInstance.GetComponent<PloppingVoxPrefabHelper>();
            musicPlayer = plopHelper.musicPlayer;
            
            // start listening for events.
            sceneData.StartFlatOutputEvent.AddListener(OnStartOutputEvent);
            sceneData.StartSpatialOutputEvent.AddListener(OnStartOutputEvent);
            sceneData.ARSessionLocalizedEvent.AddListener(OnARSessionLocalizedEvent);
            sceneData.CompletedPlaybackEvent.AddListener(OnCompletedPlaybackEvent);
            sceneData.CompletedFlatOutputEvent.AddListener(OnCompletedFlatOutputEvent);
            sceneData.SessionLoadedEvent.AddListener(OnSessionLoadedEvent);
            sceneData.PlaybackErrorEvent.AddListener(OnPlaybackError);
            
            // init spawnedVox list
            spawnedVoxes = new List<VoxCtrl>();

            // set the active vox to the most recent one.
            sceneData.activeVoxIndex = sceneData.sidecar.voxes.Count - 1;
            
        }

        /// <summary>
        /// Reason is called every frame without respect to delta time
        /// </summary>
        public override void Reason()
        {
        }
        
        /// <summary>
        /// Update is called every frame and gives you the delta time since last update
        /// </summary>
        /// <param name="deltaTime"></param>
        public override void Update(float deltaTime)
        {
            // Declare target for raycast hits
            var hitList = new List<ARRaycastHit>();

            // If there's a touch and it wasn't touched last frame, act on the touch
            if (Input.touchCount>0 && !bIsTouched)
            {
                // Get touches
                var touch = Input.touches[0];
                
                // Test if we touched a vox
                RaycastHit hit;
                bool voxHit = false;
                if (Physics.Raycast(plopHelper.ARCamera.ScreenPointToRay(touch.position ), out hit))
                {
                    if (hit.transform.tag == "Vox")
                    {
                        VoxCtrl hitVox = hit.transform.GetComponent<VoxCtrl>();
                        hitVox.RandomEmote();
                        voxHit = true;
                        if (spawnedVoxes.Count == 2)
                            sceneData.activeVoxIndex = (spawnedVoxes[0].voxData.name == hitVox.voxData.name) ? 0 : 1;
                        hitVox.transform.DOPunchScale(Vector3.one*0.02f, 0.5f,10,3);
                    }
                }
                // Raycast out, get collisions with planes
                bool touchHitPlane = plopHelper.ARRaycastManager.Raycast(touch.position,hitList,TrackableType.Planes );
                bool voxUnplaced = sceneData.sidecar.voxes[sceneData.activeVoxIndex].position.magnitude == 0;
                if ( !voxHit&& touchHitPlane && !IsPointOverUIObject(touch.position))
                {
                    if (voxUnplaced)
                    {
                        Debug.Log("Touch hits a plane. Spawning a vox. Voxes in scene:" + spawnedVoxes.Count);

                        Vector3 spawnPos = hitList[0].sessionRelativePose.position;

                        // If there already is another vox in the scene, check if it's close and reposition to make height
                        if (spawnedVoxes.Count == 1 && Vector3.Distance(spawnedVoxes[0].transform.position,spawnPos)<0.4f)
                        {
                            float otherY = spawnedVoxes[0].transform.position.y;
                            spawnPos = new Vector3(spawnPos.x, otherY, spawnPos.z);
                        }
                        // update sidecar with position info
                        sceneData.sidecar.voxes[sceneData.activeVoxIndex].position = spawnPos;

                        var newVox = SpawnVox(sceneData.sidecar.voxes[sceneData.activeVoxIndex]);

                        spawnedVoxes.Add(newVox);
                        // If we have two voxes, let them know about eachother 
                        if (spawnedVoxes.Count == 2)
                        {
                            spawnedVoxes[0].DualVoxMode = VoxCtrl.DualVoxModes.Primary;
                            spawnedVoxes[1].DualVoxMode = VoxCtrl.DualVoxModes.Secondary;
                            spawnedVoxes[0].OtherVox = spawnedVoxes[1];
                            spawnedVoxes[1].OtherVox = spawnedVoxes[0];
                        }
                        else
                        {
                            // if only one, start the music
                            musicPlayer.SetClip(sceneData.sidecar.voxes[0].voxData.music);
                        }
                        newVox.SetBeingPlaced(true);
                        newVox.Spawn(true);
                        // hide planes
                        sceneData.SetPlanesHidden(true);
                        
                        // invoke the event
                        sceneData.VoxPlacedEvent.Invoke();
                        


                        SpawnTouchCircle(spawnPos);
                    }
                    else
                    {
                        Debug.Log("hit plane. Moving.");
                        Vector3 pos = hitList[0].sessionRelativePose.position;

                        if (spawnedVoxes[sceneData.activeVoxIndex].SetLocomotionTarget(pos))
                        {
                            spawnedVoxes[sceneData.activeVoxIndex].SetLocomotionTarget(pos);
                            SpawnTouchCircle(pos);
                            sceneData.sidecar.voxes[sceneData.activeVoxIndex].position = pos;
                        }
                        
                    }
                }
                // prevent multiple touches
                bIsTouched = true;
            }
            // if it's not being touched, allow it to be touched again
            else if(Input.touchCount == 0)
            {
                bIsTouched = false;
            }
        }

        /// <summary>
        /// End is called after the prefab has been destroyed, you should clean up anything else you have created in the
        /// scene / memory. The state instance itself lives on.
        /// </summary>
        public override void End()
        {
            // clean up.
            sceneData.CompletedPlaybackEvent.RemoveListener(OnCompletedPlaybackEvent);
            sceneData.StartFlatOutputEvent.RemoveListener(OnStartOutputEvent);
            sceneData.StartSpatialOutputEvent.RemoveListener(OnStartOutputEvent);
            sceneData.ARSessionLocalizedEvent.RemoveListener(OnARSessionLocalizedEvent);
            sceneData.CompletedPlaybackEvent.RemoveListener(OnCompletedPlaybackEvent);
            sceneData.CompletedFlatOutputEvent.RemoveListener(OnCompletedFlatOutputEvent);
            sceneData.SessionLoadedEvent.RemoveListener(OnSessionLoadedEvent);
            sceneData.PlaybackErrorEvent.RemoveListener(OnPlaybackError);

            // didEnterView = false;
            
            // Reset AR Session completely.
            LoaderUtility.Deinitialize();
            LoaderUtility.Initialize();
        }

        /// <summary>
        /// IsValidNextState should implement if the nextState is valid. If all states are valid next states, just implement
        /// return true;
        /// </summary>
        /// <param name="nextState"></param>
        /// <returns></returns>
        public override bool IsValidNextState(NxAppState<NxAppManager> nextState)
        {
            return nextState != null;
        }
        #endregion
        
        #region OptionalMethods
        /// <summary>
        /// called directly after the machine and context are initialized allowing the state to do any required one-time setup
        /// </summary>
        public override void OnInitialized()
        {

        }
        
        /// <summary>
        /// LateUpdate is called every frame after all Update functions (including animation) have been called and
        /// gives you the delta time since last LateUpdate.
        /// </summary>
        /// <param name="deltaTime"></param>
        public override void LateUpdate(float deltaTime)
        {
        }

        /// <summary>
        /// Did enter gets called before instantiating the prefab (if there is one) and lets you know which state it came from.
        /// </summary>
        /// <param name="fromPrevState"></param>
        public override void WillEnterFrom(NxAppState<NxAppManager> fromPrevState)
        {
            // Cast persistentData to ARSceneData
            sceneData = (ARSceneData) PersistentData;
            Debug.Log("entering from: " + fromPrevState);
            if (fromPrevState?.GetType() == typeof(RecordingAudioState))
            {
                // if coming from rec audio, hide planes
                sceneData.SetPlanesHidden(false);
            }
            else
            {
                // otherwise hide them
                sceneData.SetPlanesHidden(true);
            }
            

        }
        
        /// <summary>
        /// WillExitTo gets called before destroying the prefab, and tells you which state is about to be loaded.
        /// </summary>
        /// <param name="nextState"></param>
        public override void WillExitTo(NxAppState<NxAppManager> nextState)
        {
            var appManager = (AppManager) _context;
            var globalData = (GlobalSessionData) appManager.GlobalAppData;
            globalData.PushBackState(this);
        }
        

        #endregion

    }
}
