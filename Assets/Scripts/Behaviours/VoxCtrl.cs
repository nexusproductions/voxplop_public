using System;
using System.Collections;
using AppStructure;
using UnityEngine;
using UnityEngine.Events;
using AppStructure.Data;

namespace NexusStudios.VoxPlop
{
    /// <summary>
    /// Controls lifecycle and interactions of Vox prefab in scene. Attached to root of Vox Prefab.
    /// </summary>
    public class VoxCtrl : MonoBehaviour
    {
        /// <summary>
        /// The associated data object.
        /// </summary>
        public VoxData voxData;

        [NonSerialized]
        public Vector3? LocomotionTarget = null;
        /// <summary>
        /// Scene Data
        /// </summary>
        public ARSceneData sceneData;
        public CharacterMovementController CMC;
        public CharacterExpressionController CEC;
        public GameObject Model;
        
        
        [HideInInspector] public AudioClip AudioClip;
        [HideInInspector] public VoxCtrl OtherVox = null;
        public enum DualVoxModes
        {
            None,
            Primary,
            Secondary
        }
        [HideInInspector] public DualVoxModes DualVoxMode = DualVoxModes.None;

        [HideInInspector] public bool ActiveVox = true;
        bool bIsBeingPlaced = true;
        private bool spawned = false;
        private bool speaking = false;
        private bool finished = false;

        private int audioLoops = 0;

        public UnityEvent OnFallingSpawn;
        
        /// <summary>
        /// The Global Session Data, set in inspector, used to get video recording length
        /// </summary>
        public GlobalSessionData globalData;

        public void Awake()
        {
            Model.SetActive(false);
        }

        void Start()
        {
            Model.SetActive(false);
        }

        private void Update()
        {
            if (finished || !ActiveVox) return;

            // Handle speaking, figure out if we need to loop or not
            if (AudioClip!=null && (DualVoxMode==DualVoxModes.None || (DualVoxMode==DualVoxModes.Primary && audioLoops==0)) && audioLoops<3 && ((!speaking && sceneData.playbackTimeRemaining > AudioClipLength()) || audioLoops==0))
            {
                audioLoops++;
                Speak();
            }
            
            // Is it time for a finishing animation?
            if (sceneData.playbackTimeRemaining <= CMC.FinishEmoteDuration)
            {
                CMC.RandomFinish();
                finished = true;
            }
        }

        /// <summary>
        /// Speak the audioclip we have stored
        /// </summary>
        public void Speak()
        {
            if (AudioClipLength() < sceneData.playbackTimeRemaining)
            {
                Debug.Log(voxData.name + " is starting to speak. Loops: " + audioLoops + "  time left: "+sceneData.playbackTimeRemaining + " clip length: " + AudioClipLength());
                CEC.Speak(AudioClip);
                speaking = true;
            }
        }
        
        /// <summary>
        /// Called when the audio clip is done playing 
        /// </summary>
        public void DoneSpeakingCallback()
        {
            speaking = false;
            if (DualVoxMode != DualVoxModes.None)
            {
                audioLoops++;
                Debug.Log(voxData.name + " Done speaking. Loops: " + audioLoops + "  time left: "+sceneData.playbackTimeRemaining + " clip length: " + AudioClipLength());
                if (audioLoops<=6 && AudioClip!=null)
                    OtherVox.Speak();
            }
        }
        
        public float AudioClipLength()
        {
            return AudioClip.length / CEC.Pitch;
        }
        /// <summary>
        /// Bring the vox into the scene at the current place - play the intro animation
        /// </summary>
        public void Spawn(bool fallIntoPlace)
        {
            CMC.MakeAnEntrance(fallIntoPlace);
            sceneData.StartRecordingEvent.AddListener(StartRecording);
            sceneData.StopRecordingEvent.AddListener(StopRecording);

            //Only called when falling. Does not fall in the RecordingAudioState.
            if(fallIntoPlace)
                OnFallingSpawn?.Invoke();
            StartCoroutine(showModelNextFrame());
        }

        IEnumerator showModelNextFrame()
        {
            yield return new WaitForEndOfFrame();
            Model.SetActive(true);
        }

        /// <summary>
        /// Triggers "listening" state for voxes. Called in audio recording scene.
        /// </summary>
        private void StartRecording()
        {
            CMC.Listen();
        }

        /// <summary>
        /// Returns voxes to idle state after listening to recording af user audio.
        /// </summary>
        private void StopRecording()
        {
            CMC.Idle();
        }
        
        /// <summary>
        /// Called when a VOX is being placed in the scene
        /// </summary>
        /// <param name="_beingPlaced"></param>
        public void SetBeingPlaced(bool _beingPlaced)
        {
            bIsBeingPlaced = _beingPlaced;
            if (bIsBeingPlaced) 
                sceneData.PitchChangedEvent.AddListener(OnPitchChangedEvent);
            else 
                sceneData.PitchChangedEvent.RemoveListener(OnPitchChangedEvent);
        }

        /// <summary>
        /// Attach the camera to the locomotion controller 
        /// </summary>
        /// <param name="camera">Transform of the AR camera in the scene</param>
        public void SetCamera(Transform camera)
        {
            CMC.ARCamera = camera;
        }
        
        /// <summary>
        /// Immediately rotates the character to face toward the camera without tilting.
        /// </summary>
        public void TurnToFaceCamera()
        {
            // create the ground plane.
            var originPlane = new Plane(transform.up, transform.position);
            // find a point on the ground plane nearest the camera.
            var lookAtPoint = originPlane.ClosestPointOnPlane(Camera.main.transform.position);
            // rotate.
            transform.LookAt(lookAtPoint);
        }

        void OnDestroy()
        {
            // clean up.
            sceneData.PitchChangedEvent.RemoveListener(OnPitchChangedEvent);
        }

        /// <summary>
        /// Set the audio clip pitch
        /// </summary>
        /// <param name="pitch">The pitch to set</param>
        public void SetPitch(float pitch)
        {
            CEC.Pitch = pitch;
        }

        /// <summary>
        /// Change pitch of AudioSource on vox. Called by event.
        /// </summary>
        /// <param name="pitch">The new pitch</param>
        void OnPitchChangedEvent(float pitch)
        {
            SetPitch(pitch);
        }

        /// <summary>
        /// Set the target for the VOX to move to
        /// </summary>
        /// <param name="pos">Position to move to</param>
        /// <returns>True if the vox can move there at this point</returns>
        public bool SetLocomotionTarget(Vector3 pos)
        {
            LocomotionTarget = pos;
            return CMC.JumpToTarget(pos);
        }

        /// <summary>
        /// Do a random emote, or a mirror emote if we have two voxes 
        /// </summary>
        public void RandomEmote()
        {
            if (DualVoxMode != DualVoxModes.None && sceneData.playbackTimer<3.5f)
            {
                MirrorEmote();
                OtherVox.MirrorEmote();
            }
            else
            {
                CMC.RandomEmote();
            }
        }

        /// <summary>
        /// Do a "mirror" emote, only happens when there are two VOXes in the scene
        /// </summary>
        public void MirrorEmote()
        {
            StartCoroutine(mirrorEmote());
        }

        IEnumerator mirrorEmote()
        {
            CMC.TurnToTarget(OtherVox.transform.position);
            yield return new WaitForSeconds(0.5f);
            CMC.Mirror();
        }
    }
}

    







