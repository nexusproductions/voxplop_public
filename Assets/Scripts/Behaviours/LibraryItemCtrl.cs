using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

using AppStructure.Data;

using TMPro;

using UnityEngine.UI;

using AppStructure.Views;
using UnityEngine.Serialization;

namespace NexusStudios.VoxPlop
{
    /// <summary>
    /// Manages UI element for displaying an individual piece of content in <see cref="LibraryView"/>.
    /// </summary>
    public class LibraryItemCtrl : MonoBehaviour
    {
        /// <summary>
        /// The data structure for the associated content. Should be set upon creation (before start).
        /// </summary>
        public LibraryItem item;
        
        /// <summary>
        /// The view to which this element belongs. Should be set upon creation by said parent.
        /// </summary>
        [HideInInspector]
        public LibraryView parentView;

        /// <summary>
        /// The UI element of the thumbnail image of the video to display in the library
        /// </summary>
        public RawImage thumbnail;

        /// <summary>
        /// The UI element of the icon to display if the video contains a vox.
        /// </summary>
        public RawImage voxIcon;
        
        /// <summary>
        /// The icon to display if there are multiple voxes in a video.
        /// </summary>
        public Texture multipleIconTex;

        // Start is called before the first frame update
        void Start()
        {
            // update UI subcomponents
            GetComponentInChildren<TMP_Text>().text = item.filename;
            thumbnail.texture = item.thumbnail;

            switch (item.type)
            {
                case LibraryItemType.PloppedVideo_Solo:
                    voxIcon.texture = item.embeddedVox.smallIcon;
                    break;
                case LibraryItemType.PloppedVideo_Multiple:
                    voxIcon.texture = multipleIconTex;
                    break;
                default:
                    voxIcon.gameObject.SetActive(false);
                    break;
            }
            
            Debug.Log("made item. " + item.thumbnail);
        }

        /// <summary>
        /// Notifies parent view that it was selected.
        /// </summary>
        public void OnClick()
        {
            Debug.Log(item.filename + " clicked");

            parentView.OnItemSelected(item);
        }
    }
}

