using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using AppStructure;
using AppStructure.Data;

#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif



namespace NexusStudios.VoxPlop
{
    /// <summary>
    /// Handles recording of microphone input to AudioClip.
    /// </summary>
    public class AudioRecorder : MonoBehaviour
    {
        /// <summary>
        /// Scene data. Set in inspector.
        /// </summary>
        public ARSceneData sceneData;

        /// <summary>
        /// The current audioclip being recorded
        /// </summary>
        AudioClip clip;

        /// <summary>
        /// Flag for if the recorder is currently active
        /// </summary>
        bool bIsRecording = false;

        /// <summary>
        /// Current time (in seconds) into recording.
        /// </summary>
        float currentRecTime = 0;

        /// <summary>
        /// The Global Session Data, set in inspector, used to get video recording length
        /// </summary>
        public GlobalSessionData globalData;
        
        // Start is called before the first frame update
        void Start()
        {
            // start listening to recording events
            sceneData.StartRecordingEvent.AddListener(OnStartRecording);
            sceneData.StopRecordingEvent.AddListener(OnStopRecording);

            // list microphone devices
            foreach (var device in Microphone.devices)
            {
                Debug.Log("Found mic device: " + device);
            }
            // check for permissions
#if PLATFORM_ANDROID
            if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
            {
                Permission.RequestUserPermission(Permission.Microphone);
            }
#endif
        }

        private void Update()
        {
            // if it's recording, update progress
            if (bIsRecording) UpdateRecProgress();

        }
        
        private void OnDestroy()
        {
            // remove listeners
            sceneData.StartRecordingEvent.RemoveListener(OnStartRecording);
            sceneData.StopRecordingEvent.RemoveListener(OnStopRecording);

        }

        /// <summary>
        /// Start recording audio clip. Called from UI event.
        /// </summary>
        void OnStartRecording()
        {
            // start recording using default microphone
            clip = Microphone.Start(null, false, 100, 44100);
            bIsRecording = true;
        }

        /// <summary>
        /// Stop recording audio clip, as set sidecar's active vox audio to the new recording. Called from UI event.
        /// </summary>
        void OnStopRecording()
        {
            int lastTime = Microphone.GetPosition(null);

            if (lastTime > 0)
            {
                Microphone.End(null);

                // Create a shorter audio clip with just the part that was recorded 
                float[] samples = new float[clip.samples]; 
                clip.GetData(samples, 0);
                float[] ClipSamples = new float[lastTime];
                Array.Copy(samples, ClipSamples, ClipSamples.Length - 1);
                clip = AudioClip.Create("playRecordClip", ClipSamples.Length, 1, 44100, false, false);
                clip.SetData(ClipSamples, 0);
            
                Debug.Log("OnStopRecording: " + clip.length);
            
                sceneData.sidecar.voxes[sceneData.activeVoxIndex].audioClip = clip;
                bIsRecording = false;

                sceneData.StoppedRecordingEvent.Invoke();
            }
            else
            {
                Debug.Log("Audio clip too short!!");
                
                // reset rec time
                bIsRecording = false;
                currentRecTime = 0;
                var normalizedProgress = Mathf.InverseLerp(0, globalData.audioRecLengthMax, currentRecTime);
                sceneData.RecProgressUpdateEvent.Invoke(normalizedProgress);
                
                sceneData.RecordingErrorEvent.Invoke();
                
            }

        }

        /// <summary>
        /// Check current record time against maximium, update listeners.
        /// </summary>
        void UpdateRecProgress()
        {
            var maxRecTime = globalData.audioRecLengthMax;

            // if the current time is under the maximum
            if (currentRecTime < maxRecTime)
            {
                // normalize time between 0 and maximum audio recording time
                var normalizedProgress = Mathf.InverseLerp(0, maxRecTime, currentRecTime);
                sceneData.RecProgressUpdateEvent.Invoke(normalizedProgress);

                // update time
                currentRecTime += Time.deltaTime;
            }
            // if it's over, stop
            else
            {
                Debug.Log("hit rec time limit");
                OnStopRecording();
            }
        }
    }
}
