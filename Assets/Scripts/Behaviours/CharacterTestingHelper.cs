using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace NexusStudios.VoxPlop
{
    public class CharacterTestingHelper : MonoBehaviour
    {
        public GameObject User;
        public GameObject[] Prefabs;
        public List<AudioClip> Clips;
        
        List<VoxCtrl> VoxCtrls = new List<VoxCtrl>();
        public TouchCircle touchCircle;

        public int CMCIndex = 0;

        private int plopCount = 0;
        
        void Update()
        {
            
            if (Input.GetMouseButtonDown(0))
            {

                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    
                    if (hit.transform.tag == "Vox")
                    {
                        hit.transform.GetComponent<VoxCtrl>().RandomEmote();
                        hit.transform.DOPunchScale(Vector3.one*0.02f, 0.5f,10,3);
                    }
                    else
                    {
                        touchCircle.SetPosition(hit.point);
 
                        
                        if (plopCount < Prefabs.Length)
                        {
                            VoxCtrl voxBeingPlaced = Instantiate(Prefabs[plopCount], hit.point,new Quaternion()).GetComponent<VoxCtrl>();
                            if (Clips.Count>0)
                                voxBeingPlaced.AudioClip = Clips[plopCount];
                            voxBeingPlaced.SetCamera(User.transform);
                            voxBeingPlaced.TurnToFaceCamera();
                            voxBeingPlaced.Spawn(true);
                            plopCount++;
                            
                            VoxCtrls.Add(voxBeingPlaced);
                            if (VoxCtrls.Count == 2)
                            {
                                VoxCtrls[0].DualVoxMode = VoxCtrl.DualVoxModes.Primary;
                                VoxCtrls[1].DualVoxMode = VoxCtrl.DualVoxModes.Secondary;
                                VoxCtrls[0].OtherVox = VoxCtrls[1];
                                VoxCtrls[1].OtherVox = VoxCtrls[0];
                            }
                        }
                        else
                        {
                        
                            VoxCtrls[CMCIndex].SetLocomotionTarget(hit.point);
                        }
                        
                    }
                }
            }
            if (Input.GetMouseButtonDown(1))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    touchCircle.SetPosition(hit.point);
                    VoxCtrls[CMCIndex].SetLocomotionTarget(hit.point);
                }
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                VoxCtrls[CMCIndex].GetComponent<CharacterMovementController>().RandomEmote();
            }
            
            if (Input.GetKeyDown(KeyCode.F))
            {
                VoxCtrls[CMCIndex].GetComponent<CharacterMovementController>().RandomFinish();
            }
            
           
            if (Input.GetKeyDown(KeyCode.Space))
            {
                CMCIndex++;
                CMCIndex = CMCIndex % VoxCtrls.Count;
            }
            
        }
    }
}