using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace NexusStudios.VoxPlop
{
	/// <summary>
	/// Controls particle system playback.
	/// </summary>
	[RequireComponent(typeof(ParticleSystem))]
	public class ParticleFxController : MonoBehaviour
	{
		/// <summary>
		/// The particle system attached to this gameobject.
		/// </summary>
		private ParticleSystem ps;

		/// <summary>
		/// Play or stop particle emission.
		/// </summary>
		/// <param name="enable"></param>
		public void EnableEmission(bool enable)
		{
			if (ps == null)
				ps = GetComponent<ParticleSystem>();

			var emission = ps.emission;
			emission.enabled = enable;

			if (enable)
				ps.Play();
			else
				ps.Stop();
		}
	}
}