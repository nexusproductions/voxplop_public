using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace NexusStudios.VoxPlop
{
    /// <summary>
    /// Handles display of circle displayed when voxes are directed to a new location.
    /// </summary>
    public class TouchCircle : MonoBehaviour
    {
        public List<WaveformCircleController> waveformCircleControllers;

        /// <summary>
        /// Sets the position and enables visibility of the waveform circle effect.
        /// </summary>
        /// <param name="p"></param>
        public void SetPosition(Vector3 p)
        {
            transform.position = p;

            foreach (WaveformCircleController w in waveformCircleControllers)
            {
                w.ShowCircle(true);
            }
        }
    }
}