﻿using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.XR.Management;
using Google.XR.ARCoreExtensions;

using UnityEngine;

using AppStructure;
using AppStructure.Data;

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace  NexusStudios.VoxPlop
{
    

    /// <summary>
    /// Handles recording of ARCore sessions and embedding of <see cref="VoxSidecar"/> as ExternalDataTrack.
    /// </summary>
    public class ARRecording : MonoBehaviour
    {
        /// <summary>
        /// Scene Data. Set in inspector.
        /// </summary>
        public ARSceneData sceneData;

        /// <summary>
        /// ARSession in current scene. Set in inspector.
        /// </summary>
        public ARSession ARSession;
      
        /// <summary>
        /// ARCameraManager in current scene, used to grab ARCameraFrameEventArgs. Set in inspector.
        /// </summary>
        public ARCameraManager cameraManager;
        
        /// <summary>
        /// Out custom ExternalDataTrack for saving our sidecar.
        /// </summary>
        Track customDataTrack;

        /// <summary>
        /// A unique ID for our datatrack.
        /// </summary>
        System.Guid extID;

        /// <summary>
        /// Acts as flag for if the ARSession should load a file during Update().
        /// </summary>
        bool bShouldTriggerPlayback = false;

        /// <summary>
        /// Flags if the ARSession is currently playing a file
        /// </summary>
        /// TODO: could this just use an ARSession status check?
        bool bIsPlaying = false;

        /// <summary>
        /// How long (in seconds) will it attempt to load an AR Session before failing and returning to live?
        /// </summary>
        float playbackTimeout = 3f;

        /// <summary>
        /// The path of the mp4 to play from or record to
        /// </summary>
        /// FIXME: should we make those discrete?
        string filepath;

        /// <summary>
        /// The path of the file currently being played
        /// </summary>
        string tempPath;
        
        /// <summary>
        /// The path of the file currently being recorded.
        /// </summary>
        string tempRecordPath;
        
        /// <summary>
        /// A flag for if the ARSession is currently recording to a file.
        /// </summary>
        /// FIXME: could this be a status check?
        bool bIsRecording = false;

        /// <summary>
        /// Tracks how long the current file has been recording. Used for rec progress update and hitting max rec time.
        /// </summary>
        float currentRecTime = 0;

        /// <summary>
        /// A flag for if the recorder should start recording a session in Update()
        /// </summary>
        /// FIXME: rename.
        bool bShouldAddExternalSample = false;

        /// <summary>
        /// The status of the attempt to record the sidecar to the ExternalDataTrack. Allows us to retry attempts across multiple Update loops.
        /// </summary>
        RecordingResult recordingResult;

        /// <summary>
        /// Suffix for file to allow for separation of raw version from sidecar embedded version
        /// </summary>
        /// FIXME: wonky. rethink.
        string filenameSuffix = "raw";

        private ARRecordingManager recManager;
        private ARPlaybackManager playManager;

        private bool bDidLocalizeOnce;
        private bool bGotFirstFrame;

        /// <summary>
        /// The Global Session Data, set in inspector, used to get video recording length
        /// </summary>
        public GlobalSessionData globalData;
        void Awake()
        {
            // setup temp path 
            tempPath = Application.persistentDataPath +"/temp_play.mp4";
            
            // Hook up to recording and output events before state begins
            if(sceneData != null ){
                sceneData.StartRecordingEvent.AddListener(OnStartRecordingEvent);
                sceneData.StopRecordingEvent.AddListener(OnStopRecordingEvent);

                sceneData.StartPlaybackEvent.AddListener(OnStartPlaybackEvent);
                sceneData.StopPlaybackEvent.AddListener(OnStopPlaybackEvent);

                sceneData.StartFlatOutputEvent.AddListener(OnStartFlatOutputEvent);
                sceneData.StartSpatialOutputEvent.AddListener(OnStartSpatialOutputEvent);
        
                sceneData.SaveVideoEvent.AddListener(OnSaveVideoEvent);
            }

            Debug.Log("AR Recording script awake...");
        }

        // Start is called before the first frame update
        void Start()
        {
            // https://developer.android.com/reference/android/media/MediaMuxer#metadata-track
            // create unique ID for ExternalDataTrack
            extID = System.Guid.NewGuid();

            // set track ID, metadata and mimetype
            customDataTrack.Id = extID;
            customDataTrack.Metadata = System.Text.Encoding.UTF8.GetBytes("custom");
            customDataTrack.MimeType = "application/custom";

            // start listening
            cameraManager.frameReceived += OnCameraFrameReceived;

            // get play and rec managers
            recManager = GetComponent<ARRecordingManager>();
            playManager = GetComponent<ARPlaybackManager>();

            // if it has a file currently in data, play it.
            if(sceneData.currentFilename != "" ) {
                Debug.Log("starting with filename: "+ sceneData.currentFilename);

                // this is a workaround. Without a delay, it cannot play a recording on first load
                // it seems like it needs to initialize the session once before playback begins.
                StartCoroutine(StartPlaybackWithDelay(0.5f));
                bShouldTriggerPlayback = false;
            }
            else
            {
                // ensure it starts live if not loading
                Debug.Log("starting live...");
                filepath = tempPath;

                StopPlayback();
            }

            ARSession.stateChanged += OnARSessionStateChange;
            
            Debug.Log("AR Recording script started...");
        }

        private void OnDestroy() 
        {
            // clean up listeners
            if(sceneData != null)
            {
                sceneData.StartRecordingEvent.RemoveListener(OnStartRecordingEvent);
                sceneData.StopRecordingEvent.RemoveListener(OnStopRecordingEvent);

                sceneData.StartPlaybackEvent.RemoveListener(OnStartPlaybackEvent);
                sceneData.StopPlaybackEvent.RemoveListener(OnStopPlaybackEvent);

                sceneData.StartFlatOutputEvent.RemoveListener(OnStartFlatOutputEvent);
                sceneData.StartSpatialOutputEvent.RemoveListener(OnStartSpatialOutputEvent);

                sceneData.SaveVideoEvent.RemoveListener(OnSaveVideoEvent);

            }

            ARSession.stateChanged -= OnARSessionStateChange;

            // disable session to make sure there's no interference in other scenes
            Debug.Log("Destroying ARRecording...");
            ARSession.enabled = false;
        }

        /// <summary>
        /// Updates timestamp. Called by ARCAmeraManager's FrameUpdate.
        /// </summary>
        /// <param name="eventArgs">Frame data</param>
        void OnCameraFrameReceived(ARCameraFrameEventArgs eventArgs)
        {
            // Debug.Log("got frame.. " + eventArgs );
            if (!bGotFirstFrame)
            {
                sceneData.SessionLoadedEvent.Invoke(ARSession.state);
                bGotFirstFrame = true;
            }
        }

        // Update is called once per frame
        void Update()
        {
            // if it should start playback, play.
            if (bShouldTriggerPlayback)
            {
                // attempt to load ARSession from mp4
                PlaybackResult result = playManager.SetPlaybackDataset(filepath);
                Debug.Log($"Triggering Playback of {filepath}: {result}");

                // if playback failed outright or it has not loaded yet, count down timeout.
                if (result == PlaybackResult.ErrorPlaybackFailed || result == PlaybackResult.SessionNotReady)
                {
                    //Debug.Log("Waiting To Be Ready: " + playbackTimeout);
                    playbackTimeout -= Time.deltaTime;
                }
                // otherwise, clear timeout.
                else
                {
                    playbackTimeout = -1f;
                }

                // if the timeout's done
                if (playbackTimeout < 0.0f)
                {
                    // flag playback so it doesn't re-trigger next frame
                    bShouldTriggerPlayback = false;

                    // if playback is ok, flag playing
                    if (result == PlaybackResult.OK)
                    {
                        Debug.Log("STARTED PLAYING.");
                        if (filepath != null)
                        {
                            bIsPlaying = true;
                        }

                        sceneData.playbackTimer = 0;
                        sceneData.playbackTimeRemaining = TimeLeft();
                    }
                    // otherwise, flag failure, go live.
                    else
                    {
                        Debug.LogWarning("error playing back recording");
                        ARSession.enabled = true;
                    }
                }
            }
            // if it shouldn't start, go to live session
            else
            {
                //FIXME: this shouldn't happen every frame
                ARSession.enabled = true;
            }

            // if it is playing back
            if (bIsPlaying)
            {
                sceneData.playbackTimer += Time.deltaTime;
                sceneData.playbackTimeRemaining = TimeLeft();
                // notify on first localized
                if (ARSession.state == ARSessionState.SessionTracking && !bDidLocalizeOnce)
                {
                    sceneData.ARSessionLocalizedEvent.Invoke();
                    bDidLocalizeOnce = true;
                    Debug.Log("localized");
                }

                // if it has reached the end of the file, loop and fire completion events
                if (playManager.PlaybackStatus == PlaybackStatus.FinishedSuccess)
                {
                    Debug.Log("finished. looping.");
                    
                    StartPlayback();

                    // if it finishes playback and is recording (for adding external data track), stop recording
                    // FIXME: hook to recordingstatus?
                    if(bIsRecording){
                        StopRecording();
                    }

                    // notify others playback is done (for outputting)
                    sceneData.CompletedPlaybackEvent.Invoke();
                    
                }
                // if it should start recording with external data, do so
                else if (bShouldAddExternalSample)
                {
                    // change file suffix for new file
                    filenameSuffix = "embedded";

                    StartRecording();
                    // switch flag to not trigger next frame.
                    bShouldAddExternalSample = false;
                      
                }
                // if it is currently playing AND recording
                else if (bIsRecording)
                {
                    // if it hasn't suceeded in recording the sidecar, try to embed.
                    if (recordingResult != RecordingResult.OK)
                    {
                        recordingResult = EmbedSidecar();
                        Debug.Log("embed status: " + recordingResult);
                    }
                    else
                        Debug.Log("Embed ok!");
                }
            }

            //TODO: could this be a hook right to ARCore recordingstatus?
            if(bIsRecording) UpdateRecProgress();
        }

        /// <summary>
        /// Begins recording AR Session to mp4. Called by UI.
        /// </summary>
        public void OnStartRecordingEvent(){
            filenameSuffix = "raw";
            StartRecording();
        }

        /// <summary>
        /// Stops recording AR Session and begins playback. Called by UI.
        /// </summary>
        public void OnStopRecordingEvent(){
            StopRecording();
            StartPlayback();
        }

        /// <summary>
        /// Starts playback of AR Session. Probably called by output process.
        /// </summary>
        public void OnStartPlaybackEvent(){
            Debug.Log("Starting Playback Event..");
            StartPlayback();
        }

        /// <summary>
        /// Stops playback of AR session
        /// </summary>
        /// FIXME: is this ever called?
        public void OnStopPlaybackEvent(){
            StopPlayback();
        }

        /// <summary>
        /// Begins playback of AR Session and enables recording of sidecar as soon as playback status allows.
        /// </summary>
        public void StartPlaybackAndRecordSidecarToDatatrack(){
            Debug.Log("play and rec event");

            StartPlayback();

            // reset result for embed success check
            recordingResult = RecordingResult.ErrorRecordingFailed;

            // set flag read in update()
            bShouldAddExternalSample = true;
        }

        /// <summary>
        /// Extracts sidecar from current file
        /// </summary> <remarks>Only really used in testing. In app, sidecar extraction happens on load from library.</remarks>
        public void OnExtractDatatrackEvent(){
            Debug.Log("extract track event");
            sceneData.sidecar = LibraryManager.ExtractSidecar(sceneData.currentFilename, true);

            //FOR TESTING
            GetComponent<AudioSource>().PlayOneShot(sceneData.sidecar.voxes[0].audioClip);
        }

        /// <summary>
        /// Starts playback for rendering flattened video.
        /// </summary>
        public void OnStartFlatOutputEvent()
        {
            StartPlayback();
        }

        /// <summary>
        /// Embeds sidecar in video.
        /// </summary>
        public void OnStartSpatialOutputEvent()
        {
            StartPlaybackAndRecordSidecarToDatatrack();

        }

        /// <summary>
        /// Starts recording of ARSession to mp4.
        /// </summary>
        public void StartRecording()
        {
            // set new filename with current date and time.
            var timestamp = DateTime.Now.ToString("yyyyMMdd-HHmmss");
            sceneData.currentFilename = $"temp_{timestamp}.mp4";

            Debug.Log("Recording to: " + sceneData.currentFilename );

            List<Track> tracks = new List<Track>();
            tracks.Add(customDataTrack);
            
            // set recording configuration: path, stop on pause, datatrack.
            ARCoreRecordingConfig recordingConfig = ScriptableObject.CreateInstance<ARCoreRecordingConfig>();
            recordingConfig.Mp4DatasetFilepath = Application.persistentDataPath + "/" + sceneData.currentFilename; 
            recordingConfig.AutoStopOnPause = true;
            recordingConfig.Tracks = tracks;

            // begin recording.
            recManager.StartRecording(recordingConfig);
            Debug.Log("Start Recording Status: " + recManager.RecordingStatus);

            // set flag and reset time
            bIsRecording = true;
            currentRecTime = 0 ;
            
            sceneData.fileIsSaved = false;
            
            sceneData.SetPlanesHidden(false);
        }

        /// <summary>
        /// Informs listeners if a session has loaded and what state the session is entering.
        /// </summary>
        /// <param name="arSessionStateChangedEventArgs">The new AR session state</param>
        public void OnARSessionStateChange(ARSessionStateChangedEventArgs arSessionStateChangedEventArgs)
        {
            // is there a state diff between actually receiveing frames
            var newState = arSessionStateChangedEventArgs.state;
            Debug.Log("ar state change: " + newState);

            if (newState == ARSessionState.SessionInitializing || newState == ARSessionState.SessionTracking)
            {
                //sceneData.SessionLoadedEvent.Invoke( newState);
            }

        }

        /// <summary>
        /// Stops current session recording.
        /// </summary>
        public void StopRecording()
        {
            recManager.StopRecording();
            Debug.Log("Stop Recording Status: " + recManager.RecordingStatus);

            // set flag
            bIsRecording = false;

            sceneData.StoppedRecordingEvent.Invoke();
        }

        /// <summary>
        /// Starts playback of ARSession at sceneData's current file.
        /// </summary>
        public void StartPlayback()
        {
            // get file path
            filepath = Application.persistentDataPath + "/" + sceneData.currentFilename;
            Debug.Log("Starting Playback... " + filepath);

            // reset timeout, flag to load/ begin playback, disable live session, reset playing flag
            bShouldTriggerPlayback = true;
            ARSession.enabled = false;
            bIsPlaying = false;
            playbackTimeout = 3f;

            bDidLocalizeOnce = false;
            bGotFirstFrame = false;

        }


        /// <summary>
        /// Stop playback of current ARSession
        /// </summary>
        public void StopPlayback()
        {
            Debug.Log("Stopping playback and going live.");
            
            if (ARSession.state == ARSessionState.SessionInitializing || 
                ARSession.state == ARSessionState.SessionTracking)            
                ARSession.Reset();

            // FIXME: this is sloppy. why go through triggering playback logic just to stop?
            filepath = null;
            bShouldTriggerPlayback = true;
            ARSession.enabled = false;
            bIsPlaying = false;
            playbackTimeout = 1f;
            
            bGotFirstFrame = false;

        }

        /// <summary>
        /// Checks recording time to update UI elements and stop recording if it maxes out.
        /// </summary>
        void UpdateRecProgress()
        {
            var recTime = globalData.videoRecLength;
            // var minRecTime = globalData.videoRecLengthMin;

            // if it hasn't exceeded its maximum recording time, update listeners.
            if(currentRecTime < recTime)
            {
                // normalize progress between 0 and max recording time.
                var normalizedProgress = Mathf.InverseLerp (0, recTime, currentRecTime);
                sceneData.RecProgressUpdateEvent.Invoke(normalizedProgress);

                // update current recording time.
                currentRecTime += Time.deltaTime;

            }
            // if it has hit its upper limit, stop recording.
            else
            {
                Debug.Log("hit rec time limit");
                StopRecording();
            }
        }
        
        /// <summary>
        /// Saves video to library, responds to UI events.
        /// </summary>
        /// <param name="suffix">The suffix to add to the filename. For tracking file status.</param>
        public void OnSaveVideoEvent(string suffix)
        {
            var timestamp = DateTime.Now.ToString("yyyyMMdd-HHmmss");
            var destinationName = $"spatialVideo_{timestamp}_{suffix}.mp4";
            Debug.Log("saving to... "+ destinationName);

            var sourcePath = Application.persistentDataPath + "/" + sceneData.currentFilename;
            var destinationPath = Application.persistentDataPath + "/" + destinationName;
            
            File.Copy(sourcePath, destinationPath, true);

            FileInfo sourceInfo = new FileInfo(sourcePath);
            FileInfo destInfo = new FileInfo(destinationPath);

            if (sourceInfo.Length == destInfo.Length)
            {
                Debug.Log("SAVE OK");
                sceneData.SaveVideoResultEvent.Invoke(true);
            }
            else
            {
                Debug.LogWarning("Save Error");
                File.Delete(destinationPath);
                sceneData.SaveVideoResultEvent.Invoke(false);

            }
            
        }

        /// <summary>
        /// Adds an arbitrary string to the data track. Used for testing.
        /// </summary>
        /// <param name="txt">String to add to track</param>
        /// <returns>The result of the recording attempt</returns>
        public RecordingResult addExternalDataTrackSample(string txt)
        {
            Debug.Log("adding sample: " + txt + " " + extID);

            // convert string to bytes
            var bytes = System.Text.Encoding.UTF8.GetBytes(txt);

            // attempt write and return result
            return recManager.RecordTrackData(extID, bytes);

        }

        /// <summary>
        /// Embeds current sidecar as sample in ExternalDataTrack.
        /// </summary>
        /// <returns>The result of the recording attempt</returns>
        private RecordingResult EmbedSidecar(){
            // convert sidecar to JSON
            var payload = sceneData.sidecar.ToJSON();
            Debug.Log("attempting embed: "+ payload);

            // convert JSON to bytes
            var bytes = System.Text.Encoding.UTF8.GetBytes(payload);

            // attempt write and return result
            return recManager.RecordTrackData(extID, bytes);
        }
        
        /// <summary>
        /// Starts playback with a delay. Allows session time to initialize before playback begins.
        /// </summary>
        /// <param name="delay">Duration of delay.</param>
        /// <returns></returns>
        private IEnumerator StartPlaybackWithDelay(float delay)
        {
            yield return new WaitForSeconds(delay);
            StartPlayback();
        }

        /// <summary>
        /// How much time (seconds) is left in the playback of a video (if its playing)
        /// </summary>
        /// <returns></returns>
        public float TimeLeft()
        {
            float videoRecLength;
            if (globalData != null)
                videoRecLength = globalData.videoRecLength;
            else
                videoRecLength = 15;
            
            return videoRecLength-sceneData.playbackTimer;
        }
    }
}