﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


namespace NexusStudios.VoxPlop
{

    /// <summary>
    /// Audio sampler will give live information about an audio source as it's playing
    /// Used for audio visualizations. 
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class AudioSampler : MonoBehaviour
    {


        public float Decibels = 0;
        public float NormalizedVolume = 0;
        public float SmoothNormalizedVolume = 0;
        public float MaxDecibels = 0;
        
        
        private AudioSource audioSource;
        [SerializeField] private float speed = 10;
        private float sampleRate;
        private float[] samples;
        private float[] spectrum;
        int _sampleWindow = 64;
        float maxValueRolling = 0;
        float[] outputData;
        
        void Start()
        {
            audioSource = GetComponent<AudioSource>();
        }

        public void SetAudioSource(AudioSource s)
        {
            audioSource = s;
        }

        void Update()
        {
            spectrum = new float[256];
            AudioListener.GetSpectrumData(spectrum, 0, FFTWindow.Rectangular);
            float maxSpectrum = 0;
            for (int i = 1; i < spectrum.Length - 1; i++)
            {
                if (spectrum[i] > maxSpectrum)
                    maxSpectrum = spectrum[i];
            }
            //Debug.Log(maxSpectrum);

            outputData = new float[256];
            audioSource.GetOutputData(outputData, 0);
            float maxOutput = 0;

            for (int i = 1; i < outputData.Length - 1; i++)
            {
                if (outputData[i] > maxOutput)
                    maxOutput = outputData[i];
            }

            if (MaxDecibels < maxOutput)
                MaxDecibels = maxOutput;
            else
                MaxDecibels = maxOutput * 0.001f + MaxDecibels * 0.999f;


            Decibels = maxOutput;
            if (MaxDecibels != 0)
            {
                NormalizedVolume = Decibels / MaxDecibels;
                SmoothNormalizedVolume = Mathf.Lerp(SmoothNormalizedVolume, NormalizedVolume, 0.3f);
            }

        }
    }
}
