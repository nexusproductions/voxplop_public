using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppStructure.Data;

namespace NexusStudios.VoxPlop
{
/// <summary>
/// Controls the Waveform Circle material with pitch and volume reactive fx.
/// </summary>
    public class WaveformCircleController : MonoBehaviour
    {
		/// <summary>
		/// Scene Data. Set in inspector.
		/// </summary>
		public ARSceneData sceneData;

		/// <summary>
		/// Time to fade in the visibility.
		/// </summary>
		[SerializeField] private float m_fadeInTime = 0.2f;

		/// <summary>
		/// Time to hold at full visibility before fading out.
		/// </summary>
		[SerializeField] private float m_holdTime = 1f;

		/// <summary>
		/// Time to fade out the visibility.
		/// </summary>
		[SerializeField] private float m_fadeOutTime = 0.5f;

		private MaterialPropertyBlock m_mpb;
		private Renderer m_renderer;

		/// <summary>
		/// Used to set the material's visibility.
		/// </summary>
		private float m_visibility = 1;

		/// <summary>
		/// Material fades in while true and out while false.
		/// </summary>
		private bool m_show;

		//material property IDs
		private readonly int VISIBILITY_ID = Shader.PropertyToID("_Visibility");
		private readonly int PITCH_ID = Shader.PropertyToID("_Pitch");
		private readonly int AMPLITUDE_ID = Shader.PropertyToID("_AmplitudeScale");

		private void Awake()
		{
			m_renderer = GetComponent<Renderer>();
			m_mpb = new MaterialPropertyBlock();
		}

		private void OnEnable()
		{
			// Listen for events.
			sceneData.PitchChangedEvent.AddListener(OnPitchChangedEvent);
			sceneData.CompletedPlaybackEvent.AddListener(OnPlaybackCompleteEvent);

			// initialize the material based on the current pitch setting
			OnPitchChangedEvent(sceneData.sidecar.voxes[sceneData.activeVoxIndex].pitch);
		}

		private void OnDisable()
		{
			// Clean up listeners.
			sceneData.PitchChangedEvent.RemoveListener(OnPitchChangedEvent);
			sceneData.CompletedPlaybackEvent.RemoveListener(OnPlaybackCompleteEvent);
		}

		private void Update()
		{
			SetVisibility();
			SetAmplitude();
			SetMaterialPropertyBlock();
		}

		/// <summary>
		/// Remaps a value from an input range to an output range.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="iMin"></param>
		/// <param name="iMax"></param>
		/// <param name="oMin"></param>
		/// <param name="oMax"></param>
		/// <returns></returns>
		float Remap(float value, float iMin, float iMax, float oMin, float oMax)
		{
			float t = Mathf.InverseLerp(iMin, iMax, value);
			return Mathf.Lerp(oMin, oMax, t);
		}

		/// <summary>
		/// Sets the pitch in the waveform circle material and shows the circle.
		/// </summary>
		/// <param name="pitch"></param>
		void OnPitchChangedEvent(float pitch)
		{
			//pitch slider ranges from 0.5 to 2. Remap it to 0-1.
			pitch = Remap(pitch, 0.5f, 2f, 0f, 1f);
			m_mpb.SetFloat(PITCH_ID, pitch);

			//show the circle when changing the pitch slider.
			ShowCircle(false);
		}

		/// <summary>
		/// Hides the circle when playback is complete so it's not visible on a blank screen.
		/// </summary>
		void OnPlaybackCompleteEvent()
		{
			m_show = false;
			m_visibility = 0;
		}

		/// <summary>
		/// Shows the waveform circle for a period of time before fading out. Reset to fade from 0 visibility.
		/// </summary>
		public void ShowCircle(bool reset)
		{
			if (reset)
				m_visibility = 0;

			m_show = true;

			CancelInvoke();
			Invoke("HideCircle", m_holdTime + m_fadeInTime);
		}

		/// <summary>
		/// Sets bool to fade the waveform circle to 0 visibility.
		/// </summary>
		public void HideCircle()
		{
			m_show = false;
		}

		/// <summary>
		/// Fades the visibility in or out.
		/// </summary>
		void SetVisibility()
		{
			if (m_show)
				m_visibility += Time.deltaTime / m_fadeInTime;
			else
				m_visibility -= Time.deltaTime / m_fadeOutTime;

			m_visibility = Mathf.Clamp01(m_visibility);

			m_mpb.SetFloat(VISIBILITY_ID, m_visibility);
		}

		/// <summary>
		/// Changes the amplitude of the waveform circle based on audio feedback.
		/// </summary>
		void SetAmplitude()
		{
			if (sceneData.sidecar.voxes.Count == 0)
				return;

			AudioSampler audioSampler = sceneData.sidecar.voxes[sceneData.activeVoxIndex].voxData.audioSampler;

			float amplitude = 0;

			if (audioSampler != null)
				amplitude = audioSampler.NormalizedVolume;

			//make normalized volume more responsive and always show a minimum amount of wave animation
			amplitude = Mathf.Clamp(amplitude * 20f, 0.3f, 1f);

			m_mpb.SetFloat(AMPLITUDE_ID, amplitude);
		}

		/// <summary>
		/// Apply updated material properties. 
		/// </summary>
		void SetMaterialPropertyBlock()
		{
			m_renderer.SetPropertyBlock(m_mpb);
		}
	}
}