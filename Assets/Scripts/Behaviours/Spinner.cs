using UnityEngine;

namespace NexusStudios.VoxPlop
{
    /// <summary>
    /// Spins UI element displayed while video renders to flat movie.
    /// </summary>
    public class Spinner : MonoBehaviour
    {
        /// <summary>
        /// The speed, in degrees per second, to spin.
        /// </summary>
        public float speed;
        
        void Update()
        {
            var eulerRot = transform.eulerAngles;
            eulerRot.z += Time.deltaTime * speed;
            transform.rotation = Quaternion.Euler(eulerRot);
            
        }
    }
}