using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using AppStructure.Data;

namespace NexusStudios.VoxPlop{

    /// <summary>
    /// Adjusts pitch of attached AudioSource when pitch is changed.
    /// </summary>
    public class PitchShiftListener : MonoBehaviour
    {
        /// <summary>
        /// Scene Data. Set in inspector.
        /// </summary>
        public ARSceneData sceneData;

        /// <summary>
        /// The attached AudioSource component. Set on start.
        /// </summary>
        AudioSource source;

        // Start is called before the first frame update
        void Start()
        {
            // Start listening.
            sceneData.PitchChangedEvent.AddListener(OnPitchChangedEvent);   
            
            // Find component.
            source = GetComponent<AudioSource>();
        }

        void OnDestroy(){
            
            // clean up.
            sceneData.PitchChangedEvent.RemoveListener(OnPitchChangedEvent);   

        }
        
        /// <summary>
        /// Sets pitch of attached component. Called by event.
        /// </summary>
        /// <param name="pitch">The new pitch value</param>
        void OnPitchChangedEvent(float pitch){
            source.pitch = pitch;
        }

    }




}
