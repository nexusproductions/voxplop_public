using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

using AppStructure.Data;


namespace NexusStudios.VoxPlop
{

    /// <summary>
    /// Sets ARPlane visibility in scene based on <see cref="ARSceneData"/> variables and events. Attached to GameObject in scene with the ARPlaneMeshVisualizer.
    /// </summary>
    public class SetMeshVisibilityFromSceneData : MonoBehaviour
    {
        /// <summary>
        /// The scene data.
        /// </summary>
        public ARSceneData sceneData;
        
        // Start is called before the first frame update
        void Start()
        {
            // Set plane visualizer component to current sceneData Values
            // You have to set the visualizer as well as renderers or it will override your changes in its Update().
            // Also, it prevents new planes from spawning visible.
            GetComponent<ARPlaneMeshVisualizer>().enabled = !sceneData.planesHidden;
            
            // Enable/Disable renderers based on current state
            foreach (var rend in GetComponentsInChildren<Renderer>() ){
                rend.enabled = !sceneData.planesHidden;
            }
            
            // start listening for change events.
            sceneData.PlaneHiddenChangedEvent.AddListener(OnPlaneHiddenChangedEvent);
        }
        
        private void OnDestroy() 
        {
            // clean up.
            sceneData.PlaneHiddenChangedEvent.RemoveListener(OnPlaneHiddenChangedEvent);
        }

        /// <summary>
        /// Sets plane visibilty. Called by event.
        /// </summary>
        /// <param name="hidden">Plane visibility</param>
        public void OnPlaneHiddenChangedEvent(bool hidden)
        {
            Debug.Log("set plane visibility.");
            
            // set visualizer component to new value.
            GetComponent<ARPlaneMeshVisualizer>().enabled = !sceneData.planesHidden;
            
            // set renderers to new values
            foreach (var rend in GetComponentsInChildren<Renderer>() )
            {
                rend.enabled = !sceneData.planesHidden;
            }
        }
    }


}

