using System;
using System.Collections;
using System.Collections.Generic;
using AppStructure.Data;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

namespace NexusStudios.VoxPlop
{

    public class CharacterMovementController : MonoBehaviour
    {
        public VoxName Name;
        [Header("References")]
        public Transform CharacterRootBone;
        public Transform ARCamera;
        public Collider CharacterCollider;
        public Animator Animator;
        public Renderer ShadowPlaneRenderer;    
        
        [Header("Animations")] 
        public Emotes Emotes;
        public Emotes Finishs;
        public Emotes.Emote ListeningEmote;
        public Emotes.Emote MirrorEmote;
        
        [Header("Timing, speeds and distances")] 
        public float FallFromHeight = 2;
        public float FallingSpeed = 1;
        public float WalkingSpeed = 1;
        public float TurnDuration = 0.5f;
        public float JumpHeight = 1;
        public float JumpDuration = 1;
        public float CrossfadeDuration = 0.2f;
        
        public enum LocomotionStates
        {
            None,
            Falling,
            WalkToTarget,
            JumpToTarget,
            Emoting,
            Blocked,
            Finishing
        }
        private Vector3 locomotionTarget;
        private Vector3 startingPosition;
        private int shortEmotesTriggersIndex = 0;
        private LocomotionStates LocomotionState = LocomotionStates.None;
        private float turning = 0;
        private float turningTarget = 0;
        private float walkingSpeed;
        private float inTheAirY;
        private bool inited = false;
        private float scaler = 1;
        private static readonly int ShadowStrengthProp = Shader.PropertyToID("_ShadowStrength");
        private Emotes.Emote finishEmote;
        private Tweener turningTween;
        
        [HideInInspector]
        public float FinishEmoteDuration;
        
        void Awake()
        {
            LocomotionState = LocomotionStates.None;

        }

        /// <summary>
        /// A little helper to make things happen after a delay
        /// </summary>
        void Wait(float seconds, Action action)
        {
            StartCoroutine(_wait(seconds, action));
        }
        IEnumerator _wait(float time, Action callback)
        {
            yield return new WaitForSeconds(time);
            callback();
        }
        
        /// <summary>
        /// Direct control over bones is done in LateUpdate so it overwrites animation
        /// </summary>
        private void LateUpdate()
        {
            if (!inited) return;
            
            // Handle falling
            if (LocomotionState == LocomotionStates.Falling)
            {
                if (inTheAirY>0)
                    inTheAirY -= FallingSpeed * Time.deltaTime;
                
                if (inTheAirY <= 0)
                {
                    LocomotionState = LocomotionStates.Blocked;
                    CharacterRootBone.transform.localPosition = startingPosition;
                    Animator.SetTrigger("Land");
                    Wait (1, () => 
                    {
                        Idle();
                    });
                }
                else
                {
                    CharacterRootBone.transform.localPosition = startingPosition + new Vector3(0, inTheAirY, 0);
                }
                
                
            }

            // Handle jumping
            if (LocomotionState == LocomotionStates.JumpToTarget || LocomotionState == LocomotionStates.Finishing)
            {
                // Move the root bone to make our character airborne
                if (inTheAirY>=0)
                    CharacterRootBone.transform.localPosition = startingPosition + new Vector3(0, inTheAirY, 0);
            }
            
            // Turn towards the jump target, but only if we're not too close (and only Barry)
            // if (LocomotionState == LocomotionStates.JumpToTarget)
            // {
            //     Vector3 FlooredCameraPosition = new Vector3(ARCamera.position.x, transform.position.y, ARCamera.position.z);
            //     Vector3 targetDir = FlooredCameraPosition - transform.position;
            //     float angle = Vector3.SignedAngle(targetDir, transform.forward, transform.up);
            //     turningTarget = -Mathf.Clamp(angle, -30f, 30f) / 30f;
            //     turning = turning * 0.98f + turningTarget * 0.02f;
            //     if (Mathf.Abs(turning)> 0.3f && Vector3.Distance(locomotionTarget ,transform.position)>0.2)
            //         transform.Rotate(transform.up,turning*Time.deltaTime*TurningSpeed*3f);
            // }
        }


        void Update()
        {
            if (!inited) return;
            
            // Handle walking to a target
            if (LocomotionState == LocomotionStates.WalkToTarget)
            {
                bool tightTurn;
                
                // Figure out the direction, normalize and clamp it. 
                Vector3 targetDir = locomotionTarget - transform.position;
                float angle = Vector3.SignedAngle(targetDir, transform.forward, transform.up);
                turningTarget = -Mathf.Clamp(angle, -30f, 30f) / 30f;
                if (Mathf.Abs(turningTarget) < 0.2f) turningTarget = 0;
                // make this a tight turn if we're more than 90 degrees away 
                float distance = Vector3.Distance(locomotionTarget, transform.position);
                tightTurn = (Mathf.Abs(angle) > 90);
                if (tightTurn) turningTarget = Mathf.Sign(turningTarget); 
                // Stop when we're close to the target
                if (distance > 0.2f)
                {
                    // Dampen the turn to prevent jitter
                    turning = turning * 0.95f + turningTarget * 0.05f;
                    Animator.SetFloat("Heading", turning);

                    // Move and rortate this gameObject as needed
                    transform.Rotate(transform.up,
                        turning * Time.deltaTime * 100 * (tightTurn ? 5f : 1));
                    walkingSpeed = walkingSpeed * 0.95f + WalkingSpeed * 0.05f;
                    transform.position += transform.forward * walkingSpeed * Time.deltaTime * scaler;
                    
                }
                else if (distance > 0.05f)
                {
                    transform.Rotate(transform.up,turning*Time.deltaTime * 100 * 5f);
                    // We're close enough to ignore turning and just move directly to target
                    transform.position += targetDir.normalized * Time.deltaTime /2 ;
                }   
                else
                {
                    // We've reach the walking destination, turn towards the user
                    TurnToUser();
                    walkingSpeed = 0;
                    turning = 0;
                }
            }
        }


        /// <summary>
        /// Walk to a given target
        /// </summary>
        /// <param name="walkingTarget">Walking target</param>
        public bool WalkToTarget(Vector3 walkingTarget)
        {
            if (LocomotionState!=LocomotionStates.None) return false;
            
            Debug.Log(name + " State changing from " + LocomotionState + " to WalkToTarget" );
            LocomotionState = LocomotionStates.WalkToTarget;
            locomotionTarget = walkingTarget;
            Animator.CrossFade("Walk", CrossfadeDuration);
            return true;
        }

        /// <summary>
        /// Jump to a given target
        /// </summary>
        /// <param name="target">Jumping target</param>
        public bool JumpToTarget(Vector3 target)
        {
            if (LocomotionState!=LocomotionStates.None) return false;
            
            Debug.Log(name + " State changing from " + LocomotionState + " to JumpToTarget" );
            LocomotionState = LocomotionStates.JumpToTarget;
            locomotionTarget = target;
            float delay = 0.5f;
            inTheAirY = 0;
            // Trigger the animation state

            // Handle the movement
            var s1 = DOTween.Sequence();
            // Barry turns towards the jumping target
            if (Name == VoxName.Barry)
            {
                TurnToTarget(target);
                s1.AppendInterval(TurnDuration);
            }

            if (Name==VoxName.Screamy)
                Animator.CrossFade("Jump", CrossfadeDuration);
            else
                Animator.CrossFade("Flip", CrossfadeDuration);
            
            s1.Append(transform.DOMove(target, JumpDuration).SetEase(Ease.Linear));
            // Screamy does a spin while jumping
            if (Name==VoxName.Screamy)
            {
                float r = transform.rotation.eulerAngles.y + 360*2f;
                s1.Join(transform.DORotate(new Vector3(0,r,0),JumpDuration,RotateMode.FastBeyond360).SetEase(Ease.OutQuad));
            }
            s1.AppendCallback(() => Animator.CrossFade("Land",CrossfadeDuration/2f));
            s1.AppendInterval(delay);
            
            s1.AppendCallback(() => TurnToUser());
            s1.AppendInterval(TurnDuration);
            s1.AppendCallback(() => Idle());
                
            // Handle the jumping in the air part
            var s2 = DOTween.Sequence();
            if (Name == VoxName.Barry)
                s2.AppendInterval(TurnDuration);
            //s2.Append(CharacterModel.transform.DOLocalMoveY(JumpHeight * scaler, JumpDuration / 2f).SetEase(Ease.OutQuad));
            //s2.Append(CharacterModel.transform.DOLocalMoveY(0, JumpDuration / 2f).SetEase(Ease.InQuad));
            s2.Append(DOTween.To(() => inTheAirY, x => inTheAirY = x, JumpHeight* scaler, JumpDuration/2f).SetEase(Ease.OutQuad));
            s2.Append(DOTween.To(() => inTheAirY, x => inTheAirY = x, -0.01f, JumpDuration/2f).SetEase(Ease.InQuad));
            return true;
        }
        
        /// <summary>
        /// Trigger the entrance animation 
        /// </summary>
        public void MakeAnEntrance( bool fall)
        {
            startingPosition = CharacterRootBone.transform.localPosition;
            scaler = transform.lossyScale.x / 0.2f;
            if (fall)
            {
                Debug.Log(name + " State changing from " + LocomotionState + " to Falling" );
                LocomotionState = LocomotionStates.Falling;
                inTheAirY = FallFromHeight;

                float v = ShadowPlaneRenderer.material.GetFloat(ShadowStrengthProp);
                ShadowPlaneRenderer.material.SetFloat(ShadowStrengthProp, 0);
                ShadowPlaneRenderer.material.DOFloat(v,ShadowStrengthProp, 0.2f);

            }
            else
            {
                inTheAirY = 0;
                Idle();
            }
            
            // Pick a random Finish
            finishEmote = Finishs.Random();
            
            RuntimeAnimatorController ac = Animator.runtimeAnimatorController;   
            for(int i = 0; i<ac.animationClips.Length; i++)                
            {
                if(ac.animationClips[i].name == finishEmote.StateName)        
                {
                    FinishEmoteDuration = ac.animationClips[i].length;
                }
            }

            if (FinishEmoteDuration == 0) FinishEmoteDuration = finishEmote.Length;
            
            Debug.Log(Name + " Finish " + finishEmote.StateName + " animation length: "+ FinishEmoteDuration);
            
            inited = true;
        }

        /// <summary>
        /// Turn towards the user (AR Camera)
        /// </summary>
        public void TurnToUser()
        {
            TurnToTarget(ARCamera.position);
        }


        /// <summary>
        /// Turn towards a target
        /// </summary>
        public void TurnToTarget(Vector3 target)
        {
            Debug.Log(name + " turn to target " + target );
            Vector3 FlooredTargetPosition = new Vector3(target.x, transform.position.y, target.z);
            //float angle = Vector3.SignedAngle(FlooredTargetPosition, transform.forward, transform.up);
            if (Name == VoxName.Barry)
               Animator.CrossFade("Walk", CrossfadeDuration);
            if (turningTween != null) 
                turningTween.Kill();
            turningTween = transform.DOLookAt(FlooredTargetPosition, 0.5f).SetEase(Ease.InOutQuad)
                .OnComplete(() => Animator.CrossFade("Idle", CrossfadeDuration));
        }
        
        /// <summary>
        /// Move to idle state
        /// </summary>
        public void Idle()
        {
            Debug.Log(name + " State changing from " + LocomotionState + " to None" );
            Animator.CrossFade("Idle", CrossfadeDuration);
            LocomotionState = LocomotionStates.None;
        }

        
        /// <summary>
        /// Play a random emote 
        /// </summary>
        public void RandomEmote()
        {
            if (LocomotionState != LocomotionStates.None) return;
            LocomotionState = LocomotionStates.Emoting;
            Emotes.Emote e = Emotes.Random();
            Debug.Log(name + " Emoting: "+e.StateName );
            StartCoroutine(triggerEmote(e));
        }
        
        // Handle an emote being played
        IEnumerator triggerEmote(Emotes.Emote e)
        {
            Debug.Log("Crossfade into state " + e.StateName);
            Animator.CrossFade(e.StateName,CrossfadeDuration/2f);
            yield return new WaitForSeconds(0.0f);
            if (e.Prefab != null)
                Instantiate(e.Prefab, transform);
            if (e.Event!=null)
                e.Event.Invoke();
            AnimatorStateInfo currStateInfo = Animator.GetCurrentAnimatorStateInfo(0);
            Wait (currStateInfo.length, () => 
            {
                if (LocomotionState!=LocomotionStates.Finishing)
                    LocomotionState=LocomotionStates.None;
            });
        }
        
        /// <summary>
        /// Play a random Finish animation
        /// </summary>
        public void RandomFinish()
        {
            LocomotionState = LocomotionStates.Finishing;
            Debug.Log(name + " Finishing: "+finishEmote.StateName );
            StartCoroutine(triggerEmote(finishEmote));
        }
        
        /// <summary>
        /// Play the listening animation
        /// </summary>
        public void Listen()
        {
            StartCoroutine(triggerEmote(ListeningEmote));
        }

        /// <summary>
        /// Do a mirror emote
        /// </summary>
        public void Mirror()
        {
            Debug.Log(name + " Mirroring" );
            LocomotionState = LocomotionStates.Emoting;
            Animator.Play(MirrorEmote.StateName);
            Wait (MirrorEmote.Length, () => 
            {
                if (LocomotionState!=LocomotionStates.Finishing)
                    LocomotionState=LocomotionStates.None;
            });
        }
    }  
}

  


