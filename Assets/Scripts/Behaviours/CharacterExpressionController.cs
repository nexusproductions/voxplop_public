
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;


namespace NexusStudios.VoxPlop
{
    /// <summary>
    /// Controls the mouth and facial bones of a VOX
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class CharacterExpressionController : MonoBehaviour
    {
        [Header("Bones")] 
        public List<Transform> EyeBones;
        public List<Transform> UpperLipBones;
        public List<Transform> LowerLipBones;
        public List<Transform> MouthCornerBones;
        public List<Transform> CheeckBones;

        [Header("Movement range")] 
        public float UpperLipMovementDelta = 0.01f;
        public float LowerLipMovementDelta = 0.04f;
        public float SmileMovementDelta = 0.04f;
        [Range(0, 1)] public float Aaaaa = 0;
        [Range(0, 1)] public float Smile = 1;

        [Header("Events")] 
        public UnityEvent DoneSpeaking;
        
        private Vector3 shadowStartScale;
        private float eyesStartingY;
        private float upperLipStartingY;
        private float lowerLipStartingY;
        private float mouthCornerStartingY;
        private float cheeckStartingY;
        private float originalEyeScale = 1;
        private float blinkTimer;
        public AudioSource audioSourece;
        public AudioSampler audioSampler;
        private bool speaking = false;
        public float Pitch = 1;
        public bool Blinking = true;
        
        void Start()
        {
            upperLipStartingY = UpperLipBones[0].localPosition.y;
            lowerLipStartingY = LowerLipBones[0].localPosition.y;
            mouthCornerStartingY = MouthCornerBones[0].localPosition.y;
            cheeckStartingY = CheeckBones[0].localPosition.y;
            originalEyeScale = EyeBones[0].localScale.x;
            eyesStartingY = EyeBones[0].localPosition.y;
        }

        // Moving mouth bones should happen in LateUpdate, so it overwrites the animation keyframes
        private void LateUpdate()
        {
            // Handle audio sampling 
            if (speaking)
            {
                Aaaaa = audioSampler.SmoothNormalizedVolume;
                Smile = (1 - audioSampler.SmoothNormalizedVolume / 2f);
                audioSourece.pitch = Pitch;
                
                // Move mouth parts
                foreach (var b in UpperLipBones)
                {
                    b.localPosition = new Vector3(
                        b.localPosition.x,
                        upperLipStartingY + Aaaaa * UpperLipMovementDelta,
                        b.localPosition.z);
                }

                foreach (var b in LowerLipBones)
                {
                    b.localPosition = new Vector3(
                        b.localPosition.x,
                        lowerLipStartingY - Aaaaa * LowerLipMovementDelta,
                        b.localPosition.z);
                }

                foreach (var b in MouthCornerBones)
                {
                    b.localPosition = new Vector3(
                        b.localPosition.x,
                        mouthCornerStartingY - (1f - Smile) * SmileMovementDelta,
                        b.localPosition.z);
                }

                foreach (var b in CheeckBones)
                {
                    b.localPosition = new Vector3(
                        b.localPosition.x,
                        cheeckStartingY - (1f - Smile) * SmileMovementDelta,
                        b.localPosition.z);
                }
                
                // Check if speaking is done
                if (!audioSourece.isPlaying)
                {
                    speaking = false;
                    DoneSpeaking.Invoke();
                }
            }



            // Handle blinking
            blinkTimer -= Time.deltaTime;
            if (blinkTimer < 0)
            {
                blinkTimer = UnityEngine.Random.Range(1f, 3f);
                foreach (var b in EyeBones)
                {
                    DOTween.Sequence()
                        .Append(b.DOScale(new Vector3(originalEyeScale, 0, originalEyeScale), 0.1f)
                            .SetEase(Ease.InOutQuad))
                        .Append(b.DOScale(new Vector3(originalEyeScale, originalEyeScale, originalEyeScale), 0.1f)
                            .SetEase(Ease.InOutQuad));
                }
            }
        }

        /// <summary>
        /// Play an audioclip while moving the mouth
        /// </summary>
        /// <param name="clip">The clip to play</param>
        public void Speak(AudioClip clip)
        {
            audioSourece.clip = clip;
            audioSourece.Play();
            speaking = true;
        }

        /// <summary>
        /// Stop playing 
        /// </summary>
        public void StopSpeaking()
        {
            speaking = false;
            audioSourece.Stop();
        }

    }
}
