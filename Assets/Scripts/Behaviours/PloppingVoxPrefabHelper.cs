using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

namespace NexusStudios.VoxPlop
{
    /// <summary>
    /// A helper class that holds references to stuff we'll need in the scene
    /// </summary>
    public class PloppingVoxPrefabHelper : MonoBehaviour
    {
        public Camera ARCamera;
        public ARRaycastManager ARRaycastManager;
        public ARSessionOrigin ARSessionOrigin;
        public MusicPlayer musicPlayer;
    }
}