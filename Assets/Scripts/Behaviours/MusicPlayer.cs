using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace NexusStudios.VoxPlop
{

    /// <summary>
    /// Mananges playback of music in plopping vox state
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class MusicPlayer : MonoBehaviour
    {
        /// <summary>
        /// The attached AudioSource componenent
        /// </summary>
        private AudioSource source;

        void Start()
        {
            // find the source.
            source = GetComponent<AudioSource>();
        }

        /// <summary>
        /// Sets and begins playback of specified audio clip.
        /// </summary>
        /// <param name="_clip">The clip to play.</param>
        public void SetClip(AudioClip _clip)
        {
            if (!source)
            {
                Debug.Log("Lazy getting audio source.");
                source = GetComponent<AudioSource>();
            }

            source.clip = _clip;
            source.Play();
        }

        /// <summary>
        /// Restarts playback to beginning of file.
        /// </summary>
        public void Restart()
        {
            source.time = 0;
        }
    }

}