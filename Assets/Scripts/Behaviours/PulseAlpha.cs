using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace NexusStudios.VoxPlop
{
    /// <summary>
    /// Make an image pulse in the alpha channel 
    /// </summary>
    public class PulseAlpha : MonoBehaviour
    {
        public Image Image;
        public float MinAlpha = 0.4f;
        public float Duration = 0.9f;

        void Start()
        {
            Image.DOFade(MinAlpha, Duration).SetLoops(-1, LoopType.Yoyo);
        }


    }
}