using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using UnityEngine;

using SimpleFileBrowser;

using AppStructure;
using AppStructure.States;
using AppStructure.Data;
using UnityEngine.Networking;

namespace NexusStudios.VoxPlop
{

    /// <summary>
    /// Manages library of media content in persistentDataStorage as well as other file operations, such as: importing, loading, extraction of sidecar data, etc.
    /// </summary>
    public static class LibraryManager
    {
        public static GlobalSessionData globalData;
        
        /// <summary>
        /// Finds movie files saved in persistentStorage and creates LibraryItems from them. Using <see cref="MakeItemFromFilename"/>, it confirms their validity, type and generates thumbnails if necessary. 
        /// </summary>
        /// <returns>Array of valid media items on disk</returns>
        public static LibraryItem[] GetItems()
        {
            // Get all filenames in storage.
            var files = Directory.EnumerateFiles(Application.persistentDataPath);
            CheckPrerecVideos();
            
            // get all mp4s as LibraryItems.
            var mp4s = 
                from file in files 
                where file.EndsWith(".mp4") && file.Contains("spatialVideo")
                orderby file
                select MakeItemFromFilename(file);

            mp4s = 
                from item in mp4s
                where item.type != LibraryItemType.None
                select item;
            
            // reverse the list (filenames are timestampped)- we want newest first
            mp4s = mp4s.Reverse();
            
            // return array.
            return mp4s.ToArray<LibraryItem>();
        }


        /// <summary>
        /// Deletes specified item from library and disk.
        /// </summary>
        /// <param name="fname">The name of the file to delete.</param>
        public static void DeleteItem(LibraryItem item)
        {
            DeleteItem(item.filename);
            
            // don't forget to refresh the lib after this!
        }

        public static void DeleteItem(string path)
        {
            var strippedName = Path.GetFileNameWithoutExtension(path);
            Debug.Log("deleting..." +strippedName);

            try
            {
                File.Delete(Application.persistentDataPath+"/"+strippedName+".mp4");
                File.Delete(Application.persistentDataPath+"/"+strippedName+".png");
            }
            catch
            {
                Debug.Log("delete fail.");
            }
            // don't forget to refresh the lib after this!

        }

        /// <summary>
        /// Loads a file from disk, verifies: 1) that it is a video file and 2) that it has spatial data. If it does, it attempts to extract its sidecar. Depending on success and returned data, it sets its <see cref="LibraryItemType"/>. 
        /// </summary>
        /// <param name="fname">The name of the media file</param>
        /// <returns>A complete LibraryItem</returns>
        private static LibraryItem MakeItemFromFilename(string fname)
        {
            Debug.Log("Making item... " + fname);

            // make a new item
            var item = new LibraryItem();

            // set its filename
            item.filename = Path.GetFileName(fname);

            // if it's a spatial video
            if (IsVideoSpatial(fname))
            {
                #if UNITY_EDITOR
                VoxSidecar sidecar = null;

                #elif UNITY_ANDROID
                // attempt to extract sidecar
                var sidecar = ExtractSidecar(item.filename, false);
                #endif
                
                // if it has a sidecar
                if (sidecar != null)
                {
                    // if it has vox data embedded, tag it plopped
                    if (sidecar.voxes.Count >= 2)
                    {
                        Debug.Log("two or more voxes in sidecar! video is plopped_both.");
                        item.type = LibraryItemType.PloppedVideo_Multiple;
                    }
                    else if (sidecar.voxes.Count == 1)
                    {
                        Debug.Log("sidecar's got a single vox.");
                        item.type = LibraryItemType.PloppedVideo_Solo;
                        item.embeddedVox = sidecar.voxes[0].voxData;

                    }
                    // if it doesn't, consider it raw
                    else
                    {
                        Debug.Log("no voxes in sidecar. must be raw.");
                        item.type = LibraryItemType.RawVideo;
                    }

                }
                // if it doesn't have a sidecar at all, it's raw.
                else
                {
                    Debug.Log("no sidecar. must be raw video");
                    item.type = LibraryItemType.RawVideo;

                }
                item.thumbnail = GetThumbnail(item.filename);

            }
            // if it isn't spatial, notify user and remove from library.
            else
            {
                Debug.LogWarning("Video isn't spatial. Removing from library.");
                // TODO: make notification
                File.Delete(fname);
            }
            
            // TODO: ensure Type.None is the default.
            return item;
        }

        /// <summary>
        /// Loads a thumbnail of specified file as a texture. Looks for cached version first (either created at recording or at previous load), and creates one if it can't find one.
        /// </summary>
        /// <param name="fname">The name of the media file for which to load a thumbnail.</param>
        /// <returns>Thumbnail as texture</returns>
        private static Texture2D GetThumbnail(string fname)
        {
            // make png equivalent of mp4 filename
            var thumbnailName = fname.Split('.')[0] + ".png";
            var thumbnailPath = Application.persistentDataPath + "/" + thumbnailName;
            var uri = new System.Uri(thumbnailPath);

            Debug.Log($"getting thumbnail for {thumbnailName}, path: {thumbnailPath}, uri: {uri}");

            // prep texture
            Texture2D tex;

            // if the png already exists, great!
            if (File.Exists(thumbnailPath))
            {
               
                // read file to texture
                var bytes = File.ReadAllBytes(thumbnailPath);
                tex = new Texture2D(256, 256, TextureFormat.RGBA32, false);
                tex.LoadImage(bytes);

            } 
            // if it doesn't, we need to make one.
            else 
            {
                Debug.Log("no thumb found. creating...");
                
                // get complete path of original media
                var filePath = Application.persistentDataPath + "/" + fname;
                
                // TODO: center cropping
                // convert a frame (1" in) to a png.
                FFmpegWrapper.Execute($"-i {filePath} -ss 00:00:00 -to 00:00:00.1 -r 1 -f image2 {thumbnailPath}");
                
                // then load it in
                try
                {
                    var bytes = File.ReadAllBytes(thumbnailPath);
                    tex = new Texture2D(256, 256, TextureFormat.RGBA32, false);
                    tex.LoadImage(bytes);
                }
                catch
                {
                    Debug.LogWarning("thumbnail creation fail!!!");
                    tex = new Texture2D(256, 256, TextureFormat.RGBA32, false);

                }

            }
            return tex;
        }

        /// <summary>
        /// Imports a file from outside app into library, copying the source file to persistentDataStorage and attempting to make a <see cref="LibraryItem"/> from it.
        /// </summary>
        /// <param name="path">SAF ("content://") URI of file to import</param>
        /// <returns>Imported media as LibraryItem</returns>
        public static LibraryItem ImportItem(string path)
        {
            Debug.Log("Importing: " + path);
            
            // Confirm file exists at target path
            if (IsPathValid(path))
            {
                // create destination filename
                var timestamp = DateTime.Now.ToString("yyyyMMdd-HHmmss");
                var filename = $"spatialVideo_{timestamp}-imported.mp4";
                var newPath = Application.persistentDataPath + "/" + filename;

                // copy file to destination
                SAFHelper.CopyFile(path, newPath);
                Debug.Log("Copied " + filename);
                
                // return item
                return MakeItemFromFilename(newPath);

            }
            // if file cannot be reached, stop and notify.
            else
            {
                Debug.LogWarning("import error.");
                // TODO: make file not valid notification
                return new LibraryItem();
            }
        }

        /// <summary>
        /// Confirms if file at path exists.
        /// </summary>
        /// <param name="path">File path to verify</param>
        /// <returns>True on success</returns>
        static bool IsPathValid(string path)
        {
            //TODO: make checker
            return true;
        }

        /// <summary>
        /// Checks if mp4 contains spatial data.
        /// </summary>
        /// <param name="path">Path of mp4 to verify</param>
        /// <returns>True if video is spatial</returns>
        static bool IsVideoSpatial(string path)
        {
            // TODO: check to see if file contains spatial data
            return true;
        }

        /// <summary>
        /// Sets <see cref="ARSceneData.currentFilename"/> and loads appropriate scene (either RecordingRaw of PloppingVox) for playback. 
        /// </summary>
        /// <param name="item"></param>
        public static void OpenItem(LibraryItem item)
        {
            Debug.Log("opening item");
            
            var arSceneData = globalData.arSceneData;

            // based on item type
            if (item.type == LibraryItemType.RawVideo)
            {
                // set current filename
                arSceneData.currentFilename = item.filename;

            }
            else if (item.type == LibraryItemType.PloppedVideo_Solo ||
                     item.type == LibraryItemType.PloppedVideo_Multiple)
            {
                // set current filename
                arSceneData.currentFilename = item.filename;

                // extract its sidecar data
                try
                {
                    arSceneData.sidecar = ExtractSidecar(item.filename, true);
                }
                catch
                {
                    Debug.LogWarning("Error creating sidecar.");
                }
            }
            

        }

        /// <summary>
        /// Attempts to extract <see cref="VoxSidecar"/> from given mp4.
        /// </summary>
        /// <param name="filename">File name of mp4 from which to extract</param>
        /// <returns>Extracted sidecar or null on failure</returns>
        public static VoxSidecar ExtractSidecar(string filename, bool deserializeAudio)
        {
            Debug.Log("Library Mgr extracting sidecar... ");

            // set full path for media file.
            var path = Application.persistentDataPath + "/" + filename;
            var textFilePath = Application.persistentDataPath + $"/{filename}.json";

            if (File.Exists(textFilePath))
            {
                // read text file into string
                Debug.Log("found existing sidecar!");
                var sr = new StreamReader(textFilePath);
                var fileContents = sr.ReadToEnd();
                sr.Close();

                // convert json to sidecar and return
                return VoxSidecar.FromJSON(fileContents, deserializeAudio);
            }
            else
            {
                // use ffmpeg to demux fifth track (our externalDataTrack) into a text file.
                int rc = FFmpegWrapper.Execute($"-i {path} -map 0:5 -c copy -copy_unknown -f data {textFilePath}");

                // if response code is success (0)
                if (rc == 0)
                {
                    // read text file into string
                    Debug.Log("extract success!");
                    var sr = new StreamReader(textFilePath);
                    var fileContents = sr.ReadToEnd();
                    sr.Close();

                    // delete text file
                    // File.Delete(textFilePath);
                    Debug.Log("Extracted: " + fileContents);
                
                    // convert json to sidecar and return
                    return VoxSidecar.FromJSON(fileContents, deserializeAudio);
                }
                // if response code is error, return null.
                else
                {
                    Debug.LogWarning("extraction fail.");
                    return null;
                }
            }
        }
        
        /// <summary>
        /// Deletes temporary files from persistent data.
        /// </summary>
        public static void RemoveTempFiles()
        {
            Debug.Log("Deleting Temp Files");
            
            // Get all filenames in storage.
            var files = Directory.EnumerateFiles(Application.persistentDataPath);
            
            // get all mp4s as LibraryItems.
            var tempFiles = 
                from file in files 
                where file.Contains("temp")
                select file;

            foreach (var file in tempFiles)
            {
                File.Delete(file);
                Debug.Log("Deleting " + file);
            }
        }
        
        /// <summary>
        /// Checks to see if the library contains Nexus Studios' prerecorded videos and adds them to the user's library if it doesn't.
        /// </summary>
        private static void CheckPrerecVideos()
        {
            for (int i = 0 ; i<3; i++)
            {
                var filename = $"spatialVideo_20210507-nexus{i}.mp4";
                var url = Path.Combine(Application.streamingAssetsPath,filename);
                Debug.Log("StreamingAssets url: " + url);
                
                // if the persistent storage doesn't have the files, add them
                if ( !Directory.EnumerateFiles(Application.persistentDataPath).Contains(filename))
                {
                    Debug.Log("Static file doesn't exist in storage. getting.");
                    
                    var www = UnityWebRequest.Get(url);
                    
                    www.SendWebRequest();
            
                    Debug.Log("getting from url: " + url);

                    // force it to be synchronous, since it's tiny and local.
                    while (!www.isDone)
                    {
                        //Debug.Log("waiting for read...");
                    }
            
                    // if it fails, log it,
                    if (www.result == UnityWebRequest.Result.ConnectionError)
                    {
                        Debug.Log(www.error);
                    }
                    // otherwise, copy it to persistent storage
                    else
                    {
                        Debug.Log("get result: " + www.result);
                        File.WriteAllBytes(Application.persistentDataPath + "/" + filename, www.downloadHandler.data);

                        Debug.Log("loaded clip.");
                    }

                }
            }
        }
    }

    /// <summary>
    /// A class to help copy files from Android's SAF system to persistentData.
    /// Courtesy: yasirkula https://forum.unity.com/threads/simple-file-browser-open-source.441908/page-8
    /// </summary>
    public static class SAFHelper
    {
        public static void CopyFile(string sourcePath, string destinationPath)
        {
            #if !UNITY_EDITOR && UNITY_ANDROID
            var AJC = new AndroidJavaClass( "com.yasirkula.unity.FileBrowser" );

			AndroidJavaObject unityClass = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" );
			var Context = unityClass.GetStatic<AndroidJavaObject>( "currentActivity" );

			AJC.CallStatic( "CopyFile", Context, sourcePath, destinationPath, false );
            #else
            File.Copy(sourcePath, destinationPath, true);
            #endif
        }
    }
}
