using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace NexusStudios.VoxPlop
{
    /// <summary>
    /// Manages interface with native sharing library.
    /// </summary>
    public class ShareManager
    {
        /// <summary>
        /// Maintains path of current shared file. Used for deletion in callback.
        /// </summary>
        private static string filePath;

        /// <summary>
        /// True if the file being shared should be deleted upon completion of sharing, either on sucess or failure.
        /// </summary>
        private static bool shouldDelete;

        private static string rawBody = "I sent you a Spatial Video! Click the attachment below and it’ll open on your VoxPlop app.\n\r\n\r" +
                                        "Don’t have VoxPlop!? Let’s fix that! Head on over to voxplop.com or the Google Play Store to download VoxPlop! onto your Android device.";

        private static string embeddedBody = "I plopped a Vox! Click the attachment below and it’ll open on your VoxPlop app.\n\r\n\r" +
                                             "Don’t have VoxPlop!? Let’s fix that! Head on over to voxplop.com or the Google Play Store to download VoxPlop! onto your Android device.";

        private static string flatBody = "I plopped a Vox into a Spatial Video, just for you! Check it out!\n\r\n\r" +
                                         "Don’t have VoxPlop!? Let’s fix that! Head on over to voxplop.com or the Google Play Store to download VoxPlop! onto your Android device.";

        //FIXME: we should align the inputs on these to be either path or filename not both
        /// <summary>
        /// Prepares and sends native sharing payload for videos with spatial content and embedded voxes.
        /// </summary>
        /// <param name="filename">name of file to share</param>
        public static void ShareSpatialVideo(string filename)
        {
            Debug.Log("Sharing Spatial...");
            var sourcePath = Application.persistentDataPath + "/" + filename;
            
            var strippedName = Path.GetFileNameWithoutExtension(ShareManager.filePath);

            var timestamp = DateTime.Now.ToString("yyyyMMdd-HHmmss"); 
            var exportName = $"spatialVideo_{timestamp}-exported";

            filePath = $"{Application.persistentDataPath}/{exportName}.plop";
            File.Copy(sourcePath, filePath, true);


            ShareVideo(filePath, "I sent you a Spatial Video with VoxPlop!", embeddedBody);
        }
        
        //FIXME: we should align the inputs on these to be either path or filename not both
        /// <summary>
        /// Prepares and sends native sharing payload for raw videos with spatial content.
        /// </summary>
        /// <param name="filename">name of file to share</param>
        public static void ShareRawVideo(string filename)
        {
            Debug.Log("Sharing Raw...");
            var sourcePath = Application.persistentDataPath + "/" + filename;
            
            var strippedName = Path.GetFileNameWithoutExtension(ShareManager.filePath);

            var timestamp = DateTime.Now.ToString("yyyyMMdd-HHmmss"); 
            var exportName = $"spatialVideo_{timestamp}-exported";

            filePath = $"{Application.persistentDataPath}/{exportName}.plop";
            File.Copy(sourcePath, filePath, true);


            ShareVideo(filePath, "I sent you a Spatial Video with VoxPlop!", rawBody);
        }

        /// <summary>
        /// Prepares and sends native sharing payload for rendered (flattened) videos.
        /// </summary>
        /// <param name="path">Path of file to share</param>
        public static void ShareFlatVideo(string path)
        {
            Debug.Log("Sharing Flat..");
            filePath = path;
            shouldDelete = true;

            var newPath = Application.persistentDataPath+"/voxplop_output.mp4";
            FFmpegWrapper.Execute($"-i {path} -ss 00:00:00.03 -t 00:00:14.97 -c:v copy -c:a copy {newPath}" );
            File.Delete(filePath);
            
            filePath = newPath;
            ShareVideo(newPath, "I sent you a video with VoxPlop!", flatBody);
        }

        /// <summary>
        /// Convenience function for native sharing library.
        /// </summary>
        /// <param name="path">Path to file to share</param>
        /// <param name="subject">Subject of sharing payload</param>
        /// <param name="body">Body of sharing payload</param>
        private static void ShareVideo(string path, string subject, string body)
        {
            new NativeShare()
                .AddFile(path)
                .SetSubject(subject)
                .SetText(body)
                .SetCallback(OnShareComplete)
                .Share();
        }

        /// <summary>
        /// Callback for NativeShare. Used to delete videos after sharing.
        /// </summary>
        /// <param name="result">Response from Native Share library</param>
        /// <param name="target">Destination</param>
        private static void OnShareComplete(NativeShare.ShareResult result, string target)
        {
            if (result == NativeShare.ShareResult.Shared)
            {
                Debug.Log("shared!");
            }
            else
            {
                Debug.Log("not shared!");
            }

            File.Delete(filePath);
        }
    }
}