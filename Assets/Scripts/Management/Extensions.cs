using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public static class Extensions 
{
    
    /// <summary>
    /// Return a random item from a list.
    /// </summary>
    /// <param name="items"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns>Random item from array</returns>
    public static T RandomElement<T>(this T[] items)
    {
        return items[Random.Range(0, items.Length)];
    }

    /// <summary>
    /// Return a random item from a list.
    /// </summary>
    /// <param name="items"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns>Random item from array</returns>
    public static T RandomElement<T>(this List<T> items)
    {
        return items[Random.Range(0, items.Count)];
    }
    
    /// <summary>
    /// Return a shuffled copy of an array
    /// </summary>
    /// <param name="_list"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns>A randomly shuffled copy of the array</returns>
    public static List<T> Shuffle<T>(this List<T> _list)
    {
        for (int i = 0; i < _list.Count; i++)
        {
            T temp = _list[i];
            int randomIndex = Random.Range(i, _list.Count);
            _list[i] = _list[randomIndex];
            _list[randomIndex] = temp;
        }

        return _list;
    }
    
    /// <summary>
    /// Return a random value from an enum
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns>Random value</returns>
    public static T RandomEnum<T>()
    {  
        Type type = typeof(T);
        Array values = Enum.GetValues(type);
        object value= values.GetValue(Random.Range(0,values.Length));
        return (T)Convert.ChangeType(value, type);
    }
    
}
