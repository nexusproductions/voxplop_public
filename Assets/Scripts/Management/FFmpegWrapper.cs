using UnityEngine;

namespace NexusStudios.VoxPlop
{
    
    /// <summary>
    /// Handles interface with ffmpeg-native library.
    /// Courtesy of: https://sourceexample.com/en/671cf50fddfec13eb5e1/
    /// </summary>
    public class FFmpegWrapper
    {
        /// <summary>
        /// Executes raw ffmpeg commands.
        /// </summary>
        /// <param name="command">The command string to execute</param>
        /// <returns></returns>
        public static int Execute(string command)
        {
            #if UNITY_EDITOR
            return 0;

#elif UNITY_ANDROID
            using (AndroidJavaClass configClass = new AndroidJavaClass("com.arthenica.mobileffmpeg.Config"))
            {
                AndroidJavaObject paramVal =
 new AndroidJavaClass("com.arthenica.mobileffmpeg.Signal").GetStatic<AndroidJavaObject>("SIGXCPU");
                configClass.CallStatic("ignoreSignal", new object[] { paramVal });

                using (AndroidJavaClass ffmpeg = new AndroidJavaClass("com.arthenica.mobileffmpeg.FFmpeg"))
                {
                    int code = ffmpeg.CallStatic<int>("execute", new object[] { command });
                    return code;
                }
            }
#elif UNITY_IOS
            return MobileFFmpegIOS.Execute(command);
#else
            return 0;
#endif
        }

        /// <summary>
        /// Cancels current ffmpeg command
        /// </summary>
        /// <remarks>Currently unused</remarks>
        /// <returns>Success code</returns>
        private static int Cancel()
        {
            #if UNITY_ANDROID
            using (AndroidJavaClass configClass = new AndroidJavaClass("com.arthenica.mobileffmpeg.Config"))
            {
                using (AndroidJavaClass ffmpeg = new AndroidJavaClass("com.arthenica.mobileffmpeg.FFmpeg"))
                {
                    int code = ffmpeg.CallStatic<int>("cancel");
                    return code;
                }
            }
            #elif UNITY_IOS
            return MobileFFmpegIOS.Cancel();
            #else
            return 0;
            #endif
        }
    }
}