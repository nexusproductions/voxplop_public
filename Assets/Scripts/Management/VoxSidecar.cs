using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

using System.IO;
using AppStructure;
using AppStructure.Data;

namespace NexusStudios.VoxPlop{

    /// <summary>
    /// A list of all potential Vox names.
    /// </summary>
    public enum VoxName{
        Barry,
        Screamy
    }

    /// <summary>
    /// Serializable structure to maintain information about a vox placed in a scene.
    /// </summary>
    /// TODO: i'd love to make this a struct. but changing things after the fact (pitch in particular) is tricky
    [System.Serializable]
    public class VoxSidecarData
    {
        /// <summary>
        /// The vox's name.
        /// </summary>
        public VoxName voxName;
        
        /// <summary>
        /// The recorded audio the vox speaks
        /// </summary>
        public AudioClip audioClip;
        
        /// <summary>
        /// The location (in world space) where the vox was initially dropped.
        /// </summary>
        public Vector3 position;
        
        /// <summary>
        /// The related <see cref="VoxData"/> to maintain vox identity.
        /// </summary>
        public VoxData voxData;
        
        /// <summary>
        /// The time (in seconds) from the start of the recording that the vox was placed.
        /// </summary>
        /// <remarks>May not be used?</remarks>
        public float placementTime;
        
        /// <summary>
        /// The pitch (and speed) of the vox's voice. 1.0 is normal.
        /// </summary>
        public float pitch = 1f;

        /// <summary>
        /// The vox's audioClip converted into bytes (compressed as ogg). For serialization and sharing.
        /// </summary>
        [HideInInspector]
        public byte[] audioClipBytes;

    }

    /// <summary>
    /// The class for maintaining, serializing and deserializing all data added to a spatial video. Currently, just the voxes.
    /// </summary>
    [System.Serializable]
    public class VoxSidecar 
    {
        /// <summary>
        /// All voxes present in the scene.
        /// </summary>
        public List<VoxSidecarData> voxes = new List<VoxSidecarData>();

        public static GlobalSessionData globalData;

        /// <summary>
        /// Converts this object to a JSON string. To ensure serialization of media, <see cref="SerializeMedia"/> must be called before conversion to JSON.
        /// </summary>
        /// <remarks>I would prefer to serialize on output, but it's too slow to do while recording an AR Session, so can't be called when adding and external datatrack sample.</remarks>
        /// <returns>This sidecar as JSON</returns>
        public string ToJSON() {
            // Don't forget to Serialize() before this gets called!
            return JsonUtility.ToJson(this);
        }

        /// <summary>
        /// Creates a new sidecar from a raw JSON string.
        /// </summary>
        /// <param name="json">Raw JSON string</param>
        /// <returns>Completed sidecar</returns>
        public static VoxSidecar FromJSON(string json, bool deserializeAudio){
            // TODO: watch for empty strings instead of try block
            try
            {
                // deserialize json
                var sidecar = JsonUtility.FromJson<VoxSidecar>(json);

                // deserialize media
                for (int i = 0; i < sidecar.voxes.Count; i++)
                {
                    var newVox = sidecar.voxes[i];
                    newVox.audioClip = deserializeAudio? GetAudioClipFromBytes(sidecar.voxes[i].audioClipBytes): null;
                    newVox.voxData = globalData.GetVox(newVox.voxName);
                    
                    sidecar.voxes[i] = newVox;
                }

                // return sidecar
                Debug.Log("sidecar creation ok.");
                return sidecar;

            }
            catch(Exception e)
            {
                Debug.LogWarning("Sidecar creation fail: " + e);
                return null;
            }

        }

        /// <summary>
        /// Convert bytes to an AudioClip, including decompression from OGG.
        /// </summary>
        /// <param name="bytes">Bytes to convert</param>
        /// <returns>Converted AudioClip</returns>
        static AudioClip GetAudioClipFromBytes (byte[] bytes){
            
            // make a clip to hold bytes
            // FIXME: this is assuming some stuff about recording format. will it be true for all devices?
            AudioClip clip;
            
            // write bytes to external file so we can load with a webrequest
            File.WriteAllBytes($"{Application.persistentDataPath}/output.ogg", bytes);
            
            // create and send the request
            UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip($"file:///{Application.persistentDataPath}/output.ogg", AudioType.OGGVORBIS);
            www.SendWebRequest();
            
            // force it to be synchronous, since it's tiny and local.
            while (!www.isDone)
            {
                Debug.Log("waiting for read...");
            }
            
            // if it fails, log it,
            if (www.result == UnityWebRequest.Result.ConnectionError)
            {
                clip = null;
                Debug.Log(www.error);
            }
            // otherwise, set our clip to the loaded one.
            else
            {
                clip = DownloadHandlerAudioClip.GetContent(www);
                Debug.Log("loaded clip.");
            }

            // clean up
            File.Delete($"{Application.persistentDataPath}/output.ogg");
            
            // return the new clip.
            return clip;
        }


        /// <summary>
        /// Convert an AudioClip to an array of bytes, compressed into OGG.
        /// </summary>
        /// <param name="clip">The AudioClip to convert</param>
        /// <returns>Compressed Audio as bytes</returns>
         static byte[] GetBytesFromAudioClip(AudioClip clip){
            
            // save a wav so ffmpeg can get it.
            SavWav.Save(Application.persistentDataPath+"/clip.wav", clip);
            
            // convert it to ogg.
            FFmpegWrapper.Execute($"-i {Application.persistentDataPath}/clip.wav {Application.persistentDataPath}/clip.ogg");

            // read it back.
            var bytes = File.ReadAllBytes($"{Application.persistentDataPath}/clip.ogg");
            
            File.Delete(Application.persistentDataPath+"/clip.wav");
            File.Delete(Application.persistentDataPath+"/clip.ogg");
            
            // return it.
            return bytes;

        }

        /// <summary>
        /// Serializes all media (currently just audio) associated with the voxes in this sidecar. Call before calling <see cref="ToJSON"/>.
        /// </summary>
        public void SerializeMedia()
        {
            Debug.Log("serializing sidecar...");

            // for each vox in the sidecar, convert its clip to bytes.
            for (int i = 0; i < voxes.Count; i++)
            {
                var newVox = voxes[i];
                newVox.audioClipBytes = GetBytesFromAudioClip(voxes[i].audioClip);
                newVox.voxName = newVox.voxData.voxName;
                
            }
        }

        /// <summary>
        /// Clears vox list.
        /// </summary>
        public void Clear()
        {
            voxes.Clear();
            
        }
    }
}
