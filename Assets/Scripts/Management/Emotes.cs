using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace NexusStudios.VoxPlop
{

    [Serializable]
    public class Emotes
    {
        [Serializable]
        public class Emote
        {
            public string StateName;
            public GameObject Prefab;
            public UnityEvent Event;
            public float Length;
        }

        public List<Emote> List;

        [NonSerialized] private int index=0;
        [NonSerialized] private List<Emote> shuffledList;

        void shuffle()
        {
            shuffledList = List.Shuffle();
            index = 0;
        }
        
        /// <summary>
        /// Pick a random emote from the list without repeating 
        /// </summary>
        /// <returns>A random emote</returns>
        public Emote Random()
        {
            if (shuffledList == null || index == shuffledList.Count) shuffle();
            Emote e = shuffledList[index];
            index++;
            return e;
        }
        

    }
}
