# README #

Welcome to VoxPlop! VoxPlop is a Spatial Video Experiment by Nexus Studios.

### What is VoxPlop!? ###

Using ARCore's [Recording and Playback API](https://developers.google.com/ar/develop/java/recording-and-playback), VoxPlop! allows you to record videos with extra sensor information (IMU, Metadata, Depth if its available, etc), then play them back as an ARSession where you can add characters and your own pitch-shifted voice.  You can send these VoxPlops to your friends, who can open them in their app and add content of their own.

### What is this repository for? ###

This Unity Project repository is the open source version of VoxPlop!, available on the Google Play Store (https://play.google.com/store/apps/details?id=com.NexusStudios.voxplop).  Here you can explore the new recording and playback capabilities of ARCore within Unity's ARFoundation framework.

### Getting Started ###
All required packages are included with the repository. Building to your device should be as easy as opening the project in Unity, switching to Android Platform in the build settings, and selecting "Build and Run".

VoxPlop is designed to run on modern Android phones, and, as it requires ARCore, will not run on other platforms or in the editor.

When opening the project, you may want to enter "Safe Mode" and update your player settings to .NET 4x.  There may be some warnings that remain that you can safely ignore.

Complete documentation can be found in the "_site" folder of this repository (_site/manual/README.html)


### Requirements ###

Unity 2020.3.0f1 (LTS)

AR Foundation 4.2.0-pre.3

ARCore XR Plugin 4.1.5

ARCore Extensions 1.24.0: [https://github.com/google-ar/arcore-unity-extensions/releases/](https://github.com/google-ar/arcore-unity-extensions/releases/)

NativeShare 1.3.8 [https://github.com/yasirkula/UnityNativeShare.git](https://github.com/yasirkula/UnityNativeShare.git)

UnitySimpleFileBrowser 1.4.3 [https://github.com/yasirkula/UnitySimpleFileBrowser](https://github.com/yasirkula/UnitySimpleFileBrowser)

mobile-ffmpeg v4.4 LTS full-gpl [https://github.com/tanersener/mobile-ffmpeg](https://github.com/tanersener/mobile-ffmpeg)

DOTween 1.2.420 [https://github.com/Demigiant/dotween](https://github.com/Demigiant/dotween)

NexusAppStructure XXXXXXX

### AppStructure ###
This project is built upon Nexus Studios' own app framework, which handles state transitions, view presentation and persistent data management. It provides a system for our internal developers to generate a codebase that adheres to best practices and consistent design priciples. It will eventually be available for use on your own projects, but a public repo does not yet exist.

### License ###
VoxPlop! itself is distributed under the MIT license.  All other dependencies have their own open source licensing which can be found in the License folder.

### Contact ###

VoxPlop! is a creation of Nexus Studios. For more information (or if you'd like to contribute!), please contact voxplop@nexusstudios.com.

VoxPlop! Credits  
Head of Interactive: Luke Ritchie  
Executive Producer: Sarah Arruda  
Producer: Tanya Leal Soto  
Creative Director: Edward Robles  
Technical Director: Pablo Colapinto  
Technical Lead: Jesse Garrison  
Studio CG Lead:  Florian Caspar                     
Senior Developer: Shachar Weis	  
Technical Artist: Eric Urban  
UX/UI Designer: Xyn Xu  
Character Designer: Aaron Martinez  


NexusAppStructure Contributors include:  
Vegard Myklebust  
Jason Walters  
Liam Walsh  
Luke Holland  
