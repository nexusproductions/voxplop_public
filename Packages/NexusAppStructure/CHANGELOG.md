# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2021-02-08
### Added
- Please check the README.MD for general usage instructions and talk about why this project exists.
- Basic unity package manager version.
- NxAppManager.cs, NxAppStateMachine.cs, NxAppState.cs, NxAppData.cs, ViewController.cs, AbstractView.cs - Scripts for setting up a general purpose app structure.
- NxGenericStateMachine.cs, NxGenericState.cs - Scripts for setting up a general purpose state machine. 
- Open source Odin Serializer in a seperate namespace for this project, for serializing the generics (NxAppState<T> etc). (https://github.com/TeamSirenix/odin-serializer)

### Removed
- Removed all references to Odin *Inspector*, as we don't want to have that dependency for the whole project. Individual projects are free to use the full inspector functionality.