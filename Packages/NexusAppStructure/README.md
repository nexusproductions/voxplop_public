
# Nexus State Machine and AR Example Project
## Unity version: 2019.4.5

## What's this?
This is an attempt to make a common re-usable framework for apps developed in Unity at Nexus Studios. 
Specifically it provides these frameworks:

- A Generic State Machine for general purpose use, like character controllers or other places where a finite state machine is useful.

- A View Controller, for managing UI in a stateful manner. 

- An App Manager and App State Machine, which is more tightly coupled with the View Controller, and the App Data classes. 

## Why is this?
At Nexus we work with a lot of freelancers, and have historically tended to let each project create it's own framework. Although we want to keep a lot of that flexibility, we have found that it can sometimes make it quite hard when searching through a project for specific problems or specific solutions. It is also harder to jump into a new project, since none of the frameworks are the same. We want to address this by providing a bit more scaffolding for the projects.

- We want the apps we develop to always be stateful. 

- We also want the UI to be stateful, and for the UI state to be seperate from the app state.

- We want the directional flow to be somewhat contained, to prevent too much spaghettification of the code.

- We want it to be clear from the outset where code should go.



## Core concepts
* __*AppData*__ are scriptable objects (inheriting from NxAppData) that contain persistent data for your states. It is useful for storing persistent references, such as references to materials, settings etc. The data object can be shared between the View and the AppState.

* __*Systems / Controllers / Managers*__ (hereafter just called mangers) are scripts that survive the entire duration of the app running. This can include external managers, such as ARFoundation's various managers, or your own managers.  

* __*AppManager*__ is a script that implements NxAppManager. This is your own app manager.
The AppManager should hold a reference to all the other managers, and controls the state of the app. 
The states are serialized in the inspector, and you can use it to define the optional references for the states. 
It also holds a reference to a global AppData for persistent data that should be available across the whole app. 
 The InitialState selects what appstate the app should start in (usually onboarding) in a build.  
 The EditorInitialState sets what appstate should start when you click Play in Unity. This way you can test just the state you are working on in editor without going through the onboarding process each time you hit play.  

* __*AppState*__ these scripts are the state that your app is in. They have three optional data fields for convenience:  
  -__View__ - This view becomes active when entering the state.  
  -__Prefab__ - This prefab gets instanced upon entering the state, and gets destroyed upon leaving the state. (To access the runtime instance in code, use __PrefabInstance__).  
  -__PersistentData__ - This is an AppData scriptable object that inherits from NxAppData, for your persistent data references.  
  If __View__ and __PersistentData__ are both filled in, the view recieves the same persistent data object upon entering the state.  

  The app state should be responsible for a lot of your runtime code. It *should* use the managers, instantiate and place things you need from the persistent data, and pass information down stream to any regular monobehaviours you may be using on your instantiated gameobjects. The app state should control and call methods on the regular monobehaviours on things it has created.  

  Be sure to set up your app environment to be how you expect it to be in __Begin()__ and clean up your memory usage in __End()__  

* __*View Controller*__ is a manager that controls the state of the UI via the Views.
The views can be opened as a "Location", effectively changing the entire state of the UI, or as an "Overlay", effectively a window that is drawn on top of the current UI.
The View Controller also holds a reference back to the App Manager, for cases where you want to modify the global data in the view, or change state from a button.

* __*View*__ is a single screen of UI, with a corresponding script. 
A lot of the time, the View simply needs to update itself, or run some small method in the view script.
If you have linked the view and data in the app state, you may cast the persistent data in OnShowStart() and update the data afterwards in various methods.

* __*Regular MonoBehaviours*__ These should be used primarily to do things like VFX. 
They should *not* use the managers. 
They should *not* talk to the App Manager. 
They should *not* assume things about what state they are used in. 
Ideally they should be able to survive in complete isolation of anything else in the app other than the gameobjects they belong on. 
If they need to send some message for example back to the state that created them, the preferred way is to invoke an event (see event/delegates c# reference) or unity action, so that they may remain oblivious to the world around them.


### How do I use it?
Scripts/AppStructure/AppManager.cs - This should form the basis for your app manager class. It inherits from NxAppManager, and contains the NxAppStateMachine. You should extend it to contain references to all the mangers you need as described above.

![alt text](https://dl.dropboxusercontent.com/s/qih1d9lc4ccxeoz/Unity_2021-02-02_17-36-14.png "Nexus unity editor window")

To create a new View, AppState or AppData script, there is a handy tool in the Unity editor. From the top menu bar select Nexus > Create > AppData / State / View.
This will create a script from a template, as well as any other objects needed (such as the view prefab) from one simple menu. 
All you need to do is fill in the name, click create and then you can start filling in your script.
(Note: This functionality uses some editor preferences, which can be a bit undefined if you have more than one instance of Unity open at the same time).
The scripts will end up in Scripts/AppStructure/ (Data / States / Views respectively) 



## What's in the package?
### Scripts/AppStructure/
The default folder for where your implentation of the classes will go. Includes some barebones examples

### Scripts/NxStateMachine/
NxGenericStateMachine.cs & NxGenericState.cs - Classes for the generic state machine.

NxAppManager.cs, NxAppStateMachine.cs, NxAppState.cs & NxAppData.cs - Classes for the app scaffolding.

ViewController.cs, AbstractView.cs - Classes for the View Controller.

### Editor/PluginsEditorScripts/
NxAppData/, NxStateMachine/ - Editor windows for the app scaffolding and the generic state machine as well as class templates.

View/ - Editor windows for the view controller.

### Resources/
The default location where views, prefabs and scriptable object assets get created.