﻿using System.IO;
using Loju.View.Editor;
using UnityEditor;
using UnityEngine;

namespace NxAppStructureEditor
{
    public class CreateAppDataWindow : EditorWindow
    {
        private string _dataFolder = "Assets/Scripts/AppStructure/Data";
        private string _dataName;
        private string _scriptPath = "Assets/Scripts/AppStructure/Data";
        private TextAsset _template;
        
        [MenuItem("Nexus/Create/AppData")]
        [MenuItem("Assets/Create/Nexus/AppData")]
        public static void Init()
        {
            CreateAppDataWindow window = (CreateAppDataWindow)EditorWindow.GetWindow(typeof(CreateAppDataWindow));
            window.titleContent = new GUIContent("Create Data");
            window.minSize = new Vector2(400, 200);
            window.Show();
        }

        public void OnGUI()
        {
            _dataName = EditorGUILayout.TextField("Data Name", _dataName);
            EditorGUILayout.Space();
            if (_template == null)
            {
                _template = AssetDatabase.LoadAssetAtPath<TextAsset>(
                    "Packages/com.nexusstudios.appstructure/Editor/NxAppStructure/NxAppDataTemplate.txt");
            }
            _template = (TextAsset)EditorGUILayout.ObjectField("Script template",_template, typeof(TextAsset),false);
            _scriptPath = UViewEditorUtils.LayoutPathSelector(_scriptPath, "Script Path");
            string pathViewName = string.IsNullOrEmpty(_dataName) ? "{DATA_NAME}" : _dataName;
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField(CreatePath(pathViewName, _scriptPath, "cs"));
            EditorGUILayout.EndVertical();
            
            if (GUILayout.Button("Create"))
            {
                CreateData(pathViewName, _scriptPath);
                Close();
            }
        }
        
        private string CreatePath(string viewName, string folder, string extension)
        {
            viewName = viewName.Replace(" ", "")+"Data";
            return Path.Combine(folder, string.Format("{0}.{1}", viewName, extension));
        }

        private void CreateData(string dataName, string scriptsFolder)
        {
            dataName = dataName.Replace(" ", "");
            if (!Directory.Exists(scriptsFolder)) Directory.CreateDirectory(scriptsFolder);
            string scriptData = _template.text.Replace("{DATA_NAME}", dataName+"Data");
            string scriptPath = CreatePath(dataName, scriptsFolder, "cs");
            File.WriteAllText(scriptPath, scriptData);
            AssetDatabase.Refresh();
        }
    }
}
