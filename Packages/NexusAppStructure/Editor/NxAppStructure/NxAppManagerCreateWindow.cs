﻿using System.IO;
using Loju.View.Editor;
using UnityEditor;
using UnityEngine;

namespace NxAppStructureEditor
{
    public class NxAppManagerCreateWindow : EditorWindow
    {
        private string _appFolder = "Assets/Scripts/AppStructure";
        private string _appName = "AppManager";
        private string _scriptPath = "Assets/Scripts/AppStructure";
        private TextAsset _template;
        
        [MenuItem("Nexus/Create/AppManager")]
        [MenuItem("Assets/Create/Nexus/AppManager")]
        public static void Init()
        {
            NxAppManagerCreateWindow window = (NxAppManagerCreateWindow)EditorWindow.GetWindow(typeof(NxAppManagerCreateWindow));
            window.titleContent = new GUIContent("Create Data");
            window.minSize = new Vector2(400, 200);
            window.Show();
        }

        public void OnGUI()
        {
            _appName = EditorGUILayout.TextField("AppManager Name", _appName);
            EditorGUILayout.Space();
            if (_template == null)
            {
                _template = AssetDatabase.LoadAssetAtPath<TextAsset>(
                    "Packages/com.nexusstudios.appstructure/Editor/NxAppStructure/NxAppManagerTemplate.txt");
            }
            _template = (TextAsset)EditorGUILayout.ObjectField("Script template",_template, typeof(TextAsset),false);
            _scriptPath = UViewEditorUtils.LayoutPathSelector(_scriptPath, "Script Path");
            string pathViewName = string.IsNullOrEmpty(_appName) ? "{DATA_NAME}" : _appName;
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField(CreatePath(pathViewName, _scriptPath, "cs"));
            EditorGUILayout.EndVertical();
            
            if (GUILayout.Button("Create"))
            {
                CreateData(pathViewName, _scriptPath);
                Close();
            }
        }
        
        private string CreatePath(string viewName, string folder, string extension)
        {
            viewName = viewName.Replace(" ", "");
            return Path.Combine(folder, string.Format("{0}.{1}", viewName, extension));
        }

        private void CreateData(string appName, string scriptsFolder)
        {
            appName = appName.Replace(" ", "");
            if (!Directory.Exists(scriptsFolder)) Directory.CreateDirectory(scriptsFolder);
            string scriptData = _template.text.Replace("{DATA_NAME}", appName);
            string scriptPath = CreatePath(appName, scriptsFolder, "cs");
            File.WriteAllText(scriptPath, scriptData);
            AssetDatabase.Refresh();
        }
    }
}


