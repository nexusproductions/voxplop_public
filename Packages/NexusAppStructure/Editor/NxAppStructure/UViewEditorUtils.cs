﻿using UnityEngine;
using UnityEditor;
using System.IO;

/**
 * UViewEditorUtils.cs
 * Author: Luke Holland (http://lukeholland.me/)
 */

namespace Loju.View.Editor
{
    public static class UViewEditorUtils
    {
        public const string KEY_SCRIPT_PATH = "keyViewControllerScriptPath";
        public const string KEY_PREFAB_PATH = "keyViewControllerPrefabPath";

        public static string[] kTabs = new string[] { "View Controller", "Settings" };

        public const string kSettingsPath = "Packages/com.nexusstudios.appstructure/Editor/NxAppStructure/";
        public const string kSettingsAssetName = "ViewSettings.asset";
        public const string kDefaultSettingsPath = "Packages/com.nexusstudios.appstructure/Editor/NxAppStructure/ViewSettings.asset";
        public const string kResources = "/Resources/";

        [MenuItem("GameObject/Create Other/Nexus/ViewController")]
        public static void ContextCreateViewController()
        {
            GameObject viewController = new GameObject("ViewController");
            viewController.AddComponent<ViewController>();
        }

        [MenuItem("Nexus/Create/View")]
        [MenuItem("Assets/Create/Nexus/View")]
        public static void ContextCreateView()
        {
            CreateViewWindow window = ScriptableObject.CreateInstance<CreateViewWindow>();
            window.titleContent = new GUIContent("Create View");
            window.minSize = new Vector2(400, 200);
            window.ShowUtility();
        }

        [MenuItem("Tools/UView")]
        public static void MenuOpenManagerWindow()
        {
            UViewWindow window = ScriptableObject.CreateInstance<UViewWindow>();
            window.titleContent = new GUIContent("UView");
            window.minSize = new Vector2(400, 400);
            window.Show();
        }

        public static UViewSettings GetSettings()
        {
            string settingsPath = Path.Combine(UViewEditorUtils.kSettingsPath, UViewEditorUtils.kSettingsAssetName);
            UViewSettings settings = AssetDatabase.LoadAssetAtPath<UViewSettings>(settingsPath);

            if (settings == null)
            {
                UViewSettings defaults = AssetDatabase.LoadAssetAtPath<UViewSettings>(UViewEditorUtils.kDefaultSettingsPath);
                settings = GameObject.Instantiate<UViewSettings>(defaults);

                if (!Directory.Exists(UViewEditorUtils.kSettingsPath)) Directory.CreateDirectory(UViewEditorUtils.kSettingsPath);

                AssetDatabase.CreateAsset(settings, settingsPath);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }

            return settings;
        }

        public static string GetResourcePath(string assetPath)
        {
            string result = assetPath.Substring(assetPath.IndexOf(kResources) + kResources.Length);
            result = result.Substring(0, result.LastIndexOf("."));

            return result;
        }

        public static void LayoutLabelWithPrefix(string prefix, object obj)
        {
            string label = obj == null ? string.Empty : obj.ToString();
            if (string.IsNullOrEmpty(label)) label = "(none)";

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(prefix);
            EditorGUILayout.SelectableLabel(label, GUILayout.Height(EditorGUIUtility.singleLineHeight));
            EditorGUILayout.EndHorizontal();
        }

        public static string LayoutPathSelector(string currentPath, string label)
        {
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.PrefixLabel(label);
            EditorGUILayout.SelectableLabel(currentPath, GUILayout.Height(EditorGUIUtility.singleLineHeight));
            bool pressed = GUILayout.Button("...", EditorStyles.miniButton, GUILayout.Width(30));
            EditorGUILayout.EndHorizontal();

            if (pressed)
            {
                string path = EditorUtility.OpenFolderPanel(label, currentPath, string.Empty);
                if (!string.IsNullOrEmpty(path))
                {
                    currentPath = path.Replace(Application.dataPath.Substring(0, Application.dataPath.Length - 6), string.Empty);
                }
            }

            return currentPath;
        }

        public static string GetViewName(SerializedProperty property)
        {
            try
            {
                return System.Type.GetType(property.stringValue).Name;
            }
            catch
            {
                return string.Format("{0} (Not Found)", property.stringValue.Substring(0, Mathf.Max(0,property.stringValue.IndexOf(','))));
            }
        }

        public static string[] GetViewNames(SerializedProperty propertyViewAssets, bool shortNames)
        {
            int i = 0, l = propertyViewAssets.arraySize;
            string[] names = new string[l];

            for (; i < l; ++i)
            {
                SerializedProperty propertyViewAsset = propertyViewAssets.GetArrayElementAtIndex(i);
                SerializedProperty propertyViewTypeID = propertyViewAsset.FindPropertyRelative("ViewTypeID");
                if (shortNames)
                {
                    names[i] = GetViewName(propertyViewTypeID);
                }
                else
                {
                    names[i] = propertyViewTypeID.stringValue;
                }
            }

            return names;
        }

        public static void CreateViewAsset(SerializedProperty property, AbstractView view)
        {
            string assetPath = AssetDatabase.GetAssetPath(view);

            SerializedProperty propertyViewTypeID = property.FindPropertyRelative("ViewTypeID");
            SerializedProperty propertyResourcePath = property.FindPropertyRelative("ResourcePath");
            SerializedProperty propertyAssetID = property.FindPropertyRelative("AssetID");
            propertyViewTypeID.stringValue = view.GetType().AssemblyQualifiedName;
            propertyResourcePath.stringValue = UViewEditorUtils.GetResourcePath(assetPath);
            propertyAssetID.stringValue = AssetDatabase.AssetPathToGUID(assetPath);
        }

        public static void RemoveViewAssets(SerializedProperty property)
        {
            SerializedProperty propertyAssetID = property.FindPropertyRelative("AssetID");
            string assetPath = AssetDatabase.GUIDToAssetPath(propertyAssetID.stringValue);

            AbstractView view = AssetDatabase.LoadAssetAtPath<AbstractView>(assetPath);
            if (view != null)
            {
                MonoScript script = MonoScript.FromMonoBehaviour(view);
                string scriptPath = AssetDatabase.GetAssetPath(script);

                AssetDatabase.DeleteAsset(scriptPath);
            }

            AssetDatabase.DeleteAsset(assetPath);
            AssetDatabase.Refresh();
        }

        public static bool ValidateResourcePath(string path)
        {
            return path.Contains(kResources);
        }

        public static bool ValidateViewAsset(SerializedProperty property)
        {
            SerializedProperty propertyViewTypeID = property.FindPropertyRelative("ViewTypeID");
            SerializedProperty propertyAssetID = property.FindPropertyRelative("AssetID");

            if (!string.IsNullOrEmpty(propertyAssetID.stringValue) && !string.IsNullOrEmpty(propertyViewTypeID.stringValue))
            {

                string assetPath = AssetDatabase.GUIDToAssetPath(propertyAssetID.stringValue);
                System.Type baseType = typeof(AbstractView);
                System.Type viewType = System.Type.GetType(propertyViewTypeID.stringValue);

                return File.Exists(assetPath) && viewType != null && baseType.IsAssignableFrom(viewType);
            }
            else
            {
                return false;
            }
        }

        public static void Rebuild(SerializedProperty propertyViewAssets)
        {
            int i = 0, l = propertyViewAssets.arraySize;
            for (; i < l; ++i)
            {
                SerializedProperty propertyViewAsset = propertyViewAssets.GetArrayElementAtIndex(i);
                SerializedProperty propertyAssetID = propertyViewAsset.FindPropertyRelative("AssetID");

                string assetPath = AssetDatabase.GUIDToAssetPath(propertyAssetID.stringValue);
                AbstractView view = AssetDatabase.LoadAssetAtPath<AbstractView>(assetPath);
                if (view != null)
                {
                    CreateViewAsset(propertyViewAsset, view);
                }
            }
        }

    }

}