using System;
using System.Collections.Generic;
using System.Linq;
using Loju.View;
using NxAppStructure;
using UnityEngine;
using UnityEditor;

namespace NxAppStructureEditor
{
    [CustomEditor(typeof(NxAppManager), true)]
    [CanEditMultipleObjects]
    public class NxAppManagerEditor : Editor
    {
        // The target editor
        private NxAppManager _nxAppManager;
        
        // UI variables
        private List<System.Type> _possibleAppStates;
        private string[] _possibleAppStateNames;
        private int _addStateDropdownChoice;
        private int _initialStateDropdownChoice;
        private int _editorInitialStateDropdownChoice;
        private static bool _foldOutStates; // Static choice so that the foldout remains if you deselect and reselect
        
        // GUI styling
        private GUIStyle _foldOutStyle;
        private GUIStyle _stateStyle;
        private GUIStyle _boldText;
    
        // Intitialize all our styles
        void OnEnable()
        {
            _nxAppManager = (NxAppManager) target;
            _possibleAppStates = GetAllAppStateTypes();
            _possibleAppStateNames = new string[_possibleAppStates.Count];
            for (int i = 0; i < _possibleAppStates.Count; i++)
            {
                _possibleAppStateNames[i] = _possibleAppStates[i].Name;
            }

            _foldOutStyle = new GUIStyle();
            _foldOutStyle.normal.background = Texture2D.grayTexture;

            _stateStyle = new GUIStyle();
            _stateStyle.normal.background = Texture2D.whiteTexture;

            _boldText = new GUIStyle();
            _boldText.fontStyle = FontStyle.Bold;
            _boldText.normal.textColor = Color.white * 0.8f;

            if (_nxAppManager.StateMachine != null)
            {
                if (_nxAppManager.StateMachine.States == null)
                {
                    _nxAppManager.StateMachine.States = new NxAppState<NxAppManager>[0];
                    EditorUtility.SetDirty(_nxAppManager);
                }
                // There is some serialization issues when switching between version 2020 and 2019 of Unity. For the time being, we want to avoid nullref exceptions 
                bool anyStateNull = false;
                List<NxAppState<NxAppManager>> notNullStates = new List<NxAppState<NxAppManager>>();
                for (int i = 0; i < _nxAppManager.StateMachine.States.Length; i++)
                {
                    if (_nxAppManager.StateMachine.States[i] != null)
                    {
                        notNullStates.Add(_nxAppManager.StateMachine.States[i]);
                    }
                    else
                    {
                        anyStateNull = true;
                    }
                }
                if (anyStateNull)
                {
                    _nxAppManager.StateMachine.States = notNullStates.ToArray();
                    EditorUtility.SetDirty(_nxAppManager);
                }
            }
        }

        // This is a copy pasta using linq and reflection from here: https://answers.unity.com/questions/1779912/how-to-find-all-classes-deriving-from-a-base-class.html
        private List<System.Type> GetAllAppStateTypes()
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(assembly => assembly.GetTypes())
                .Where(type => type.IsSubclassOf(typeof(NxAppState<NxAppManager>))).ToList();
        }
    
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            
            // Draw the UI for the state machine
            if (_nxAppManager.StateMachine == null)
            {
                if (GUILayout.Button("New State Machine"))
                {
                    _nxAppManager.StateMachine = new NxAppStateMachine<NxAppManager>();
                    _nxAppManager.StateMachine.States = new NxAppState<NxAppManager>[0];
                    // Since we are operating on types that are not serialized by unity, make sure we mark as dirty, so odin gets a serialization callback on save.
                    EditorUtility.SetDirty(_nxAppManager);
                }
            }
            else
            {
                _foldOutStates = EditorGUILayout.BeginFoldoutHeaderGroup(_foldOutStates, "States");
                // Draw the states
                if (_foldOutStates)
                {
                    EditorGUI.BeginChangeCheck();
                    // Alternate colours a bit so it's a bit easier to see the items in the list
                    Color[] colors = new[] {Color.gray * 0.25f, Color.gray * 0.5f};
                    for (int i = 0; i < _nxAppManager.StateMachine.States.Length; i++)
                    {
                        GUI.backgroundColor = colors[i % colors.Length];
                        EditorGUILayout.BeginVertical(_stateStyle);    
                        EditorGUILayout.BeginHorizontal();
                        // State name
                        EditorGUILayout.LabelField(_nxAppManager.StateMachine.States[i].GetType().Name, _boldText);
                        // Remove state button
                        GUI.backgroundColor = Color.white;
                        if (GUILayout.Button("Remove State", GUILayout.MaxWidth(100)))
                        {
                            // Remove the state
                            List<NxAppState<NxAppManager>> states = new List<NxAppState<NxAppManager>>(_nxAppManager.StateMachine.States);
                            states.RemoveAt(i);
                            _nxAppManager.StateMachine.States = states.ToArray();
                            // Break out as we just modified the array we were looping through.
                            break;
                        }
                        GUI.backgroundColor = colors[i % colors.Length];
                        EditorGUILayout.EndHorizontal();
                        // Optional data fields
                        EditorGUI.indentLevel++;
                        EditorGUILayout.LabelField("Optional References");
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.PrefixLabel("View");
                        _nxAppManager.StateMachine.States[i].View = (AbstractView)EditorGUILayout.ObjectField(_nxAppManager.StateMachine.States[i].View, typeof(AbstractView), false);
                        EditorGUILayout.EndHorizontal();
                        
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.PrefixLabel("Prefab");
                        _nxAppManager.StateMachine.States[i].Prefab = (GameObject)EditorGUILayout.ObjectField(_nxAppManager.StateMachine.States[i].Prefab, typeof(GameObject), false);
                        EditorGUILayout.EndHorizontal();
                        
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.PrefixLabel("Persistent Data");
                        _nxAppManager.StateMachine.States[i].PersistentData = (NxAppData)EditorGUILayout.ObjectField(_nxAppManager.StateMachine.States[i].PersistentData, typeof(NxAppData), false);
                        EditorGUILayout.EndHorizontal();
                        
                        EditorGUI.indentLevel--;
                        EditorGUILayout.EndVertical();
                        
                        
                    }
                    if (EditorGUI.EndChangeCheck())
                    {
                        EditorUtility.SetDirty(_nxAppManager);
                    }

                    EditorGUILayout.Space();
                    // Draw a button to add a state
                    GUI.backgroundColor = Color.white;
                    EditorGUILayout.BeginHorizontal(_foldOutStyle);
                    EditorGUILayout.PrefixLabel("Add State");
                    _addStateDropdownChoice = EditorGUILayout.Popup(_addStateDropdownChoice, _possibleAppStateNames);
                    if (GUILayout.Button("Add State"))
                    {
                        Array.Resize(ref _nxAppManager.StateMachine.States, _nxAppManager.StateMachine.States.Length + 1);
                        Type stateTypeToAdd = _possibleAppStates[_addStateDropdownChoice];
                        _nxAppManager.StateMachine.States[_nxAppManager.StateMachine.States.Length - 1] =
                            Activator.CreateInstance(stateTypeToAdd) as NxAppState<NxAppManager>;
                        EditorUtility.SetDirty(_nxAppManager);
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                    
                }

                EditorGUILayout.EndFoldoutHeaderGroup();
                
                // Setup initial state / editor initial state data
                string currentInitialState = "";
                if (_nxAppManager.InitialState != null)
                {
                    currentInitialState = _nxAppManager.InitialState.GetType().Name;
                }
                else if (_nxAppManager.StateMachine.States.Length > 0) 
                {
                    _nxAppManager.InitialState = _nxAppManager.StateMachine.States[0];
                    currentInitialState = _nxAppManager.InitialState.GetType().Name;
                    EditorUtility.SetDirty(_nxAppManager);
                }

                string currentEditorInitialState = "";
                if (_nxAppManager.EditorInitialState != null)
                {
                    currentEditorInitialState = _nxAppManager.EditorInitialState.GetType().Name;
                }
                else if (_nxAppManager.StateMachine.States.Length > 0) 
                {
                    _nxAppManager.EditorInitialState = _nxAppManager.StateMachine.States[0];
                    currentEditorInitialState = _nxAppManager.EditorInitialState.GetType().Name;
                    EditorUtility.SetDirty(_nxAppManager);
                }
                
                int currentInitialStateID = 0;
                int currentcurrentEditorInitialStateID = 0;
                
                string[] stateChoices = new string[_nxAppManager.StateMachine.States.Length];
                for (int i = 0; i < _nxAppManager.StateMachine.States.Length; i++)
                {
                    stateChoices[i] = _nxAppManager.StateMachine.States[i].GetType().Name;
                    if (stateChoices[i] == currentInitialState)
                    {
                        currentInitialStateID = i;
                    }

                    if (stateChoices[i] == currentEditorInitialState)
                    {
                        currentcurrentEditorInitialStateID = i;
                    }
                }
                
                // Draw initial state
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PrefixLabel("Initial State");
                _initialStateDropdownChoice = EditorGUILayout.Popup(currentInitialStateID, stateChoices);
                if (currentInitialStateID != _initialStateDropdownChoice)
                {
                    _nxAppManager.InitialState = _nxAppManager.StateMachine.States[_initialStateDropdownChoice];
                    EditorUtility.SetDirty(_nxAppManager);
                }
                EditorGUILayout.EndHorizontal();
                
                // Draw editor initial state
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PrefixLabel("Editor Initial State");
                _editorInitialStateDropdownChoice = EditorGUILayout.Popup(currentcurrentEditorInitialStateID, stateChoices);
                if (currentcurrentEditorInitialStateID != _editorInitialStateDropdownChoice)
                {
                    _nxAppManager.EditorInitialState = _nxAppManager.StateMachine.States[_editorInitialStateDropdownChoice];
                    EditorUtility.SetDirty(_nxAppManager);
                }
                EditorGUILayout.EndHorizontal();
            }
            
            serializedObject.ApplyModifiedProperties();
            // Since we're doing some custom serialization, make sure to set dirty if the ui changed
            
            
            DrawDefaultInspector();
        }
    }
}