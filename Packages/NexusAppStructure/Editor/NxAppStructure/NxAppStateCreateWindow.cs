﻿using System.IO;
using Loju.View.Editor;
using UnityEditor;
using UnityEngine;

namespace NxAppStructureEditor
{
    public class CreateAppStateWindow : EditorWindow
    {
        private string _stateFolder = "Assets/Scripts/AppStructure/States";
        private string _stateName;
        private string _scriptPath = "Assets/Scripts/AppStructure/States";
        private TextAsset _template;
        
        [MenuItem("Nexus/Create/State")]
        [MenuItem("Assets/Create/Nexus/State")]
        public static void Init()
        {
            CreateAppStateWindow window = (CreateAppStateWindow)EditorWindow.GetWindow(typeof(CreateAppStateWindow));
            window.titleContent = new GUIContent("Create State");
            window.minSize = new Vector2(400, 200);
            window.Show();
        }

        public void OnGUI()
        {
            _stateName = EditorGUILayout.TextField("State Name", _stateName);
            EditorGUILayout.Space();
            if (_template == null)
            {
                _template = AssetDatabase.LoadAssetAtPath<TextAsset>(
                    "Packages/com.nexusstudios.appstructure/Editor/NxAppStructure/NxAppStateTemplate.txt");
            }
            _template = (TextAsset)EditorGUILayout.ObjectField("Script template",_template, typeof(TextAsset),false);
            _scriptPath = UViewEditorUtils.LayoutPathSelector(_scriptPath, "Script Path");
            string pathViewName = string.IsNullOrEmpty(_stateName) ? "{STATE_NAME}" : _stateName;
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField(CreatePath(pathViewName, _scriptPath, "cs"));
            EditorGUILayout.EndVertical();
            
            if (GUILayout.Button("Create"))
            {
                CreateState(pathViewName, _scriptPath);
                Close();
            }
        }
        
        private string CreatePath(string viewName, string folder, string extension)
        {
            viewName = viewName.Replace(" ", "")+"State";
            return Path.Combine(folder, string.Format("{0}.{1}", viewName, extension));
        }

        private void CreateState(string stateName, string scriptsFolder)
        {
            stateName = stateName.Replace(" ", "");
            if (!Directory.Exists(scriptsFolder)) Directory.CreateDirectory(scriptsFolder);
            string scriptData = _template.text.Replace("{STATE_NAME}", stateName+"State");
            string scriptPath = CreatePath(stateName, scriptsFolder, "cs");
            File.WriteAllText(scriptPath, scriptData);
            AssetDatabase.Refresh();
        }
    }
}
