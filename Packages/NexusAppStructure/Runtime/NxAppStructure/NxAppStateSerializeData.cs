using System;
using Loju.View;
using UnityEngine;

namespace NxAppStructure
{
    [Serializable]
    public class NxAppStateSerializeData
    {
        public AbstractView View;
        public GameObject Prefab;
        public NxAppData PersistentData;

        public NxAppStateSerializeData(AbstractView view, GameObject prefab, NxAppData persistentData)
        {
            View = view;
            Prefab = prefab;
            PersistentData = persistentData;
        }
    }
}