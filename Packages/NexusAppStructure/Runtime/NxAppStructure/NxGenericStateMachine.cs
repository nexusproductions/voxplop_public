using System;
using UnityEngine;

namespace NxAppStructure
{
	[Serializable]
    public class NxGenericStateMachine<T>
    {
	    public NxGenericState<T>[] States;
        protected T _context;
		#pragma warning disable
		public event Action OnStateChanged;
		#pragma warning restore

	    private NxGenericState<T> _currentState;
	    private NxGenericState<T> _previousState;
	    public NxGenericState<T> CurrentState { get { return _currentState; } }
	    public NxGenericState<T> PreviousState { get { return _previousState; } }
		public float ElapsedTimeInState = 0f;


		public NxGenericStateMachine( T machine )
		{
			this._context = machine;
			_currentState = States[0];
			_currentState.Begin();
		}

		public NxGenericStateMachine()
		{
			
		}

		public bool HasContext()
		{
			return _context != null;
		}

		public void SetContext(T machine)
		{
			this._context = machine;
		}


		public void Initialize(NxGenericState<T> startingState)
		{
			for (int i = 0; i < States.Length; i++)
			{
				States[i].SetMachineAndContext(this, _context);
				if (States[i].GetType() == startingState.GetType())
				{
					_currentState = States[i];
				}
			}
			_currentState.Begin();
		}

		/// <summary>
		/// ticks the state machine with the provided delta time
		/// </summary>
		public void Update( float deltaTime )
		{
			ElapsedTimeInState += deltaTime;
			_currentState.Reason();
			_currentState.Update( deltaTime );
		}


		/// <summary>
		/// changes the current state
		/// </summary>
		public R ChangeState<R>() where R : NxGenericState<T>
		{
			// avoid changing to the same state
			var newType = typeof( R );
			if( _currentState.GetType() == newType )
				return _currentState as R;

			// Find the new state in our machine
			NxGenericState<T> nextState = null;
			for (int i = 0; i < States.Length; i++)
			{
				if (States[i].GetType() == newType)
				{
					nextState = States[i];
				}
			}

			// Throw an error if it's not there.
			if (nextState == null)
			{
				string error = "State is not in the state machine: " + newType;
				Debug.LogError(error);
				throw new Exception(error);
			}

			// Check if the next state is valid
			if (!_currentState.IsValidNextState(nextState))
			{
				string warning = newType + " is not a valid new state for "+_currentState.GetType();
				Debug.LogWarning(warning);
				return _currentState as R;
			}
			
			// only call end if we have a currentState
			if (_currentState != null)
			{
				_currentState.WillExitTo(nextState);
				_currentState.End();
			}

			// swap states and call begin
			_previousState = _currentState;
			_currentState = nextState;
			_currentState.WillEnterFrom(_previousState);
			_currentState.Begin();
			ElapsedTimeInState = 0f;

			// fire the changed event if we have a listener
			if( OnStateChanged != null )
				OnStateChanged();

			return _currentState as R;
		}
    }
}