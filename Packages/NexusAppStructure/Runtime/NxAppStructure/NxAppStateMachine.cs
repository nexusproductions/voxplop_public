using System;
using UnityEngine;

namespace NxAppStructure
{
	[Serializable]
    public class NxAppStateMachine<T>
    {
	    public NxAppState<T>[] States;

	    protected T _context;
		#pragma warning disable
		public event Action OnStateChanged;
		#pragma warning restore
	    
	    private NxAppState<T> _currentState;
	    private NxAppState<T> _previousState;
	    public NxAppState<T> CurrentState { get { return _currentState; } }
	    public NxAppState<T> PreviousState { get { return _previousState; } }
	    public float ElapsedTimeInState = 0f;

		public NxAppStateMachine( T machine )
		{
			this._context = machine;
			_currentState = States[0];
			_currentState.Begin();
		}

		public NxAppStateMachine()
		{
			
		}

		public bool HasContext()
		{
			return _context != null;
		}

		public void SetContext(T machine)
		{
			this._context = machine;
		}


		public void Initialize(NxAppState<T> startingState)
		{
			for (int i = 0; i < States.Length; i++)
			{
				States[i].SetMachineAndContext(this, _context);
				if (States[i].GetType() == startingState.GetType())
				{
					_currentState = States[i];
				}
			}
			if (_currentState.Prefab != null)
			{
				_currentState.PrefabInstance = GameObject.Instantiate(_currentState.Prefab);
			}
			_currentState.Begin();
		}

		/// <summary>
		/// ticks the state machine with the provided delta time
		/// </summary>
		public void Update( float deltaTime )
		{
			ElapsedTimeInState += deltaTime;
			_currentState.Reason();
			_currentState.Update( deltaTime );
		}
		
		/// <summary>
		/// provides the state machine with a LateUpdate opportunity, for things that may rely on animation updates etc.
		/// </summary>
		public void LateUpdate( float deltaTime )
		{
			_currentState.LateUpdate( deltaTime );
		}


		/// <summary>
		/// changes the current state
		/// </summary>
		public R ChangeState<R>() where R : NxAppState<T>
		{
			// avoid changing to the same state
			var newType = typeof( R );
			if( _currentState.GetType() == newType )
				return _currentState as R;

			// Find the new state in our machine
			NxAppState<T> nextState = null;
			for (int i = 0; i < States.Length; i++)
			{
				if (States[i].GetType() == newType)
				{
					nextState = States[i];
				}
			}

			// Throw an error if it's not there.
			if (nextState == null)
			{
				string error = "State is not in the state machine: " + newType;
				Debug.LogError(error);
				throw new Exception(error);
			}

			// Check if the next state is valid
			if (!_currentState.IsValidNextState(nextState))
			{
				string warning = newType + " is not a valid new state for "+_currentState.GetType();
				Debug.LogWarning(warning);
				return _currentState as R;
			}
			
			// only call end if we have a currentState
			if (_currentState != null)
			{
				_currentState.WillExitTo(nextState);
				// Destroy the current prefab
				if (_currentState.PrefabInstance != null)
				{
					GameObject.Destroy(_currentState.PrefabInstance);
				}
				_currentState.End();
			}

			// swap states and call begin
			_previousState = _currentState;
			_currentState = nextState;
			_currentState.WillEnterFrom(_previousState);
			// Instantiate the prefab if there is one
			if (_currentState.Prefab != null)
			{
				_currentState.PrefabInstance = GameObject.Instantiate(_currentState.Prefab);
			}
			_currentState.Begin();
			ElapsedTimeInState = 0f;

			// fire the changed event if we have a listener
			if( OnStateChanged != null )
				OnStateChanged();

			return _currentState as R;
		}
    }
}