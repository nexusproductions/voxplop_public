﻿using Loju.View;
using NxAppStructure;
using NxStateMachine.OdinSerializer;
using UnityEngine;

/// <summary>
/// The AppManager is the glue responsible for keeping key references and communicating between the different systems.
/// </summary>
public class NxAppManager : SerializedMonoBehaviour
{
    #region NexusApp
    
    [Header("Nexus App references")]
    public Camera MainCamera;
    public ViewController ViewController;
    [OdinSerialize]
    public NxAppStateMachine<NxAppManager> StateMachine;
    [OdinSerialize]
    public NxAppState<NxAppManager> InitialState;
    [OdinSerialize]
    public NxAppState<NxAppManager> EditorInitialState;
    public NxAppData GlobalAppData;

    [SerializeField] [HideInInspector] private NxAppStateSerializeData[] _serializedStates;
    [SerializeField] [HideInInspector] private NxAppStateSerializeData _serializedInitialState;
    [SerializeField] [HideInInspector] private NxAppStateSerializeData _serializedEditorInitialState;
    
    #endregion

    // Initialize the state machine in Awake
    public virtual void Awake()
    {
        // Unity doesn't store the protected context when the state machine is created in editor
        if (!StateMachine.HasContext())
        {
            StateMachine.SetContext(this);
        }
        // If there are prefabs hanging around in the scene, delete them.
        for (int i = 0; i < StateMachine.States.Length; i++)
        {
            if (StateMachine.States[i].PrefabInstance != null)
            {
                DestroyImmediate(StateMachine.States[i].PrefabInstance);
            }
        }
        
        StateMachine.OnStateChanged += OnStateChanged;
        // Initialize all the app data
        if (GlobalAppData != null)
        {
            GlobalAppData.Initialize();    
        }
        for (int i = 0; i < StateMachine.States.Length; i++)
        {
            if (StateMachine.States[i].PersistentData != null)
            {
                StateMachine.States[i].PersistentData.Initialize();
            }
        }
        // Initialize the view controller
        // Initialize the state machine
        
        // For testing in editor purposes, jump directly to the editor state if it is defined
        if (Application.isEditor && EditorInitialState != null)
        {
            StateMachine.Initialize(EditorInitialState);
        }
        else
        {
            StateMachine.Initialize(InitialState);
        }
        
        // If the current state is expecting a specific view, set it and feed it the persistent data from this state
        if (StateMachine.CurrentState.View != null)
        {
            var view = StateMachine.CurrentState.View.GetType();
            var data = StateMachine.CurrentState.PersistentData;
            ViewController.Setup(view, data);
        }
    }

    // Update is called once per frame with the delta time from Unity
    public virtual void Update()
    {
        StateMachine.Update(Time.deltaTime);
    }

    // Late update is called after all Update calls including unity's own systems such as animation update.
    public void LateUpdate()
    {
        StateMachine.LateUpdate(Time.deltaTime);
    }

    public void ChangeState<R>() where R : NxAppState<NxAppManager>
    {
        var state = StateMachine.ChangeState<R>();
        if (state.View != null)
        {
            var view = state.View.GetType();
            ViewController.ChangeLocation(view, state.PersistentData, true);
        }
    }

    public virtual void OnStateChanged()
    {
    }

    #region Serialization

    protected override void OnBeforeSerialize()
    {
        _serializedStates = new NxAppStateSerializeData[StateMachine.States.Length];
        for (int i = 0; i < StateMachine.States.Length; i++)
        {
            _serializedStates[i] = new NxAppStateSerializeData(StateMachine.States[i].View, StateMachine.States[i].Prefab, StateMachine.States[i].PersistentData);
        }
    
        _serializedInitialState = new NxAppStateSerializeData(InitialState.View, InitialState.Prefab, InitialState.PersistentData);
        _serializedEditorInitialState = new NxAppStateSerializeData(EditorInitialState.View, EditorInitialState.Prefab, EditorInitialState.PersistentData);
    }
    
    protected override void OnAfterDeserialize()
    {
        if (_serializedStates != null && _serializedStates.Length > 0)
        {
            // The states are already serialized by odin, but the data apparently messes up occasionally.
            // Let's try deferring to unity serialization
            for (int i = 0; i < _serializedStates.Length; i++)
            {
                StateMachine.States[i].View = _serializedStates[i].View;
                StateMachine.States[i].Prefab = _serializedStates[i].Prefab;
                StateMachine.States[i].PersistentData = _serializedStates[i].PersistentData;
            }
        }
    
        if (_serializedInitialState != null)
        {
            InitialState.View = _serializedInitialState.View;
            InitialState.Prefab = _serializedInitialState.Prefab;
            InitialState.PersistentData = _serializedInitialState.PersistentData;
        }
    
        if (_serializedEditorInitialState != null)
        {
            EditorInitialState.View = _serializedEditorInitialState.View;
            EditorInitialState.Prefab = _serializedEditorInitialState.Prefab;
            EditorInitialState.PersistentData = _serializedEditorInitialState.PersistentData;
        }
    }

    #endregion
    
}
