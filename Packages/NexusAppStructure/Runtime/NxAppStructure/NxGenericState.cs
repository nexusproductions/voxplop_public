namespace NxAppStructure
{
    public abstract class NxGenericState<T>
    {
	    protected NxGenericStateMachine<T> _machine;
        protected T _context;
		
        public NxGenericState()
        {}
		
        internal void SetMachineAndContext( NxGenericStateMachine<T> machine, T context )
        {
            _machine = machine;
            _context = context;
            OnInitialized();
        }

        /// <summary>
        /// called directly after the machine and context are initialized allowing the state to do any required one-time setup
        /// </summary>
        public virtual void OnInitialized()
        {}

        /// <summary>
        /// Did enter gets called before instantiating the prefab (if there is one) and lets you know which state it came from.
        /// </summary>
        /// <param name="fromPrevState"></param>
        public virtual void WillEnterFrom(NxGenericState<T> fromPrevState)
        {
	        
        }
        
        /// <summary>
        /// Begin is called once the state has been entered and the prefab has been instantiated.
        /// This is where you should initialize any runtime part of your state.
        /// </summary>
        public abstract void Begin();


        /// <summary>
        /// Reason is called every frame without respect to delta time
        /// </summary>
        public abstract void Reason();
		
		/// <summary>
		/// Update is called every frame and gives you the delta time since last update
		/// </summary>
		/// <param name="deltaTime"></param>
        public abstract void Update( float deltaTime );

		/// <summary>
		/// WillExitTo gets called before destroying the prefab, and tells you which state is about to be loaded.
		/// </summary>
		/// <param name="nextState"></param>
		public virtual void WillExitTo(NxGenericState<T> nextState)
		{
	        
		} 

		/// <summary>
		/// End is called after the prefab has been destroyed, you should clean up anything else you have created in the
		/// scene / memory. The state instance itself lives on.
		/// </summary>
        public abstract void End();

		/// <summary>
		/// IsValidNextState should implement if the nextState is valid. If all states are valid next states, just implement
		/// return true;
		/// </summary>
		/// <param name="nextState"></param>
		/// <returns></returns>
		public abstract bool IsValidNextState(NxGenericState<T> nextState);
    }
}