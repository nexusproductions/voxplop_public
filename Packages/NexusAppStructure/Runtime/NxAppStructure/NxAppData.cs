﻿using System;
using NxStateMachine.OdinSerializer;

namespace NxAppStructure
{
    /// <summary>
    /// The NxAppData class should be used to store persistent data and references as a scriptable object
    /// </summary>
    [Serializable]
    public abstract class NxAppData : SerializedScriptableObject
    {
        /// <summary>
        /// Called as soon as the app has been initialized and the state machine is ready. 
        /// </summary>
        public virtual void Initialize()
        {
            
        }
    }
}
